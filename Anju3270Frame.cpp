#include "Anju3270Frame.h"
#include <wx/aboutdlg.h>
const long Anju3270Frame::ID_STATUSBAR_MAIN = wxNewId();
Anju3270Frame::Anju3270Frame(wxWindow* parent)
    : Anju3270FrameBase(parent)
{
	Connect(ID_3270_CALLBACK,wxEVT_COMMAND_MENU_SELECTED,
	        wxCommandEventHandler(Anju3270Frame::On3270Callback));
	Connect(ID_KEYBOARD_HOOK,wxEVT_COMMAND_MENU_SELECTED,
	        wxCommandEventHandler(Anju3270Frame::OnKeyBoardHookCallBack));
            SetKeyhook(true);
			//----
			//Scr_Buffer= NULL;
            Cur_Session = NULL;
			//---enable debug logging
			//----
	    FILE *m_logfile = NULL;
		event_mutex = new wxMutex(wxMUTEX_DEFAULT);
		//Scr_Buffer = NULL;
        m_logfile = fopen("./debuglog.txt", "w+");
		wxLogStderr* m_logger = new wxLogStderr(m_logfile);
        wxASSERT(m_logger);
        wxLog::SetActiveTarget(m_logger);
        wxLogMessage("Logging Started");
	//-
			//---
	//---
//delete m_grid_main;
//m_grid_main = new wxGrid( this,
//                   -1,
//                 wxPoint( 0, 0 ),
	//                this->GetClientSize() );
//m_grid_main->SetSize(this->GetClientSize());
//m_grid_main->CreateGrid( 80, 40);
//m_grid_main->SetBackgroundColour(*wxBLACK);
//m_grid_main->SetGridLineColour(*wxBLACK);
//m_grid_main->HideColLabels();
//m_grid_main->HideRowLabels();
//m_grid_main->EnableGridLines(false);
//m_grid_main->EnableScrolling(false,false);
//m_grid_main->ShowScrollbars(wxSHOW_SB_NEVER,wxSHOW_SB_NEVER);
//m_grid_main->SetRowSize( 0, 60 );
//m_grid_main->SetColSize( 0, 120 );
//m_grid_main->SetColFormatFloat(5, 6, 2);
//m_grid_main->SetCellValue(0, 6, "3.1415");
//wxMessageBox( wxT("Window Activate"), wxT("Debug Message"));
	Init_Screen();
	//Clear_Screen();
	//--
m_menuItem_Save_As->Enable(false);
m_menuItem_Save->Enable(false);
}

Anju3270Frame::~Anju3270Frame()
{
}
/*
void Anju3270Frame::OnConnect(wxCommandEvent& event)
{
}*/
void Anju3270Frame::Init_Screen()
{  
	//-----------
	//----------
//	delete m_grid_main;
	/*m_grid_main = new wxGrid(this, wxID_ANY, wxDefaultPosition, wxSize(40,80), wxWANTS_CHARS|wxBORDER_NONE);
	//m_grid_main->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_BACKGROUND));
	wxFont m_grid_mainFont(9, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxT("Courier New"));
	m_grid_main->SetFont(m_grid_mainFont);
	m_grid_main->SetDefaultCellFont(m_grid_mainFont);
	m_grid_main->CreateGrid(40, 80);
	//m_grid_main->SetRowLabelAlignment(wxALIGN_RIGHT, wxALIGN_CENTRE);
	//m_grid_main->SetColLabelAlignment(wxALIGN_CENTRE, wxALIGN_CENTRE);
#if wxVERSION_NUMBER >= 2904
	m_grid_main->UseNativeColHeader(true);
#endif
	m_grid_main->EnableEditing(true);
	m_grid_main->HideColLabels();
	m_grid_main->HideRowLabels();
	m_grid_main->EnableGridLines(false);
	//m_grid_main->EnableScrolling(false,false);
	//m_grid_main->ShowScrollbars(wxSHOW_SB_NEVER,wxSHOW_SB_NEVER);
	//m_grid_main->SetColSizes() 
	m_mainsizer->Add(m_grid_main, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	//m_grid_main->SetSize(this->GetClientSize());
	m_grid_main->AutoSize();*/
	//m_html_3270->SetSize(this->GetClientSize());
	statusbar_m = new wxStatusBar(this, ID_STATUSBAR_MAIN, 0, _T("ID_STATUSBAR_MAIN"));
	//int __wxStatusBarWidths_1[1] = { -1 };
    int __wxStatusBarStyles_1[2] = { wxSB_NORMAL , wxSB_NORMAL};
    statusbar_m->SetFieldsCount(2); //,__wxStatusBarWidths_1);
    statusbar_m->SetStatusStyles(2,__wxStatusBarStyles_1);
    SetStatusBar(statusbar_m);
	Create_Web_View();
	this->Maximize(true);
}
void Anju3270Frame::On3270Callback(wxCommandEvent& event)
{
	event_mutex->Lock();
	//wxLogDebug(wxT("3270 Callback.."));
	std::stringstream bufferstr;
	switch(event.GetInt()) {
	case CONNECT_3270_ERROR:
		Error_Exit(wxT("Session Error "));
		break;
	case CONNECT_3270_SUCCESS:
		if (Cur_Session->Run() != wxTHREAD_NO_ERROR ) {
			LOGPRINTF("Can't run socket Listener!");
		}
		break;
	case TN3270_INVALID_COMMAND:
		Error_Exit(wxT("Invalid TN3270 Packet Receieved ")+Cur_Session->Get_Command_Buffer());
		//delete Cur_Session;
		//Cur_Session = NULL;
		break;
	//case TN3270_INVALID_OPTION:
	//	Error_Exit(wxT("Invalid TN3270 Option Receieved ")+Cur_Session->Get_Command_Buffer());
		//delete Cur_Session;
		//Cur_Session = NULL;
		break;
	case TN3270_LOGIC_ERROR:
		Error_Exit(wxT("Program Exception-")+Cur_Session->Err_Message);
		//delete Cur_Session;
		//Cur_Session = NULL;
		break;
	case TN3270_INVALID_DATASTREAM:
		Error_Exit(wxT("3270 Datastream Error-")+Cur_Session->Get_Command_Buffer());
		//delete Cur_Session;
		//Cur_Session = NULL;
		break;
	case TN3270_INVALID_WSF:
		//	wxMessageBox( Cur_Session->Err_Message, wxT("Session Error"), wxICON_ERROR);
		Error_Exit(wxT("Invalid Structured Field Request "));
		break;	
	case TN3270_SOCKET_READ_ERROR:
	Error_Exit(wxT("Socket Read Error"));
	break;
	    case TN3270_LOCK_SCREEN:
    LockScreen();
    break;
    case TN3270_UNLOCK_SCREEN:
    UnLockScreen();
    break;
	case TN3270_REFRESH_SCREEN:
	//UnLockScreen();
	Refresh_Screen();
	break;
	default:
	break;
	}

	wxUnusedVar(event);
event_mutex->Unlock();
}
void Anju3270Frame::OnExit(wxCommandEvent& event)
{
	wxUnusedVar(event);
	Close();
}

void Anju3270Frame::OnAbout(wxCommandEvent& event)
{
	wxUnusedVar(event);
	wxAboutDialogInfo info;
	info.SetCopyright(_("My Anju3270Frame"));
	info.SetLicence(_("GPL v2 or later"));
	info.SetDescription(_("Short description goes here"));
	::wxAboutBox(info);
}


void Anju3270Frame::On_3270_Size(wxSizeEvent& event)
{
	/*
	m_grid_main->SetSize(this->GetClientSize());*/
	//m_html_3270->SetSize(this->GetClientSize());
	m_web_view->SetSize(this->GetClientSize());
}

void Anju3270Frame::OnConnect(wxCommandEvent& event)
{
	Con_Dialog = new ConnectionSetupDialog(this);
	Con_Dialog->ShowModal();
	if(Con_Dialog->verified)
	{
	Cur_Session = new Session3270(this,(std::string)Con_Dialog->Host,(std::string)Con_Dialog->Port,(std::string)Con_Dialog->Terminal_Type,Con_Dialog->tn3270eflag);
	//Cur_Session->Host=Con_Dialog->Host;
	//Cur_Session->Port=Con_Dialog->Port;
	//Cur_Session->Terminal_Type=Con_Dialog->Terminal_Type;
//	Cur_Session->Screen_Height=Con_Dialog->max_rows;
//	Cur_Session->Screen_Width=Con_Dialog->max_columns;
//	Scr_Buffer = new ScreenBuffer3270(Cur_Session->GetScreenHeight(),Cur_Session->GetScreenWidth());
	Cur_Session->Connect_To_Host();
	m_menuItem_Save_As->Enable(true);
m_menuItem_Save->Enable(true);
	}
	delete Con_Dialog;
}
/*
void Anju3270Frame::Update_Screen_Buffer(wxString data,bool changeflag)
{
	wxCharBuffer buffer=data.ToUTF8();
	//Update_Screen_Buffer((unsigned char*)buffer.data(),strlen(buffer.data()));
	Cur_Session->GetScrBuffer()->Update_Screen_Buffer((unsigned char*)buffer.data(),strlen(buffer.data()),changeflag);
}
void Anju3270Frame::Update_Screen_Buffer(unsigned char* data,int length,bool changeflag,bool refreshflag)
{
	
	if(Cur_Session->GetScrBuffer()->Update_Screen_Buffer(data,length,changeflag))
	{
		if(refreshflag)
		{Refresh_Screen();}
		}
	else
	{
		if(!changeflag)
		{
			Error_Exit(wxString::FromUTF8((char*)data,length));
		
		}
		else
		{
			LockScreen();

		}
	}
}*/
/*
void Anju3270Frame::Set_Screen_Buffer_Address(int bufadr)
{
	//currow = (int) (bufadr / Cur_Session->Screen_Width);
	//curcolumn = (bufadr-currow*Cur_Session->Screen_Width);
	//----------------send message to stream ----
	std::stringstream bufferstr;
    bufferstr << "Anju3270Frame::Set_Screen_Buffer_Address " <<bufadr <<std::endl;
	Utility::Log_Message(bufferstr.str());
	//-------------------------------------------
	
	Cur_Session->GetScrBuffer()->Set_Screen_Buffer_Address(bufadr);
}*/
void Anju3270Frame::Refresh_Screen()
{

//HTMLParser::Convert_3270_Screen_to_HTML(SCreenBuffer,bufhtml,rows,cols,SCREEN_BUF_SIZE*1000);
Utility::Log_Message("Refresh_Screen\n");
if(Cur_Session!=NULL)
{
    //std::string bufhtml=HTMLParser::Convert_3270_Screen_to_HTML(Cur_Session->GetScrBuffer());
std::string bufhtml= Cur_Session->Get_Screen_HTML();
Utility::Log_Message(bufhtml.c_str());
m_web_view->SetPage(wxString(bufhtml),"");

}
}

void Anju3270Frame::Create_Web_View()
{
	m_web_view = wxWebView::New(this, wxID_ANY);
	m_web_view->SetName("Screen3270");
	m_mainsizer->Add(m_web_view, wxSizerFlags().Expand().Proportion(1));
	m_web_view->SetSize(this->GetClientSize());
	wxString bufhtml="<html><bodystyle=\"background-color:#000000\"><h1 style=\"color:blue\">Anju3270</h1></body></html>";
	m_web_view->SetPage(bufhtml,"");
	this->SetFocus();
}

void Anju3270Frame::OnKeyBoardHookCallBack(wxCommandEvent& event)
{
    if(!IsKeyhook())
    {
        return;
    }
	Utility::Log_Message(std::string(event.GetString()+wxString::Format("-%d -%d-",event.GetInt(),event.GetId())+" Key Captured"));
	if(Cur_Session!=NULL) 
	{
	wxChar uc = event.GetInt();
	//SetStatusText("Key Pressed-"+ uc,1);
    if ( uc != WXK_NONE )
    {
        // It's a "normal" character. Notice that this includes
        // control characters in 1..31 range, e.g. WXK_RETURN or
        // WXK_BACK, so check for them explicitly.
        if ( uc >= 32 )
        {
			//if(!Cur_Session->GetScrBuffer()->IsLocked())
           // {this->Update_Screen_Buffer((unsigned char*)&uc,1,true);
			
			//Refresh_Screen();}
            Cur_Session->Process_Key(uc); 
        }
        else
        {
            // It's a control character
        //    ...
		//
		switch ( uc )
        {
			//case WXK_ESCAPE:
			//UnLockScreen();
			//break;
			//case WXK_RETURN:		
			//Cur_Session->Process_Key(uc);
			//break;
           case WXK_TAB:
         //  Cur_Session->GetScrBuffer()->Set_Cursor_Pos(Cur_Session->GetScrBuffer()->Get_Next_Field_Pos());
         Cur_Session->Move_Cursor(CURSOR_TAB);
          // Refresh_Screen();
           break;
			default:
            Cur_Session->Process_Key(uc);
			break;
		}
		//
        }
    }
    else // No Unicode equivalent.
    {
        // It's a special key, deal with all the known ones:
        int k =event.GetExtraLong();
      //  if(Utility::Key_Map.find(k) != Utility::Key_Map.end())
      //  {
      //   Cur_Session->Process_Key(Utility::Key_Map[k]);  
        //}
       // else
       // {
        switch (k  )
        {
            case WXK_LEFT:
			Cur_Session->Move_Cursor(CURSOR_LEFT);
			//Refresh_Screen();
			break;
            case WXK_RIGHT:
			Cur_Session->Move_Cursor(CURSOR_RIGHT);
			//Refresh_Screen();
			break;
			case WXK_UP:
			Cur_Session->Move_Cursor(CURSOR_UP);
			//Refresh_Screen();
			break;
			case WXK_DOWN:
			Cur_Session->Move_Cursor(CURSOR_DOWN);
			//Refresh_Screen();
			break;
            /*
            case WXK_RETURN:		
			Send_Screen_Buffer(AID_ENTER);
			break;*/
            case WXK_TAB:
            Cur_Session->Move_Cursor(CURSOR_TAB);//GetScrBuffer()->Set_Cursor_Pos(Cur_Session->GetScrBuffer()->Get_Next_Field_Pos());
            //Refresh_Screen();
           break;
          default:
    Cur_Session->Process_Key(k);  
				//--
				//--
				break;
				
        }
      //  }
		//int curpos=Cur_Session->GetScrBuffer()->Get_Cursor_Pos();
  SetStatusText(wxString::Format("%d %d",Cur_Session->GetRowPos(),Cur_Session->GetColPos()),1);
	}

	}
	}

void Anju3270Frame::Error_Exit(wxString ErrorMsg)
{
	//Set_Screen_Buffer_Address(0);
	//Update_Screen_Buffer(ErrorMsg);
    wxMessageBox(ErrorMsg);
   Refresh_Screen();
    
//    delete this->Scr_Buffer;
  //  delete this->Cur_Session;
}

void Anju3270Frame::LockScreen()
{
	//Cur_Session->GetScrBuffer()->LockScreen();
	SetStatusText(wxString::Format("%s","Locked"),0);
}

void Anju3270Frame::UnLockScreen()
{
	//Cur_Session->GetScrBuffer()->UnLockScreen();
	SetStatusText(wxString::Format("%s","Unlocked"),0);
}

void Anju3270Frame::Connection_Close(wxCommandEvent& event)
{
   
	if(Cur_Session)
	{this->Cur_Session->Close_Connection();
	this->Cur_Session= NULL;
	//Clear_Screen();
	Refresh_Screen();
	m_menuItem_Save_As->Enable(false);
m_menuItem_Save->Enable(false);
		}
         
}
void Anju3270Frame::On_Menu_Save_Session(wxMenuEvent& event)
{
}
void Anju3270Frame::On_Menu_Save_Session_As(wxCommandEvent& event)
{
	 wxString filepath = Show_File_Dialog();
	 std::ofstream out(filepath);
	 if(Cur_Session)
	 {
 SessionParms st = this->Cur_Session->Get_Session_Parameters();
 out<<st;
 out.close();
	 }
	 
	//wxFileInputStream input_stream(filepath);
}
std::string Anju3270Frame::Show_File_Dialog(wxString message,long style)
{
    SetKeyhook(false);
	/* if (...current content has not been saved...)
    {
        if (wxMessageBox(_("Current content has not been saved! Proceed?"), _("Please confirm"),
                         wxICON_QUESTION | wxYES_NO, this) == wxNO )
            return;
        //else: proceed asking to the user the new file to open
    }*/
    
    //wxFileDialog 
    //    openFileDialog(this, _("Save Current Session"), "", "",
     //                  "ANJ Files (*.anj)|*.anj", wxFD_OPEN|wxFD_FILE_MUST_EXIST);
 wxFileDialog openFileDialog(this, message, "", "", "ANJ Files (*.anj)|*.anj", style);
    if (openFileDialog.ShowModal() == wxID_CANCEL)
        {   SetKeyhook(true);
            return ""; }    // the user changed idea...
            SetKeyhook(true);
    return  std::string(openFileDialog.GetPath().mb_str());
    // proceed loading the file chosen by the user;
    // this can be done with e.g. wxWidgets input streams:
   

}
void Anju3270Frame::On_Menu_Open_Session(wxCommandEvent& event)
{
	 wxString filepath = Show_File_Dialog("Open Session",wxFD_OPEN);
	 //std::ifstream in(filepath);
     if(filepath=="")
     {
         return;
     }
     this->SetSessionPath(filepath);
       std::ifstream in(filepath, std::ios::in | std::ios::binary);
       SessionParms sessparms;
       in>>sessparms;
     /*
	 std::string str =Utility::get_file_contents(filepath.mb_str()) ;
	// in>>str;
	 std::string delim="\n:";
	 vector<std::string> parts;
	 Utility::string_split(str, delim, parts);
     std::string host,port,term;
	 unsigned int i=0;
	// Cur_Session = new Session3270(this);
    bool tn3270flag = false;
 for ( i=0;i<parts.size();i++)
{
    Utility::Log_Message(parts[i]);
    Utility::Cleanup_String(parts[i]);
    Utility::Log_Message(parts[i]);
	if(i%2!=0)
	{
		if(parts[i-1].compare("Host")==0)
		{
	 host=parts[i];
			}
		else if(parts[i-1].compare("Port")==0)
		{
			
	
	port=parts[i];
			
		}
		else if(parts[i-1].compare("TerminalType")==0)
		{
			term=parts[i];
		}
		else if(parts[i-1].compare("TN3270E")==0)
		{
			if(parts[i].compare("True")==0)
            {
               tn3270flag=true; 
            }
            else
            {
              tn3270flag=false;  
            }
		}
		
			}
	
	
           // cout  << address_entry.first << " < " << address_entry.second << ">" << endl;
} */ 
 	this->Connection_Close(*(wxCommandEvent*) 0);
 
 //Cur_Session = new Session3270(this,host,port,term,tn3270flag);
 Cur_Session = new Session3270(this,sessparms);
 //Scr_Buffer = new ScreenBuffer3270(Cur_Session->GetScreenHeight(),Cur_Session->GetScreenWidth());
   	Cur_Session->Connect_To_Host();
    m_menuItem_Save_As->Enable(true);
m_menuItem_Save->Enable(true);
}
//void Anju3270Frame::On_Edit_Session(wxCommandEvent& event)
//{
//}
void Anju3270Frame::Edit_Session_Settings(wxMenuEvent& event)
{
}
void Anju3270Frame::On_Edit_Session(wxCommandEvent& event)
{
}
