#ifndef ANJU3270FRAME_H
#define ANJU3270FRAME_H
#include "STLserialization.h"
#include "wxcrafter.h"
#include "ScreenBuffer3270.h"
#include "DataStream3270.h"
#include "Session3270.h"
#include "ConnectionSetupDialog.h"
#include "Utility.h"
#include "HTMLParser.h"
#include <wx/log.h>
#include "wx/webview.h"
#include "wx/webviewarchivehandler.h"
#include "wx/webviewfshandler.h"
#include "wx/infobar.h"
#include "wx/filesys.h"
#include "wx/fs_arc.h"
#include "wx/fs_mem.h"
#include <wx/statusbr.h>
#include <wx/filedlg.h>
#include <fstream>
#include <vector>
using std::vector;
#include "ScreenBuffer3270.h"

#define ID_KEYBOARD_HOOK 10009999
class Anju3270Frame : public Anju3270FrameBase
{
public:
    Anju3270Frame(wxWindow* parent);
    virtual ~Anju3270Frame();
    void OnExit(wxCommandEvent& event);
    void OnAbout(wxCommandEvent& event);
  //  void FullKeyCapture(int keycode);

protected:
    virtual void On_Edit_Session(wxCommandEvent& event);
    virtual void Edit_Session_Settings(wxMenuEvent& event);
  //  virtual void On_Edit_Session(wxCommandEvent& event);
    virtual void On_Menu_Open_Session(wxCommandEvent& event);
    virtual void On_Menu_Save_Session(wxMenuEvent& event);
    virtual void On_Menu_Save_Session_As(wxCommandEvent& event);
    virtual void Connection_Close(wxCommandEvent& event);
    //    virtual void Close(wxCommandEvent& event);
    //virtual void Kill_Focus(wxFocusEvent& event);
    //virtual void On_Activate(wxActivateEvent& event);
    //virtual void Set_Focus(wxFocusEvent& event);
    //  virtual void On_Activate(wxActivateEvent& event);
    //  virtual void Key_Up(wxKeyEvent& event);
    //  virtual void Kill_Focus(wxFocusEvent& event);
    // virtual void Set_Focus(wxFocusEvent& event);
    //  virtual void OnChar(wxKeyEvent& event);
    // virtual void Key_Down(wxKeyEvent& event);
    // virtual void Key_UP(wxKeyEvent& event);
    virtual void OnConnect(wxCommandEvent& event);
    // virtual void Key_Down(wxKeyEvent& event);
    virtual void On_3270_Size(wxSizeEvent& event);
 //   virtual void On_3270_Activate(wxActivateEvent& event);

private:
    bool keyhook;
    wxString session_path;
   
    void SetSessionPath(const wxString& session_path)
    {
        this->session_path = session_path;
    }
    const wxString& GetSessionPath() const
    {
        return session_path;
    }
    void SetKeyhook(bool keyhook)
    {
        this->keyhook = keyhook;
    }
    bool IsKeyhook() const
    {
        return keyhook;
    }
    Session3270* Cur_Session;
    wxMutex* event_mutex;
    ConnectionSetupDialog* Con_Dialog;
    // int currow,curcolumn;
    wxWebView* m_web_view;
    // unsigned char Screen_Buffer[SCREEN_BUF_SIZE ];
    // ScreenBuffer3270* Scr_Buffer;
    static const long ID_STATUSBAR_MAIN;
    wxPanel* panel_m;
    wxStatusBar* statusbar_m;
    void LockScreen();
    void UnLockScreen();
    // void Update_Screen_Buffer(unsigned char* data,int length,bool changeflag=false,bool refreshflag=true);
    // void Update_Screen_Buffer(wxString data,bool changeflag=false);
    void Refresh_Screen();
    void Init_Screen();
    void On3270Callback(wxCommandEvent& event);
    void OnKeyBoardHookCallBack(wxCommandEvent& event);
    // void Set_Screen_Buffer_Address(int bufadr);
    void Error_Exit(wxString ErrorMsg);
    /*
    int Get_Screen_Buffer_Data(int startbufadr,int endbufadr,unsigned char* getbuf);
    int Get_Screen_Buffer_Data(unsigned char* getbuf);
     */
    // void Send_Screen_Buffer();
    //	 void Send_Screen_Buffer(unsigned char AID=AID_NO);
    // int Get_Cur_Buffer_Pos();
    void Create_Web_View();
    std::string Show_File_Dialog(wxString message = "Save Current Session",
                                 long style = wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
};
#endif // ANJU3270FRAME_H
