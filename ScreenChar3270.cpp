#include "ScreenChar3270.h"

ScreenChar3270::ScreenChar3270()
{
	Init();
}

ScreenChar3270::~ScreenChar3270()
{
}
/*void ScreenChar3270::SetData ( unsigned char data )
		{
			this->data = data;
		}*/
		unsigned char ScreenChar3270::GetData() const
		{
			
			if(field)
			{
				return ' ';
			}
			else
			{
			return data;
			}
			
		}

void ScreenChar3270::MarkFieldChanged()
{
	data= data|FA_MODIFY;
}

void ScreenChar3270::SetData(unsigned char data)
{
	//changed=changeflag;
	
	this->data = data;
	this->field = false;
}

void ScreenChar3270::SetField(unsigned char fielddata)
{
	this->data = 0x00;
	this->field = true;
	if(fielddata & FA_NUMERIC )
		{
			numeric=true;
		}
		else
		{
			numeric=false;
		}
	    if(fielddata & FA_PROTECT){
		 protect = true;
		}
		else
		{
			protect = false;
		}
		

}

bool ScreenChar3270::IsField()
{
	return field;
}

bool ScreenChar3270::IsChanged()
{
	return data & FA_MODIFY;
}

void ScreenChar3270::SetFieldExtended(unsigned char* buffer, int length)
{
	int i=0;
	unsigned char tmpch;
	for(i=0;i<length;i++)
	{
	switch(*buffer)
	{
		case XA_3270:
		tmpch = *(buffer+1);
		if(tmpch & FA_NUMERIC )
		{
			numeric=true;
		}
		else
		{
			numeric=false;
		}
	    if(tmpch & FA_PROTECT){
		 protect = true;
		}
		else
		{
			protect = false;
		}
		break;
		case XA_HIGHLIGHTING:
		break;
		case XA_FOREGROUND:
			switch(*(buffer+1))
			{
				case 0x00:
				colorval="Blue";
				break;
				case 0xF1:
				colorval="Blue";
				break;
				case 0xF2:
				colorval="Red";
				break;
				case 0xF3:
				colorval="Pink";
				break;
				case 0xF4:
				colorval="Green";
				break;
				case 0xF5:
				colorval="Turquoise";
				break;
				case 0xF6:
				colorval="Yellow";
				break;
				case 0xF7:
				colorval="White";
				break;
			}
		break;
		default:
		break;

	}
    buffer+=2;
	}
	//this->SetField(' ');
	field=true;
}

bool ScreenChar3270::IsNumeric()
{
	return numeric;
}

bool ScreenChar3270::IsProtected()
{
	return protect;
}

void ScreenChar3270::Init()
{
	this->data=(unsigned char)0x00;
	colorval="Blue";
	numeric= false;
	protect = true;
	field=false;
}
