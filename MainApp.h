#ifndef MAINAPP_H
#define MAINAPP_H
// Define the MainApp

#include <wx/app.h>
#include <wx/event.h>
//#include "MainFrame.h"
//#include "Anju3270Frame.h"
#include <wx/image.h>
#include <wx/msgqueue.h>
#include <typeinfo> 
#include "Anju3270Frame.h"
typedef wxMessageQueue<wxEvent*> CommandQueue;
class MainApp : public wxApp
{

private:
Anju3270Frame *anjuFrame;

public:
    static CommandQueue AIDCommandQueue;
    MainApp() {};
    virtual ~MainApp() {};
	virtual bool OnInit();
	
	int FilterEvent(wxEvent& event);
	
   };

DECLARE_APP(MainApp)
#endif
