#ifdef CRAP1

////xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  new code
//refer https://stuff.mit.edu/afs/athena/astaff/project/x3270/src/screen.c
/*
 * https://stuff.mit.edu/afs/sipb/project/source-sipb/source/third/x3270/sf.c  
 * Process a 3270 Read-Modified command and transmit the data back to the
 * host.
 */
 #ifdef CRAP
 //---
 case ORDER_SF:	/* start field */
		cp++;		/* skip field attribute */
		copy_abort();
		screen_dirty = TRUE;
		screen_buf[buffer_addr] = FA_BASE;
		if (*cp & 0x20)
		    screen_buf[buffer_addr] |= FA_PROTECT;
		if (*cp & 0x10)
		    screen_buf[buffer_addr] |= FA_NUMERIC;
		if (*cp & 0x01)
		    screen_buf[buffer_addr] |= FA_MODIFY;
		screen_buf[buffer_addr] |= (*cp >> 2) & FA_INTENSITY;
		current_fa = &(screen_buf[buffer_addr]);
		formatted = TRUE;
		INC_BA (buffer_addr);
		last_cmd = TRUE;
		break;
 //--
do_read_modified ()
{
    register int	baddr, sbaddr;

    obptr = &obuf[0];
    if (aid != AID_PA1 && aid != AID_PA2
    &&  aid != AID_PA3 && aid != AID_CLEAR) {
	if (aid == AID_SYSREQ) {
	    *obptr++ = 0x01;	/* soh */
	    *obptr++ = 0x5B;	/*  %  */
	    *obptr++ = 0x61;	/*  /  */
	    *obptr++ = 0x02;	/* stx */
	}
	else {
	    *obptr++ = aid;
	    *obptr++ = code_table[(cursor_addr >> 6) & 0x3F];
	    *obptr++ = code_table[cursor_addr & 0x3F];
	}
	baddr = 0;
	if (formatted) {
	    /* find first field attribute */
	    do {
		if (IS_FA (screen_buf[baddr]))
		    break;
		INC_BA (baddr);
	    } while (baddr != 0);
	    sbaddr = baddr;
	    do {
		if (FA_IS_MODIFIED (screen_buf[baddr])) {
		    INC_BA (baddr);
		    *obptr++ = ORDER_SBA;
		    *obptr++ = code_table[(baddr >> 6) & 0x3F];
		    *obptr++ = code_table[baddr & 0x3F];
		    do {
			if (screen_buf[baddr])
			    *obptr++ = cg2ebc[screen_buf[baddr]];
			INC_BA (baddr);
		    } while (!IS_FA (screen_buf[baddr]));
		}
		else {	/* not modified - skip */
		    do {
			INC_BA (baddr);
		    } while (!IS_FA (screen_buf[baddr]));
		}
	    } while (baddr != sbaddr);
	}
	else {
	    do {
		if (screen_buf[baddr])
		    *obptr++ = cg2ebc[screen_buf[baddr]];
		INC_BA (baddr);
	    } while (baddr != 0);
	}
    }
    else
	*obptr++ = aid;
    net_output (obuf, obptr - obuf);
}

#endif

#define OPEN_DATA "FT:    "    /* Open request for data */

#define OPEN_MSG "FT:MSG "     /* Open request for message */

#define END_TRANSFER "TRANS03" /* Message for xfer complete */



/* Typedefs. */

struct data_buffer

{

    char sf_length[2];       /* SF length = 0x0023 */

    char sf_d0;              /* 0xD0 */

    char sf_request_type[2]; /* request type */

    char compress_indic[2];  /* 0xc080 */

    char begin_data;         /* 0x61 */

    char data_length[2];     /* Data Length in 3270 byte order+5 */

    char data[256];          /* The actual data */

};



struct open_buffer /* Buffer passed in open request */

{

    char sf_request_type[2]; /* 0x0012 for open request */

    char fixed_parms[6];     /* Doc says 010601010403 */

    char func_required[10];  /* Doc says 0a0a0000000011010100 */

    char data_not_compr[5];  /* Doc says 50055203f0 */

    char hdr_length[2];      /* 0x0309 */

    char name[7];            /* ft: or ft:msg */

};

struct error_response /* Buffer to build for an error resp. */

{

    char sf_length[2];   /* 0009 */

    char sf_d0;          /* 0xd0 */

    char sf_tt;          /* tt = 00,47,45,46 */

    char sf_08;          /* 0x08 */

    char sf_err_hdr[2];  /* 0x6904 */

    char sf_err_code[2]; /* The error code */

};

#define UPLOAD_LENGTH 2048 - 2

struct upload_buffer_hdr

{

    char sf_length[2];       /* SF length */

    char sf_d0;              /* 0xd0 */

    char sf_request_type[2]; /* 0x4605 */

    char sf_recnum_hdr[2];   /* 0x6306 */

    char sf_recnum[4];       /* record number in host byte order */

    char sf_compr_indic[2];  /* 0xc080 */

    char sf_begin_data;      /* 0x61 */

    char sf_data_length[2];  /* Data length */

};

struct upload_buffer

{

    struct upload_buffer_hdr header; /* The header */

    /* The actual data */

    char sf_data[UPLOAD_LENGTH - sizeof ( struct upload_buffer_hdr )];

};



/* Statics. */

static Boolean message_flag = False; /* Open Request for msg received */

static int at_eof;

static unsigned long recnum;

static char* abort_string = CN;



static void dft_abort();

static void dft_close_request();

static void dft_data_insert();

static void dft_get_request();

static void dft_insert_request();

static void dft_open_request();

static void dft_set_cur_req();

static int filter_len();



/* Process a Transfer Data structured field from the host. */

void ft_dft_data ( data_bufr, length ) struct data_buffer* data_bufr;

int length;

{

    unsigned short data_type;

    unsigned char* cp;



    if ( ft_state == FT_NONE )

    {

        trace_ds ( " (no transfer in progress)\n" );

        return;

    }



    /* Position to character after the d0. */

    cp = ( unsigned char* ) ( data_bufr->sf_request_type );



    /* Get the function type */

    GET16 ( data_type, cp );



    /* Handle the requests */

    switch ( data_type )

    {

        case TR_OPEN_REQ:

            dft_open_request ( ( struct open_buffer* ) cp );

            break;



        case TR_INSERT_REQ: /* Insert Request */

            dft_insert_request();

            break;



        case TR_DATA_INSERT:

            dft_data_insert ( data_bufr );

            break;



        case TR_SET_CUR_REQ:

            dft_set_cur_req();

            break;



        case TR_GET_REQ:

            dft_get_request();

            break;



        case TR_CLOSE_REQ:

            dft_close_request();

            break;



        default:

            trace_ds ( " Unsupported(0x%04x)\n", data_type );

            break;

    }

}



/* Process an Open request. */

static void dft_open_request ( open_buf ) struct open_buffer* open_buf;

{

    trace_ds ( " Open %*.*s\n", 7, 7, open_buf->name );



    if ( memcmp ( open_buf->name, OPEN_MSG, 7 ) == 0 )

        message_flag = True;



    else

    {

        message_flag = False;

        ft_running ( False );

    }



    at_eof = False;

    recnum = 1;



    trace_ds ( "> WriteStructuredField FileTransferData OpenAck\n" );

    obptr = obuf;

    space3270out ( 6 );

    * ( obuf++ ) = AID_SF;

    SET16 ( obuf, 5 );

    * ( obuf++ ) = SF_TRANSFER_DATA;

    SET16 ( obuf, 9 );

    Send_Output_Buffer();

}



/* Process an Insert request. */

static void dft_insert_request()

{

    trace_ds ( " Insert\n" );

    /* Doesn't currently do anything. */

}



/* Process a Data Insert request. */

static void dft_data_insert ( data_bufr ) struct data_buffer* data_bufr;

{

    /* Received a data buffer, get the length and process it */

    int my_length;

    unsigned char* cp;



    trace_ds ( " Data\n" );



    if ( !message_flag && ft_state == FT_ABORT_WAIT )

    {

        dft_abort ( get_message ( "ftUserCancel" ), TR_DATA_INSERT );

        return;

    }



    cp = ( unsigned char* ) ( data_bufr->data_length );



    /* Get the data length in native format. */

    GET16 ( my_length, cp );



    /* Adjust for 5 extra count */

    my_length -= 5;



    /*

     * First, check to see if we have message data or file data.

     * Message data will result in a popup.

     */

    if ( message_flag )

    {

        /* Data is from a message */

        unsigned char* msgp;

        unsigned char* dollarp;



        /* Get storage to copy the message. */

        msgp = ( unsigned char* ) XtMalloc ( my_length + 1 );



        /* Copy the message. */

        memcpy ( msgp, data_bufr->data, my_length );



        /* Null terminate the string. */

        dollarp = ( unsigned char* ) memchr ( msgp, '$', my_length );



        if ( dollarp != NULL )

            *dollarp = '\0';



        else

            * ( msgp + my_length ) = '\0';



        /* If transfer completed ok, use our msg. */

        if ( memcmp ( msgp, END_TRANSFER, strlen ( END_TRANSFER ) ) == 0 )

        {

            XtFree ( msgp );

            ft_complete ( ( String ) NULL );

        }



        else if ( ft_state == FT_ABORT_SENT && abort_string != CN )

        {

            XtFree ( msgp );

            ft_complete ( abort_string );

            abort_string = CN;

        }



        else

            ft_complete ( msgp );

    }



    else if ( my_length > 0 )

    {

        /* Write the data out to the file. */

        int rv = 1;



        if ( ascii_flag && cr_flag )

        {

            char* s = ( char* ) data_bufr->data;

            unsigned len = my_length;



            /* Delete CRs and ^Zs. */

            while ( len )

            {

                int l = filter_len ( s, len );



                if ( l )

                {

                    rv = fwrite ( s, l, ( size_t ) 1, ft_local_file );



                    if ( rv == 0 )

                        break;



                    ft_length += l;

                }



                if ( l < len )

                    l++;



                s += l;

                len -= l;

            }

        }



        else

        {

            rv = fwrite ( ( char* ) data_bufr->data, my_length, ( size_t ) 1, ft_local_file );

            ft_length += my_length;

        }



        if ( !rv )

        {

            /* write failed */

            char* buf;



            buf = xs_buffer ( "write(%s): %s", ft_local_filename, local_strerror ( errno ) );



            dft_abort ( buf, TR_DATA_INSERT );

            XtFree ( buf );

        }



        /* Add up amount transferred. */

        ft_update_length();

    }



    /* Send an acknowledgement frame back. */

    trace_ds ( "> WriteStructuredField FileTransferData DataAck(%lu)\n", recnum );

    obptr = obuf;

    space3270out ( 12 );

    * ( obuf++ ) = AID_SF;

    SET16 ( obuf, 11 );

    * ( obuf++ ) = SF_TRANSFER_DATA;

    SET16 ( obuf, TR_NORMAL_REPLY );

    SET16 ( obuf, TR_RECNUM_HDR );

    SET32 ( obuf, recnum );

    recnum++;

    Send_Output_Buffer();

}



/* Process a Set Cursor request. */

static void dft_set_cur_req()

{

    trace_ds ( " SetCursor\n" );

    /* Currently doesn't do anything. */

}



/* Process a Get request. */

static void dft_get_request()

{

    int numbytes;

    size_t numread;

    unsigned char* bufptr;

    struct upload_buffer* upbufp;



    trace_ds ( " Get\n" );



    if ( !message_flag && ft_state == FT_ABORT_WAIT )

    {

        dft_abort ( get_message ( "ftUserCancel" ), TR_GET_REQ );

        return;

    }



    /*

     * This is a request to send an upload buffer.

     * First check to see if we are finished (at_eof = True).

     */

    if ( at_eof )

    {

        /* We are done, send back the eof error. */

        trace_ds ( "> WriteStructuredField FileTransferData EOF\n" );

        space3270out ( sizeof ( struct error_response ) + 1 );

        obptr = obuf;

        * ( obuf++ ) = AID_SF;

        SET16 ( obuf, sizeof ( struct error_response ) );

        * ( obuf++ ) = SF_TRANSFER_DATA;

        * ( obuf++ ) = HIGH8 ( TR_GET_REQ );

        * ( obuf++ ) = TR_ERROR_REPLY;

        SET16 ( obuf, TR_ERROR_HDR );

        SET16 ( obuf, TR_ERR_EOF );

    }



    else

    {

        //	trace_ds ( "> WriteStructuredField FileTransferData Data(%lu)\n",

        //	           recnum );

        //	space3270out ( sizeof ( struct upload_buffer ) + 1 );

        obptr = obuf;

        * ( obuf++ ) = AID_SF;

        /* Set buffer pointer */

        upbufp = ( struct upload_buffer* ) obptr;

        /* Skip length for now */

        obptr += 2;

        * ( obuf++ ) = SF_TRANSFER_DATA;

        SET16 ( obuf, TR_GET_REPLY );

        SET16 ( obuf, TR_RECNUM_HDR );

        SET32 ( obuf, recnum );

        recnum++;

        SET16 ( obuf, TR_NOT_COMPRESSED );

        * ( obuf++ ) = TR_BEGIN_DATA;



        /* Size of the data buffer */

        numbytes = sizeof ( upbufp->sf_data );

        SET16 ( obuf, numbytes + 5 );

        bufptr = ( unsigned char* ) upbufp->sf_data;

        obptr = ( unsigned char* ) upbufp->header.sf_data_length;



        while ( numbytes > 1 )

        {

            /* Continue until we run out of buffer */

            if ( cr_flag )

            {

                /* Insert CR after LF. */

                if ( fgets ( ( char* ) bufptr, numbytes, ft_local_file ) != CN )

                {

                    /* We got a line. */



                    /* Decrement left. */

                    numbytes -= ( strlen ( ( char* ) bufptr ) + 1 );



                    /* Point to \r at end of str. */

                    bufptr += ( strlen ( ( char* ) bufptr ) - 1 );



                    if ( *bufptr == '\n' )

                    {

                        /* Stick in the \r\n. */

                        memcpy ( bufptr, "\r\n", 2 );

                    }



                    /* Point to next space. */

                    bufptr += 2;



                    if ( numbytes == 0 )

                    {

                        /* At end of buffer */

                        if ( * ( bufptr - 1 ) != '\n' )

                        {

                            /*

                             * If not a LF, Back up

                             * the buff pointer.

                             */

                            bufptr--;

                        }

                    }

                }

            }



            else

            {

                /* Not crlf, do binary read. */

                numread = fread ( bufptr, 1, numbytes, ft_local_file );

                bufptr += numread;

                numbytes -= numread;

            }



            if ( feof ( ft_local_file ) )

            {

                /* End of file. */



                /* Set that we are out of data. */

                at_eof = True;

                break;

            }



            else if ( ferror ( ft_local_file ) )

            {

                char* buf;



                buf = xs_buffer ( "read(%s): %s", ft_local_filename, local_strerror ( errno ) );

                dft_abort ( buf, TR_GET_REQ );

            }

        }



        /* Set data length. */

        SET16 ( obuf, bufptr - obptr + 4 );



        /* Accumulate length written. */

        ft_length += bufptr - obptr;



        /* Send last byte to net_output. */

        obptr = bufptr;

    }



    /* We built a buffer, let's write it back to the mainframe. */



    /* Position to beg. of buffer. */

    bufptr = obuf;

    bufptr++;



    /* Set the sf length. */

    SET16 ( bufptr, ( obptr - 1 ) - obuf );



    Send_Output_Buffer();

    ft_update_length();

}



/* Process a Close request. */

static void dft_close_request()

{

    /*

     * Recieved a close request from the system.

     * Return a close acknowledgement.

     */

    trace_ds ( " Close\n" );

    trace_ds ( "> WriteStructuredField FileTransferData CloseAck\n" );

    obptr = obuf;

    space3270out ( 6 );

    * ( obuf++ ) = AID_SF;

    SET16 ( obuf, 5 ); /* length */

    * ( obuf++ ) = SF_TRANSFER_DATA;

    SET16 ( obuf, TR_CLOSE_REPLY );

    Send_Output_Buffer();

}



/* Abort a transfer. */

static void dft_abort ( s, code ) char* s;

unsigned short code;

{

    if ( abort_string != CN )

        XtFree ( abort_string );



    abort_string = XtNewString ( s );



    trace_ds ( "> WriteStructuredField FileTransferData Error\n" );



    obptr = obuf;

    space3270out ( 10 );

    * ( obuf++ ) = AID_SF;

    SET16 ( obuf, 9 ); /* length */

    * ( obuf++ ) = SF_TRANSFER_DATA;

    * ( obuf++ ) = HIGH8 ( code );

    * ( obuf++ ) = TR_ERROR_REPLY;

    SET16 ( obuf, TR_ERROR_HDR );

    SET16 ( obuf, TR_ERR_CMDFAIL );

    Send_Output_Buffer();



    /* Update the pop-up and state. */

    ft_aborting();

}



/* Returns the number of bytes in s, limited by len, that aren't CRs or ^Zs. */

static int filter_len ( s, len ) char* s;

register int len;

{

    register char* t = s;



    while ( len && *t != '\r' && *t != 0x1a )

    {

        len--;

        t++;

    }



    return t - s;

}

//

    /*

    WSADATA wsaData;

    SOCKET ConnectSocket = INVALID_SOCKET;

    struct addrinfo *result = NULL,

                    *ptr = NULL,

                    hints;

    wxString sendbuf= "this is a test";

    char recvbuf[DEFAULT_BUFLEN];

    int iResult;

    int recvbuflen = DEFAULT_BUFLEN;

    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);

    if (iResult != 0) {

        printf("WSAStartup failed with error: %d\n", iResult);

      //  return 1;

    }



    ZeroMemory( &hints, sizeof(hints) );

    hints.ai_family = AF_UNSPEC;

    hints.ai_socktype = SOCK_STREAM;

    hints.ai_protocol = IPPROTO_TCP;





    iResult = getaddrinfo(Host.mb_str(),  Port.mb_str(), &hints, &result);

    if ( iResult != 0 ) {

        printf("getaddrinfo failed with error: %d\n", iResult);

        WSACleanup();

    //        return 1;

    }





    for(ptr=result; ptr != NULL ;ptr=ptr->ai_next) {





        ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,

            ptr->ai_protocol);

        if (ConnectSocket == INVALID_SOCKET) {

            printf("socket failed with error: %d\n", WSAGetLastError());

            WSACleanup();

       //     return 1;

        }





        iResult = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);

        if (iResult == SOCKET_ERROR) {

            closesocket(ConnectSocket);

            ConnectSocket = INVALID_SOCKET;

            continue;

        }

        break;

    }



    freeaddrinfo(result);



    if (ConnectSocket == INVALID_SOCKET) {

        printf("Unable to connect to server!\n");

        WSACleanup();

     //   return 1;

    }





    iResult = send( ConnectSocket, sendbuf, (int)strlen(sendbuf), 0 );

    if (iResult == SOCKET_ERROR) {

        printf("send failed with error: %d\n", WSAGetLastError());

        closesocket(ConnectSocket);

        WSACleanup();

    //      return 1;

    }



    printf("Bytes Sent: %i\n", iResult);





    iResult = shutdown(ConnectSocket, SD_SEND);

    if (iResult == SOCKET_ERROR) {

        printf("shutdown failed with error: %d\n", WSAGetLastError());

        closesocket(ConnectSocket);

        WSACleanup();

    //    return 1;

    }





    do {



        iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);

        if ( iResult > 0 )

            printf("Bytes received: %d\n", iResult);

        else if ( iResult == 0 )

            printf("Connection closed\n");

        else

            printf("recv failed with error: %d\n", WSAGetLastError());



    } while( iResult > 0 );





    closesocket(ConnectSocket);

    WSACleanup();

    */

    /*

     * int __cdecl main(int argc, char **argv)

    {







    if (argc != 2) {

        printf("usage: %s server-name\n", argv[0]);

        return 1;

    }







    return 0;

    }



     */
     /*

int Session3270::Parse_Telnet_Command ( unsigned char telnet_command )

{

        Command_Buffer[Command_Length] = telnet_command;



// is this the first command--- check for IAC

        if ( Command_Length == 0  )

        {

                // invalid communication recieved. Raise an error

                if ( telnet_command != IAC )

                {

                        //Raise_GUI_Event ( TN3270_INVALID_COMMAND );

                        return TN3270_INVALID_COMMAND ; // invalid command

                }



        }



        else

        {

                if ( Command_Buffer[Command_Length - 1] == IAC )   //a command started in the previous byte?

                {

                        //check valid command code

                        unsigned short i = 0;



                        for ( i = 0; i < ( sizeof ALL_COMMANDS ); i++ )

                        {

                                if ( ( unsigned char ) ALL_COMMANDS[i] == telnet_command )

                                {

                                        break;

                                }

                        }



                        if ( i >= sizeof ALL_COMMANDS )

                        {

                                //Raise_GUI_Event ( TN3270_INVALID_COMMAND );

                                return TN3270_INVALID_COMMAND; // error in command



                        }



                        else

                        {

                                //Command_Buffer[Command_Length]=telnet_command;

                                if ( telnet_command == SE )   // sub command ends

                                {

                                        wxLogMessage ( "About to send response" );



                                        if ( Send_Telnet_Response() )

                                        {

                                                Command_Length = 0;



                                        }

                                }



                        }



                }



                else     // last was not IAC. So this is either an option or a new command

                {

                        if ( Command_Buffer[Command_Length - 1] == DO || Command_Buffer[Command_Length - 1] == SB )  //

this is a DO. So it must be an option

                        {

                                unsigned short i = 0;



                                for ( i = 0; i < ( sizeof ALL_OPTIONS ); i++ )

                                {

                                        if ( ( unsigned char ) ALL_OPTIONS[i] == telnet_command )

                                        {

                                                break;

                                        }

                                }



                                if ( i >= sizeof ALL_OPTIONS )

                                {

                                        //Raise_GUI_Event ( TN3270_INVALID_COMMAND );

                                        return TN3270_INVALID_COMMAND; // invalid command



                                }



                                else

                                {

                                        if ( Command_Buffer[Command_Length - 1] == DO )

                                                //Command_Buffer[Command_Length]=telnet_command;

                                        {

                                                wxLogMessage ( "About to send response" );



                                                if ( Send_Telnet_Response() )

                                                {

                                                        Command_Length = 0;

                                                        return 0;



                                                }



                                        }



                                }

                        }

                }



        }



        Command_Length++;

        return 0;

}

 */

bool Session3270::Send_Telnet_Response()

{

        if ( Command_Length == 2 && Command_Buffer[Command_Length - 1] == DO )

        {

                switch ( Command_Buffer[Command_Length] )

                {

                        case TERMTYP:

                                wxLogMessage ( "Sending terminal ok response" );

                                sockConn->Write ( RESP_WILL_TERMINAL, 3 );

                                return true;

                                break;



                        default:

                                Raise_GUI_Event ( TN3270_INVALID_OPTION );

                                return false;

                                break;

                }



                //---------is it terminal option--//

                return false;

        }



        else if ( Command_Length == 5 && memcmp ( Command_Buffer, SB_SEND_TERMINAL, 6 ) == 0 )

        {

                unsigned char send_resp[20];

                unsigned char * resptr;

                int resptln=0;

                resptr=send_resp;

                memcpy(resptr,SB_IS_TERMINAL, sizeof SB_IS_TERMINAL );

                resptr+=sizeof SB_IS_TERMINAL;

                memcpy(resptr,Terminal_Type.mb_str(),Terminal_Type.Length());

                resptr+=Terminal_Type.Length();

                unsigned char IACSE[2] = {IAC, SE};

                memcpy(resptr,IACSE , sizeof IACSE);

                resptr+=sizeof IACSE;

                resptln= (int)(resptr-send_resp);

                sockConn->Write ( send_resp, resptln);

                wxLogMessage ( "Sending terminal info" );

                return true;

        }



        return false;

}*/

/*TELNET_OPTION*/ /*unsigned char Session3270::ALL_OPTIONS[] =

{

        BINARY, ECHO, RC, SGA, AMSN, STATUS, TM, XECHO, OLW, OPS, OCR, HTS, HTD, OFD, OVT, OVD, OLD, EASCII, LOGOUT,

BYTM, DET, SUPDUP,

        SUPDUPO, SL, TERMTYP, EOR, TUI, OMR, TLN, TN3270REG, X3PAD, NAW, TSPEED, RFC, LM, XDL, ENVO, AUTHO, ENCRYO, NEO,

TN3270E,

        XAUTH, CHARSET, TRPORT, COMPORT, SUPECHO, STLS, KERMIT, SENDURL, FORWARD_X, LOGONPRAGMA, LOGONSSPI, HRTBEAT,

EOLIST

};*/

/*TELNET_COMMAND*/ /*unsigned char Session3270::ALL_COMMANDS[] =

{

        SE, NOP, DM, BRK, IP, AO, AYT, EC, EL, GA, SB, WILL, WONT, DO, DONT, IAC

};*/

/*------------wsf start-------*/

#endif
