#include "HTMLParser.h"
/*
int HTMLParser::Convert_3270_Screen_to_HTML ( unsigned char* buf3270, unsigned char* bufhtml,
        int rows, int cols,
        int htmlbufsize )
{

	unsigned char HTML_HEADER[] = "<!DOCTYPE html><html><body  bgcolor=\"#000000\">";
	unsigned char HTML_TAILER[] = "</body></html>";
	unsigned char HTML_PARAGRAPH[] = "<p>";
	unsigned char HTML_PARAGRAPH_END[] = "</p>";
	unsigned char HTML_SPACE[]="&nbsp;";
	unsigned char HTML_FONT[]="<FONT COLOR=\"#0000ff\" SIZE=9 FACE=\"Courier New\">";
	unsigned char HTML_FONT_END[]="</FONT>";
	unsigned char* outbufptr;
	char data;
	int htmllen;
	outbufptr = bufhtml;
	APPENDHTML(outbufptr,HTML_HEADER);
    APPENDHTML(outbufptr,HTML_FONT);
	for ( int i = 0; i < rows; i++ )
	{
		APPENDHTML(outbufptr,HTML_PARAGRAPH);

		for ( int j = 0; j < cols; j++ )
		{
			data = ( char ) ( * ( buf3270 + i * cols + j ) );
			
			if ( data < ' ' ) //ignore nulls
			{
				data=' ';
			}
			if(data!=' ')
			{
			*(outbufptr++) = data;
			
			}
			else
			{
		      APPENDHTML(outbufptr,HTML_SPACE);

				
			}
			
			}
			
		

		APPENDHTML(outbufptr,HTML_PARAGRAPH_END);

	}

    APPENDHTML(outbufptr,HTML_FONT_END);
	APPENDHTML(outbufptr,HTML_TAILER);

	outbufptr = '\0';

//if it hasn't got an exception yet...
    htmllen=  int ( outbufptr - bufhtml ) ;
	if (htmllen> htmlbufsize ) // NOT AT THE RIGHT PLACE consider revising
	{
		return 0;
	}

	return htmllen;
}*/
 std::string HTMLParser::Convert_3270_Screen_to_HTML( ScreenBuffer3270* scrnbuf)
{
	//std::string htmlstring;
	std::stringstream htmlstream;
	unsigned char HTML_HEADER[] = "<!DOCTYPE html><html>";
	unsigned char HTML_JAVASCRIPT_START[]="<script type=\"text/javascript\">";
	unsigned char HTML_JAVASCRIPT_CURSOR[]="function blinkcursor(show)"
											"{"
											"if(show==true)"
											"{"
											"document.getElementById(\"mock-cursor\").style.backgroundColor=\"Grey\""

											"}"
											"else"
											"{"
											"document.getElementById(\"mock-cursor\").style.backgroundColor=\"Black\""
											"}"
											"setTimeout(function(){blinkcursor(!show)},1000);"
											"}"
											"window.onload=function(){blinkcursor(true);};";
	unsigned char HTML_JAVASCRIPT_END[]="</script>";
	unsigned char HTML_CSS_START[] ="<style type=\"text/css\">";
	unsigned char HTML_CSS_PARA[]="p { margin:0 }\n";
	/*#mock-cursor {"
									"position: absolute;"
									"color: red;"
									"-webkit-animation: blink 1s infinite;"
									"animation: blink 1s infinite;"
									"top: .45em;"
									"left: .45em;"
									"font-weight: bold;"
									"font-size: 1.2em;}\n";
*/
	unsigned char HTML_CSS_CURSOR[]="#mock-cursor {"
									"color: Yellow;"
									"background-color:Grey;"
									"}\n";
	unsigned char HTML_CSS_END[] ="</style>";
	unsigned char HTML_BODY[] = "<body  bgcolor=\"#000000\">";
	unsigned char HTML_TAILER[] = "</body></html>";
	unsigned char HTML_PARAGRAPH[] = "<p>";
	unsigned char HTML_PARAGRAPH_END[] = "</p>";
	unsigned char HTML_SPACE[]="&nbsp;";
	unsigned char HTML_FONT[]="<FONT COLOR=\"#ffffff\" SIZE=4 FACE=\"Courier New\">";
	unsigned char HTML_FONT_END[]="</FONT>";
	unsigned char HTML_SPAN_CURSOR_START[]="<span id=\"mock-cursor\">";
	unsigned char HTML_SPAN_START_PRT1[]="<span style=\"color:";
	unsigned char HTML_SPAN_START_PRT2[]="\">";
	unsigned char HTML_SPAN_END[]="</span>";
	//unsigned char* outbufptr;
	ScreenChar3270* scrchr;
	unsigned char data=' ';
	std::string colorval="Blue",scrcolor="";
	//int htmllen;
	//outbufptr = bufhtml;
	htmlstream<<HTML_HEADER;
	htmlstream<<HTML_CSS_START;
	htmlstream<<HTML_CSS_PARA;
	htmlstream<<HTML_CSS_CURSOR;
	htmlstream<<HTML_CSS_END;
	htmlstream<<HTML_JAVASCRIPT_START<<HTML_JAVASCRIPT_CURSOR<<HTML_JAVASCRIPT_END;
	htmlstream<<HTML_BODY;
    htmlstream<<HTML_FONT;
	int rows,cols,currow,curcol;//,bufadr;	
	rows=scrnbuf->GetMaxRows();
	cols=scrnbuf->GetMaxColumns();
	currow = scrnbuf->GetRow(scrnbuf->Get_Cursor_Pos());
	curcol= scrnbuf->GetCol(scrnbuf->Get_Cursor_Pos());
	for ( int i = 0; i < rows; i++ )
	{
		htmlstream<<HTML_PARAGRAPH<<HTML_SPAN_START_PRT1<<colorval<<HTML_SPAN_START_PRT2;

		for ( int j = 0; j < cols; j++ )
		{
			//data = ( char ) ( * ( buf3270 + i * cols + j ) );
			//bufadr=i * cols + j;
			//scrnbuf->Get_Screen_Buffer_Data(bufadr,bufadr,&data,false);
			scrchr = scrnbuf->GetScreenDataAt(i,j);
			if(scrchr->IsField())
			{
				data =' ';
				if(scrchr->colorval!=colorval)
				{
					colorval=scrchr->colorval;
					htmlstream<<HTML_SPAN_END<<HTML_SPAN_START_PRT1<<colorval<<HTML_SPAN_START_PRT2;
				}
			}
			else
			{
				data = scrchr->GetData();
			}
			if ( data < ' ' ) //ignore nulls
			{
				data=' ';
			}
			if(i==currow && j==curcol)
			{
				htmlstream<<HTML_SPAN_CURSOR_START;
			}
			if(data!=' ')
			{
			htmlstream<< data;
			//htmlstream<<i<<'-'<<j<<'('<<bufadr<<')'<<'='<<data<<' ';
			
			}
			else
			{
		      htmlstream<<HTML_SPACE;
			
			}
			if(i==currow && j==curcol)
			{
				htmlstream<<HTML_SPAN_END;
			}
			}
			
		

		htmlstream<<HTML_SPAN_END<<HTML_PARAGRAPH_END;

	}

	htmlstream<<HTML_FONT_END;
	htmlstream<<HTML_TAILER;

	//outbufptr = '\0';

//if it hasn't got an exception yet...
    /*htmllen=  int ( outbufptr - bufhtml ) ;
	if (htmllen> htmlbufsize ) // NOT AT THE RIGHT PLACE consider revising
	{
		return 0;
	}
*/
	return htmlstream.str();

	
}