
#include "STLserialization.h"
#include "Utility.h"

// helper function to convert a string to a number
template <typename T>
T ToNumber(const std::string& s)
{
   T val;
   std::stringstream ss(s);
   ss >> val;
   return val;
}

// operator << for pair<string,string>
 std::ostream& operator << (std::ostream& strm , const std::pair<std::string,std::string>& rec)
{
   strm << rec.first  << ":";
   strm << rec.second << std::endl;

   return strm;
}


// operator << for map<string,string>
 std::ostream& operator << (std::ostream& strm , const std::map<std::string,std::string>& smap)
{
   std::map<std::string,std::string>::const_iterator it;

   strm << smap.size() << std::endl;

   for (it=smap.begin(); it!=smap.end(); ++it)
   {
      strm << (*it);   // uses operator <<  for pair<string,string>
   }

   return strm;
}


// operator << for map< string , map<string,string> >
// std::ostream& operator << (std::ostream& strm , const std::map<std::string,std::map< std::string,std::string> >& ssmap)
//{
 //  std::map< std::string,std::map< std::string,std::string> >::const_iterator it;

 //  strm << ssmap.size() << std::endl;

 //  for (it=ssmap.begin(); it!=ssmap.end(); ++it)
 //  {
  //    strm << (*it).first << std::endl;
  //    strm << (*it).second;   // uses operator <<  for map<string,string>
  // }

  // return strm;
//}

// operator >> for pair<string,string>
 std::istream& operator >> (std::istream& strm , std::pair<std::string,std::string>& rec)
{
   std::string str;
   std::getline(strm,str);
   std::string delim=":";
   vector<std::string> parts;
   Utility::string_split(str, delim, parts);
   Utility::Cleanup_String(parts[0]);
   Utility::Cleanup_String(parts[1]);
   rec.first=parts[0];
   rec.second=parts[1];
   return strm;
}

// operator >> for map<string,string>
 std::istream& operator >> (std::istream& strm , std::map<std::string,std::string>& smap)
{
   smap.clear();

   std::string s;
   std::getline(strm,s);
   int n = ToNumber<int>(s);

   for (int i=0; i<n; ++i)
   {
      std::pair<std::string,std::string> spair;
      strm >> spair;   // uses operator >>  for pair<string,string>
      smap.insert(spair);
   }

   return strm;
}


// operator >> for map< string , map<string,string> >
// std::istream& operator >> (std::istream& strm , std::map<std::string,std::map< std::string,std::string> >& ssmap)
//{
 //  ssmap.clear();

 //  std::string s;
 //  std::getline(strm,s);
 //  int n = ToNumber<int>(s);

//   std::map<std::string,std::string> smap;
 //  for (int i=0; i<n; ++i)
 //  {
  //    std::getline(strm,s);
   //   strm >> smap;   // uses operator >>  for map<string,string>
   //   ssmap.insert(std::make_pair(s,smap));
  // }

 //  return strm;
//}

/*
//int main()
//{ 
    // create some test data

  //  std::map<std::string,std::map< std::string,std::string> > ssmap1;
   // std::map<std::string,std::string> smap;

  //  smap.insert( std::make_pair(std::string("a"),std::string("1")) );
    smap.insert( std::make_pair(std::string("b"),std::string("2")) );

    ssmap1.insert( std::make_pair( std::string("a"),smap ) );

    smap.insert( std::make_pair(std::string("c"),std::string("3 - three")) );
    ssmap1.insert( std::make_pair( std::string("b"),smap ) );

    // write to file

    std::ofstream out("test.txt");
    out << ssmap1;
    out.close();

    // read back in, compare with original

    std::map<std::string,std::map< std::string,std::string> > ssmap2;
    std::ifstream in("test.txt");

    in >> ssmap2;

    if (ssmap1 == ssmap2)
       std::cout << "read/write : OK" << std::endl;
    else
       std::cout << "read/write : ERROR" << std::endl;



    return 0;
}*/