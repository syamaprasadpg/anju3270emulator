#ifndef STLSERIALIZATION_H
#define STLSERIALIZATION_H
#include <iostream>
#include <fstream>
#include <map>
#include <sstream>


// helper function to convert a string to a number
template <typename T>
 T ToNumber(const std::string& s);

// operator << for pair<string,string>
  std::ostream& operator << (std::ostream& strm ,  std::pair<std::string,std::string> const& rec);


// operator << for map<string,string>
  std::ostream& operator << (std::ostream& strm ,  std::map<std::string,std::string> const& smap);



// operator << for map< string , map<string,string> >
 // std::ostream& operator << (std::ostream& strm , const std::map<std::string,std::map< std::string,std::string> >& ssmap);

// operator >> for pair<string,string>
  std::istream& operator >> (std::istream& strm , std::pair<std::string,std::string>& rec);

// operator >> for map<string,string>
  std::istream& operator >> (std::istream& strm , std::map<std::string,std::string>& smap);
// operator >> for map< string , map<string,string> >
//  std::istream& operator >> (std::istream& strm , std::map<std::string,std::map< std::string,std::string> >& ssmap);
#endif