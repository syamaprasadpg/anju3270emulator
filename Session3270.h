#ifndef SESSION3270_H
#define SESSION3270_H
#include "HTMLParser.h"
#include <list>
#include <wx/colour.h>
#include <wx/gdicmn.h>
#include <wx/thread.h>
#include <wx/event.h>
#include <wx/msgqueue.h>
#include <wx/app.h>
#include "ScreenBuffer3270.h"
typedef  std::map<std::string,std::string> SessionParms;
class keycode
{
public:
    std::string name;
    unsigned char code;
    bool operator == (const unsigned char& kcode)
    {
        return kcode==code?true:false;
    }
    bool operator == (const std::string & str)
    {
        return name==str?true:false;
    }
};
typedef std::list<keycode> aidlist;
typedef std::map<int, unsigned char> Key_Map;
#define CONVERT_WX_KEY(X) WX_KEY_MAP.find(X) != WX_KEY_MAP.end()?WX_KEY_MAP[X]:X;
#define IS_WX_PFKEY(X) WX_KEY_MAP.find(X) != WX_KEY_MAP.end()?1:0
#define IS_AID(X) (std::find (ALL_AIDS.begin(), ALL_AIDS.end(), X) !=ALL_AIDS.end())
#define GET_AID(X) (std::find (ALL_AIDS.begin(), ALL_AIDS.end(), X))->code
 
//#include "HTMLParser.h"
//#include "Anju3270Frame.h"
#include "TelnetCommands.h"
//#include<MainApp.h>
#include <iostream>
#include <wx/frame.h>
#include <wx/log.h>
#include <sstream>
#include "DataStream3270.h"
#include "Utility.h"
#include <vector>
using std::vector;
typedef vector<unsigned char> DynamicArray;
/*
#define WIN32_LEAN_AND_MEAN
#define _WIN32_WINNT 0x0600
 // MinGW header ws2tcpip.h checks _WIN32_WINNT before allowing the use of these
# if !defined(

_WIN32_WINNT ) || ( _WIN32_WINNT < 0x0501 )
#  undef  _WIN32_WINNT
#  define _WIN32_WINNT 0x0501
# endif
# if !defined( WINVER ) || (

WINVER < 0x0501 )
#  undef  WINVER
#  define WINVER 0x0501
# endif
#include <windows.h>
#include <ws2tcpip.h>
#include <winsock2.h>*/
#include <wx/socket.h>
#include <stdlib.h>
#include <stdio.h>
//#define DEFAULT_BUFLEN 512
//#define DEFAULT_PORT "27015"
#define COMMAND_BUF_MAX 5000
#define OUT_BUF_MAX 10000
#define DEVICE_NAME_LENGTH 50
#define ID_3270_CALLBACK 32703270
/*
 * rfc 1647
 * alid device-types are:

       terminals: IBM-3278-2  IBM-3278-2-E  (24 row x 80 col display)
                  IBM-3278-3  IBM-3278-3-E  (32 row x 80 col display)
                  IBM-3278-4  IBM-3278-4-E  (43 row x 80 col display)
                  IBM-3278-5  IBM-3278-5-E  (27 row x 132 col display)
                  IBM-DYNAMIC            (no pre-defined display size)

        printers: IBM-3287-1
 */
enum CALLBACKS_3270 {
    CONNECT_3270_ERROR = 10000,
    CONNECT_3270_SUCCESS,
    TN3270_INVALID_COMMAND,
    TN3270_INVALID_OPTION,
    TN3270_LOGIC_ERROR,
    TN3270_INVALID_DATASTREAM,
    TN3270_SOCKET_READ_ERROR,
   // TN3270_SET_BUFFER_ADR,
   // TN3270_GET_SCREEN_BUFFER,
   // TN3270_START_FIELD,
   // TN3270_RAW_DATA,
    TN3270_INVALID_WSF,
    TN3270_CTL_ERASE,
    TN3270_CTL_WRITE,
    TN3270_CTL_ERASE_ALL_UNPROT,
    TN3270_KEYBOARD_INHIBIT,
    //SET_EXTENDED_FIELD_ATTRIBUTE,
    TN3270_REFRESH_SCREEN,
    TN3270_LOCK_SCREEN,
    TN3270_UNLOCK_SCREEN,
    //TN3270_INSERT_CURSOR,
    TN3270_14BIT_ADR //,
   // TN3270_CLEAR_SCREEN
};
/*    enum TELNET_COMMAND {
        EORMARK=239, //EF = END OF RECORD MARKER
    SE=240, // End of subnegotiation parameters.
    NOP=241,// No operation.
    DM=242, // Data Mark The data stream portion of a Synch.
    //This should always be accompanied
    //by a TCP Urgent notification.
    BRK=243, //NVT character BRK.
    IP=244, //Interrupt Process,The function IP.
    AO=245, //Abort output  The function AO.
    AYT=246, //Are You There , The function AYT.
    EC=247,//Erase character 247 The function EC.
    EL=248,//Erase Line 248 The function EL.
    GA=249,//Go ahead 249 The GA signal.
    SB=250, //Indicates that what follows is

//subnegotiation of the indicated
    //option.
    WILL=251, // (option code) 251 Indicates the desire to begin
    //performing, or confirmation that
    //you are now performing, the
    //indicated option.
    WONT=252, // (option code) 252 Indicates the refusal to perform,
    // or continue performing, the
    // indicated option.
    DO=253, // (option code) 253 Indicates the request that the
    //other party perform, or
    //confirmation that you are expecting
    //the other party to perform,the
    //indicated option.
    DONT=254, // (option code) 254 Indicates the demand that the
    //other party stop performing,
    //or confirmation that you are no
    //longer expecting the other party
    //to perform, the indicated option.
    IAC=255   //Data Byte 255.

};
enum TELNET_OPTION {
    //Name 	Reference
    BINARY=0,	//Binary Transmission	[RFC856]
    ECHO=1,//	Echo	[RFC857]
    RC=2,//	Reconnection	[DDN Protocol Handbook, "Telnet Reconnection Option", "Telnet Output Line Width Option",
"Telnet Output Page Size Option", NIC 50005, December 1985.]
    SGA=3,//	Suppress Go Ahead	[RFC858]
    AMSN=4,//	Approx Message Size Negotiation	["The Ethernet, A Local Area Network: Data Link Layer and Physical
Layer Specification", AA-K759B-TK, Digital Equipment Corporation, Maynard, MA. Also as: "The Ethernet - A Local Area
Network", Version 1.0, Digital Equipment Corporation, Intel Corporation, Xerox Corporation, September 1980. And: "The
Ethernet, A Local Area Network: Data Link Layer and Physical Layer Specifications", Digital, Intel and Xerox, November
1982. And: XEROX, "The Ethernet, A Local Area Network: Data Link Layer and Physical Layer Specification", X3T51/80-50,
Xerox Corporation, Stamford, CT., October 1980.]
    STATUS=5,//	Status	[RFC859]
    TM=6,//	Timing Mark	[RFC860]
    XECHO=7,//	Remote Controlled Trans and Echo	[RFC726]
    OLW=8,//	Output Line Width	[DDN Protocol Handbook, "Telnet Reconnection Option", "Telnet Output Line Width
Option", "Telnet Output Page Size Option", NIC 50005, December 1985.]
    OPS=9,//	Output Page Size	[DDN Protocol Handbook, "Telnet Reconnection Option", "Telnet Output Line Width
Option", "Telnet Output Page Size Option", NIC 50005, December 1985.]
    OCR=10,//	Output Carriage-Return Disposition	[RFC652]
    HTS=11,//	Output Horizontal Tab Stops	[RFC653]
    HTD=12,//	Output Horizontal Tab Disposition	[RFC654]
    OFD=13,//	Output Formfeed Disposition	[RFC655]
    OVT=14,//	Output Vertical Tabstops	[RFC656]
    OVD=15,//	Output Vertical Tab Disposition	[RFC657]
    OLD=16,//	Output Linefeed Disposition	[RFC658]
    EASCII=17,//	Extended ASCII	[RFC698]
    LOGOUT=18,//	Logout	[RFC727]
    BYTM=19,//	Byte Macro	[RFC735]
    DET=20,//	Data Entry Terminal	[RFC1043][RFC732]
    SUPDUP=21,//	SUPDUP	[RFC736][RFC734]
    SUPDUPO=22,//	SUPDUP Output	[RFC749]
    SL=23,//	Send Location	[RFC779]
    TERMTYP=24,//	Terminal Type	[RFC1091]
    EOR=25,//	End of Record	[RFC885]
    TUI=26,//	TACACS User Identification	[RFC927]
    OMR=27,//	Output Marking	[RFC933]
    TLN=28,//	Terminal Location Number	[RFC946]
    TN3270REG=29, //	Telnet 3270 Regime	[RFC1041]
    X3PAD=30,//	X.3 PAD	[RFC1053]
    NAW=31,//	Negotiate About Window Size	[RFC1073]
    TSPEED=32,//	Terminal Speed	[RFC1079]
    RFC=33,//	Remote Flow Control	[RFC1372]
    LM=34,//	Linemode	[RFC1184]
    XDL=35,//	X Display Location	[RFC1096]
    ENVO=36,//	Environment Option	[RFC1408]
    AUTHO=37,//	Authentication Option	[RFC2941]
    ENCRYO=38,//	Encryption Option	[RFC2946]
    NEO=39, //	New Environment Option	[RFC1572]
    TN3270E=40, //	TN3270E	[RFC2355]
    XAUTH=41, //	XAUTH	[Rob_Earhart]
    CHARSET=42, //	CHARSET	[RFC2066]
    TRPORT=43,//	Telnet Remote Serial Port (RSP)	[Robert_Barnes]
    COMPORT=44, //	Com Port Control Option	[RFC2217]
    SUPECHO=45,//	Telnet Suppress Local Echo	[Wirt_Atmar]
    STLS=46,//	Telnet Start TLS	[Michael_Boe]
    KERMIT=47,//	KERMIT	[RFC2840]
    SENDURL=48,//	SEND-URL	[David_Croft]
    FORWARD_X=49,//	FORWARD_X	[Jeffrey_Altman]
//50-137	Unassigned	[IANA]
    LOGONPRAGMA=138, //	TELOPT PRAGMA LOGON	[Steve_McGregory]
    LOGONSSPI=139, //	TELOPT SSPI LOGON	[Steve_McGregory]
    HRTBEAT=140, //	TELOPT PRAGMA HEARTBEAT	[Steve_McGregory]
//141-254	Unassigned
    EOLIST=255 //	Extended-Options-List	[RFC861]
};
enum TELNET_SUB_OPTION
{
        IS=0,
        SEND=1
};*/
/*
 *


 */
class Session3270 : public wxThread
{
public:
    Session3270(wxFrame* parent, std::string hst, std::string prt, std::string ttype,bool tn3270eflag =true);
    Session3270(wxFrame* parent, SessionParms& parms);
    ~Session3270();
    wxFrame* m_parent;
    static  Key_Map WX_KEY_MAP;
    wxColour Screen_Background_Colour;

    void Connect_To_Host();
    virtual void* Entry();
    wxSocketClient* sockConn;
    wxString Err_Message;
    bool Err_Flag;
   // static unsigned char /*TELNET_OPTION*/ ALL_OPTIONS[54];
  //  static unsigned char /*TELNET_COMMAND*/ ALL_COMMANDS[16];
  static aidlist ALL_AIDS;
    //--static responses and inputs
    static unsigned char IAC_DO_TERMINAL[3];
    static unsigned char RESP_WILL_TERMINAL[3];
    //*--
    static unsigned char SB_SEND_TERMINAL[6];
    static unsigned char SB_IS_TERMINAL[4];
    //*--
    static unsigned char IAC_WILL_BINARY[3];
    static unsigned char IAC_DO_BINARY[3];
    //*--
    static unsigned char IAC_WILL_EOR[3];
    static unsigned char IAC_DO_EOR[3];
    //*-----3279 connection - revisit this part later
    //*--
    static unsigned char IAC_WILL_TN3270E[3];
    static unsigned char IAC_DO_TN3270E[3];
    static unsigned char IAC_WONT_TN3270E[3];
    static unsigned char IAC_SB_TN3270E_SEND_DEVICE[7];
    static unsigned char SB_TN3270E_DEVICE_TYPE_REQUEST[5];
    static unsigned char SB_TN3270E_DEVICE_IS[5];
    static unsigned char TN3270E_FUNCTIONS_REQUEST[8];
    static unsigned char TN3270E_FUNCTIONS_RESPONSE[8];
    static unsigned char IAC_SB_TN3270E_REJECT_DEVICE[6];
    //*-----------------------------------------
    bool model_3279;
    bool raw_data_mode;
    //------------------
    wxString Get_Command_Buffer();
    void Sock_Send(void* msg, int size);
    int GetRowPos();
    int GetColPos();
    bool Is_TN3270E();
    unsigned char SeqNum[2];
    void Close_Connection();
    SessionParms Get_Session_Parameters();
    short GetScreenHeight() const
    {
        return Screen_Height;
    }
    short GetScreenWidth() const
    {
        return Screen_Width;
    }
    void Send_Buffer(unsigned char* buf, int size);
    std::string Get_Screen_HTML();
    void Process_Key(int key);//=AID_NO);
    void Move_Cursor(int direction);
private:
static unsigned char supported_replies[];
 void Set_Session_Parameters(SessionParms parmlist);
 void SetTerminalType(std::string termtype);
ScreenBuffer3270* GetScrBuffer()
    {
        return Scr_Buffer;
    }

void Send_Screen_Buffer(unsigned char AID);//=AID_NO);
    // static wxMessageQueue<wxEvent*> AIDCommandQueue;
    ScreenBuffer3270* Scr_Buffer;
    
    std::string Host;
    std::string Port;
    std::string Terminal_Type;
    short Screen_Width;

    short Screen_Height;
    bool exitflg;
    void Send_Response_Command();
    // retruns zero or error code
    int Parse_Telnet_Command();
    int Parse_3270_DataStream();
    bool Send_Telnet_Response();
    void Raise_GUI_Event(int event_num, int event_id = 0);
    unsigned char Command_Buffer[COMMAND_BUF_MAX];
    int Command_Length; // zero based. Actual length is +1
    // void Sock_Send(void* msg,int size);
    int Sock_Read(void* buffer, int size);
    int Process_Data_Stream(unsigned char* buffer, int size);
    // DynamicArray obuf, device_name;
    DynamicArray device_name;
    bool support_TN3270E,isTN3270E;
    bool process_SF_RP_QUERY(unsigned char partition);
    bool process_SF_RP_QLIST(unsigned char partition, unsigned char* buf, int buflen);
    // int obufsize;
    //*==================data stream commands=========

    int CMD_Write(unsigned char* bufr, int len); //	X'01'	//Write to a character buffer.
    int Read_Buffer();                           //	X'02'	//Read the entire character buffer.
    int No_Op();
    int Erase_write(unsigned char* bufr, int len); // X'05'	//Erase and then write to a character buffer.
    int Read_Modified();         //	X'06'	//Read only the modified data from the character buffer (some exceptions).
    int Erase_Write_Alternate(unsigned char* bufr, int len); // X'0d'	//Erase and then write to the alternate size character buffer.
    int Read_Modified_All();     // X'0e'	//Read all the modified data from the character buffer (no exceptions).
    int Erase_All_Unprotected(); // X'0f'	//Erase the unprotected data from the character buffer.
    //*======structured field functions ===============
    //*===============================================
    //*==3270 Order==================================

    //= 0x05,	/* program tab */
    int Order_Program_Tab(unsigned char* buffer);
    //= 0x08,	/* graphic escape */
    int Order_Graphic_Escape(unsigned char* buffer);
    //= 0x11,	/* set buffer address */
    int Order_Set_Buffer_Address(unsigned char* buffer);
    //= 0x12,	/* erase unprotected to address */
    int Order_Erase_Unprotected_to_Address(unsigned char* buffer);
    //= 0x13,	/* insert cursor */
    int Order_Insert_Cursor(unsigned char* buffer);
    //= 0x1d,	/* start field */
    int Order_Start_Field(unsigned char* buffer);
    //= 0x28,	/* set attribute */
    int Order_Set_Attribute(unsigned char* buffer);
    //= 0x29,	/* start field extended */
    int Order_Start_Field_Extended(unsigned char* buffer);
    //= 0x2b,	/* Yale sub command */
    int Order_Yale(unsigned char* buffer);
    //= 0x2c,	/* modify field */
    int Order_Modify_Field(unsigned char* buffer);
    // = 0x3c	/* repeat to address */
    int Order_Repeat_Address(unsigned char* buffer);

    //*========handle EOR===========================
    int EOR_Received(unsigned char* buffer);
    //*=============================================
    //*=======WSF FUNCTIONS=========================
    int Write_Structured_Field(unsigned char* buffer, int len); //	X'11'	//Write a structured field.
    bool SF_Read_Part(unsigned char* cp, int fieldlen);
    bool SF_Erase_Reset(unsigned char* cp, int fieldlen);
    bool SF_Set_Reply_Mode(unsigned char* cp, int fieldlen);
    bool SF_Outbound_DS(unsigned char* cp, int fieldlen);
    // void Query_Reply_Start();
    int Do_Query_Reply(unsigned char* qbuf, unsigned char type);
    void Query_Reply_End(unsigned char* qbuf, int bufsz);
    //*=============================================
    int Process_Raw_Data(unsigned char* buffer);
    bool Is3270Order(unsigned char buffer);
    void Set16(int pos, int x);
    void Send_Output_Buffer();
    void Remove_Double_Telnet_Escape();
};
enum cursor_direction
                {
                    CURSOR_UP=1,
                    CURSOR_DOWN,
                    CURSOR_LEFT,
                    CURSOR_RIGHT,
                    CURSOR_TAB
                };
#endif // SESSION3270_H
