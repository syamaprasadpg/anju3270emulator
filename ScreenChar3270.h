#ifndef SCREENCHAR3270_H
#define SCREENCHAR3270_H
#include <string>
#include "DataStream3270.h"
class ScreenChar3270
{
	public:
		ScreenChar3270();
		void Init();
		virtual ~ScreenChar3270();
		//void SetData ( unsigned char data );
		void SetData ( unsigned char data);
		void SetField(unsigned char fielddata);
		void SetFieldExtended(unsigned char* buffer,int length);
		unsigned char GetData() const;
		void MarkFieldChanged();
		bool IsField();
		bool IsChanged();
		bool IsProtected();
		bool IsNumeric();
		std::string colorval;
	private:
		char data;
		//bool changed;
		bool field,numeric,protect;
		
};

#endif // SCREENCHAR3270_H
