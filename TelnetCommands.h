#ifndef TELNET_COMMANDS_H
#define TELNET_COMMANDS_H
enum TELNET_COMMAND {
	EORMARK=239, //EF = END OF RECORD MARKER
    SE=240, // End of subnegotiation parameters.
    NOP=241,// No operation.
    DM=242, // Data Mark The data stream portion of a Synch.
    //This should always be accompanied
    //by a TCP Urgent notification.
    BRK=243, //NVT character BRK.
    IP=244, //Interrupt Process,The function IP.
    AO=245, //Abort output  The function AO.
    AYT=246, //Are You There , The function AYT.
    EC=247,//Erase character 247 The function EC.
    EL=248,//Erase Line 248 The function EL.
    GA=249,//Go ahead 249 The GA signal.
    SB=250, //Indicates that what follows is

//subnegotiation of the indicated
    //option.
    WILL=251, // (option code) 251 Indicates the desire to begin
    //performing, or confirmation that
    //you are now performing, the
    //indicated option.
    WONT=252, // (option code) 252 Indicates the refusal to perform,
    // or continue performing, the
    // indicated option.
    DO=253, // (option code) 253 Indicates the request that the
    //other party perform, or
    //confirmation that you are expecting
    //the other party to perform,the
    //indicated option.
    DONT=254, // (option code) 254 Indicates the demand that the
    //other party stop performing,
    //or confirmation that you are no
    //longer expecting the other party
    //to perform, the indicated option.
    IAC=255   //Data Byte 255.
	
};
enum TELNET_OPTION {
    //Name 	Reference
    BINARY=0,	//Binary Transmission	[RFC856]
    ECHO=1,//	Echo	[RFC857]
    RC=2,//	Reconnection	[DDN Protocol Handbook, "Telnet Reconnection Option", "Telnet Output Line Width Option", "Telnet Output Page Size Option", NIC 50005, December 1985.]
    SGA=3,//	Suppress Go Ahead	[RFC858]
    AMSN=4,//	Approx Message Size Negotiation	["The Ethernet, A Local Area Network: Data Link Layer and Physical Layer Specification", AA-K759B-TK, Digital Equipment Corporation, Maynard, MA. Also as: "The Ethernet - A Local Area Network", Version 1.0, Digital Equipment Corporation, Intel Corporation, Xerox Corporation, September 1980. And: "The Ethernet, A Local Area Network: Data Link Layer and Physical Layer Specifications", Digital, Intel and Xerox, November 1982. And: XEROX, "The Ethernet, A Local Area Network: Data Link Layer and Physical Layer Specification", X3T51/80-50, Xerox Corporation, Stamford, CT., October 1980.]
    STATUS=5,//	Status	[RFC859]
    TM=6,//	Timing Mark	[RFC860]
    XECHO=7,//	Remote Controlled Trans and Echo	[RFC726]
    OLW=8,//	Output Line Width	[DDN Protocol Handbook, "Telnet Reconnection Option", "Telnet Output Line Width Option", "Telnet Output Page Size Option", NIC 50005, December 1985.]
    OPS=9,//	Output Page Size	[DDN Protocol Handbook, "Telnet Reconnection Option", "Telnet Output Line Width Option", "Telnet Output Page Size Option", NIC 50005, December 1985.]
    OCR=10,//	Output Carriage-Return Disposition	[RFC652]
    HTS=11,//	Output Horizontal Tab Stops	[RFC653]
    HTD=12,//	Output Horizontal Tab Disposition	[RFC654]
    OFD=13,//	Output Formfeed Disposition	[RFC655]
    OVT=14,//	Output Vertical Tabstops	[RFC656]
    OVD=15,//	Output Vertical Tab Disposition	[RFC657]
    OLD=16,//	Output Linefeed Disposition	[RFC658]
    EASCII=17,//	Extended ASCII	[RFC698]
    LOGOUT=18,//	Logout	[RFC727]
    BYTM=19,//	Byte Macro	[RFC735]
    DET=20,//	Data Entry Terminal	[RFC1043][RFC732]
    SUPDUP=21,//	SUPDUP	[RFC736][RFC734]
    SUPDUPO=22,//	SUPDUP Output	[RFC749]
    SL=23,//	Send Location	[RFC779]
    TERMTYP=24,//	Terminal Type	[RFC1091]
    EOR=25,//	End of Record	[RFC885]
    TUI=26,//	TACACS User Identification	[RFC927]
    OMR=27,//	Output Marking	[RFC933]
    TLN=28,//	Terminal Location Number	[RFC946]
    TN3270REG=29, //	Telnet 3270 Regime	[RFC1041]
    X3PAD=30,//	X.3 PAD	[RFC1053]
    NAW=31,//	Negotiate About Window Size	[RFC1073]
    TSPEED=32,//	Terminal Speed	[RFC1079]
    RFC=33,//	Remote Flow Control	[RFC1372]
    LM=34,//	Linemode	[RFC1184]
    XDL=35,//	X Display Location	[RFC1096]
    ENVO=36,//	Environment Option	[RFC1408]
    AUTHO=37,//	Authentication Option	[RFC2941]
    ENCRYO=38,//	Encryption Option	[RFC2946]
    NEO=39, //	New Environment Option	[RFC1572]
    TN3270E=40, //	TN3270E	[RFC2355]
    XAUTH=41, //	XAUTH	[Rob_Earhart]
    CHARSET=42, //	CHARSET	[RFC2066]
    TRPORT=43,//	Telnet Remote Serial Port (RSP)	[Robert_Barnes]
    COMPORT=44, //	Com Port Control Option	[RFC2217]
    SUPECHO=45,//	Telnet Suppress Local Echo	[Wirt_Atmar]
    STLS=46,//	Telnet Start TLS	[Michael_Boe]
    KERMIT=47,//	KERMIT	[RFC2840]
    SENDURL=48,//	SEND-URL	[David_Croft]
    FORWARD_X=49,//	FORWARD_X	[Jeffrey_Altman]
//50-137	Unassigned	[IANA]
    LOGONPRAGMA=138, //	TELOPT PRAGMA LOGON	[Steve_McGregory]
    LOGONSSPI=139, //	TELOPT SSPI LOGON	[Steve_McGregory]
    HRTBEAT=140, //	TELOPT PRAGMA HEARTBEAT	[Steve_McGregory]
//141-254	Unassigned
    EOLIST=255 //	Extended-Options-List	[RFC861]
};
enum TELNET_SUB_OPTION
{
	IS=0,
	SEND=1
};

enum TN3270E_SUB_COMMANDS {
         TN3270E_ASSOCIATE=00,
         TN3270E_CONNECT=01,
         TN3270E_DEVICE_TYPE=02,
         TN3270E_FUNCTIONS=03,
         TN3270E_IS=04,
         TN3270E_REASON=05,
         TN3270E_REJECT=06,
         TN3270E_REQUEST=07,
		 TN3270E_SEND=8
		 };
static  std::map<int,std::string> TN3270E_REASON_CODES ={
    {00,"TN3270E_CONN_PARTNER"},
    {01,"TN3270E_DEVICE_IN_USE"},
    {02,"TN3270E_INV_ASSOCIATE"},
    {03,"TN3270E_INV_NAME"},
             {04,"INV-DEVICE-TYPE"},    
         {05,"TYPE-NAME-ERROR"},   
         {06,"UNKNOWN-ERROR"},      
         {07,"UNSUPPORTED-REQ"}    
};
/*
 enum TN3270E_REASON_CODES {
         TN3270E_CONN_PARTNER=00,
         TN3270E_DEVICE_IN_USE=01,
         TN3270E_INV_ASSOCIATE=02,
		TN3270E_INV_NAME=03};*/
		enum TN3270E_FUNCTION_CODES {
			         TN3270E_BIND_IMAGE=00,
         TN3270E_DATA_STREAM_CTL=01,
         TN3270E_RESPONSES=02,
         TN3270E_SCS_CTL_CODES=03,
         TN3270E_SYSREQ=04
		};



#endif
