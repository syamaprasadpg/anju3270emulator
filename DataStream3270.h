#ifndef DATA_STREAM_3270_H
#define DATA_STREAM_3270_H
enum LOCAL_EBCDIC_COMMANDS_3270 {
    /* 3270 commands */
    CMD_W = 0x01,   /* write */
    CMD_RB = 0x02,  /* read buffer */
    CMD_NOP = 0x03, /* no-op */
    CMD_EW = 0x05,  /* erase/write */
    CMD_RM = 0x06,  /* read modified */
    CMD_EWA = 0x0d, /* erase/write alternate */
    CMD_RMA = 0x0e, /* read modified all */
    CMD_EAU = 0x0f, /* erase all unprotected */
    CMD_WSF = 0x11  /* write structured field */

};
////* SNA 3270 commands
enum REMOTE_EBCDIC_COMMANDS_3270 {
    SNA_CMD_RMA = 0x6e, //* read modified all
    SNA_CMD_EAU = 0x6f, //* erase all unprotected
    SNA_CMD_EWA = 0x7e, //* erase/write alternate
    SNA_CMD_W = 0xf1,   //* write
    SNA_CMD_RB = 0xf2,  //* read buffer
    SNA_CMD_WSF = 0xf3, //* write structured field
    SNA_CMD_EW = 0xf5,  //* erase/write
    SNA_CMD_RM = 0xf6   //* read modified
};
enum ASCII_COMMANDS_3270 {
    ASCII_CMD_RMA = 0x3e, //* read modified all
    ASCII_CMD_EAU = 0x3f, //* erase all unprotected
    ASCII_CMD_EWA = 0x3d, //* erase/write alternate
    ASCII_CMD_W = 0x31,   //* write
    ASCII_CMD_RB = 0x32,  //* read buffer
    // ASCII_CMD_WSF=0xf3,	//* write structured field
    ASCII_CMD_EW = 0x35, //* erase/write
    ASCII_CMD_RM = 0x36  //* read modified
};
// TN3270E HEADER BYTES
#define DATA_TYPE  0
#define REQUEST_FLAG  1
#define RESPONSE_FLAG  2
#define SEQ_NUMBER  3
enum DATA_TYPE_FLAGS {
    DATA_3270 = 0x00,
    //The data portion of the message contains only the 3270 data stream.

    SCS_DATA = 0x01, // The data portion of the message  contains SNA Character Stream data.

    RESPONSE = 0x02, // The data portion of the message  constitutes device-status information
    // and the RESPONSE-FLAG field indicates
    // whether this is a positive or negative
    // response (see below).

    BIND_IMAGE = 0x03, // The data portion of the message is
    // the SNA bind image from the session
    // established between the server and the
    // host application.

    UNBIND = 0x04, // The data portion of the message is
    // an Unbind reason code.

    NVT_DATA = 0x05, // The data portion of the message is to
    // be interpreted as NVT data.

    REQUEST = 0x06, // There is no data portion present in
    // the message.  Only the REQUEST-FLAG
    // field has any meaning.

    SSCP_LU_DATA = 0x07, // The data portion of the message is
    // data from the SSCP-LU session.

    PRINT_EOJ = 0x08 // There is no data portion present in
    //      the message.  This value can be sent
    //      only by the server, and only on a
    //      printer session.
};
// only one request flag value
#define ERR_COND_CLEARED 0x00
enum TN3270E_RESPONSE_FLAGS { NO_RESPONSE = 0x00, ERROR_RESPONSE = 0x01, ALWAYS_RESPONSE = 0x02 };
enum TN3270E_RESPONSE_ANSWER { POSITIVE_RESPONSE = 0x00, NEGATIVE_RESPONSE = 0x01 };
enum ORDERS_3270 {
    //* 3270 orders
    ORDER_PT = 0x05,   /* program tab */
    ORDER_GE = 0x08,   /* graphic escape */
    ORDER_SBA = 0x11,  /* set buffer address */
    ORDER_EUA = 0x12,  /* erase unprotected to address */
    ORDER_IC = 0x13,   /* insert cursor */
    ORDER_SF = 0x1d,   /* start field */
    ORDER_SA = 0x28,   /* set attribute */
    ORDER_SFE = 0x29,  /* start field extended */
    ORDER_YALE = 0x2b, /* Yale sub command */
    ORDER_MF = 0x2c,   /* modify field */
    ORDER_RA = 0x3c    /* repeat to address */

};
enum FORMAT_ORDERS_3270 {
    FCORDER_NULL = 0x00, /* format control: null */
    FCORDER_FF = 0x0c,   /*		   form feed */
    FCORDER_CR = 0x0d,   /*		   carriage return */
    FCORDER_SO = 0x0e,   /*                 shift out (DBCS subfield) */
    FCORDER_SI = 0x0f,   /*                 shift in (DBCS end) */
    FCORDER_NL = 0x15,   /*		   new line */
    FCORDER_EM = 0x19,   /*		   end of medium */
    FCORDER_DUP = 0x1c,  /*		   duplicate */
    FCORDER_FM = 0x1e,   /*		   field mark */
    FCORDER_SUB = 0x3f,  /*		   substitute */
    FCORDER_EO = 0xff    /*		   eight ones */
};
/* SCS control code, some overlap orders */
enum SCS_CONTROL_CODE {
    SCS_BS = 0x16,           /* Back Space  */
    SCS_BEL = 0x2f,          /* Bell Function */
    SCS_CR = 0x0d,           /* Carriage Return */
    SCS_ENP = 0x14,          /* Enable Presentation */
    SCS_FF = 0x0c,           /* Forms Feed */
    SCS_GE = 0x08,           /* Graphic Escape */
    SCS_HT = 0x05,           /* Horizontal Tab */
    SCS_INP = 0x24,          /* Inhibit Presentation */
    SCS_IRS = 0x1e,          /* Interchange-Record Separator */
    SCS_LF = 0x25,           /* Line Feed */
    SCS_NL = 0x15,           /* New Line */
    SCS_SA = 0x28,           /* Set Attribute: */
    SCS_SA_RESET = 0x00,     /*  Reset all */
    SCS_SA_HIGHLIGHT = 0x41, /*  Highlighting */
    SCS_SA_CS = 0x42,        /*  Character set */
    SCS_SA_GRID = 0xc2,      /*  Grid */
    SCS_SET = 0x2b,          /* Set: */
    SCS_SHF = 0xc1,          /*  Horizontal format */
    SCS_SLD = 0xc6,          /*  Line Density */
    SCS_SVF = 0xc2,          /*  Vertical Format */
    SCS_SO = 0x0e,           /* Shift out (DBCS subfield start) */
    SCS_SI = 0x0f,           /* Shift in (DBCS subfield end) */
    SCS_TRN = 0x35,          /* Transparent */
    SCS_VCS = 0x04,          /* Vertical Channel Select */
    SCS_VT = 0x0b            /* Vertical Tab */
};
/* Structured fields */
enum STRUCTURED_FIELD {
    SF_READ_PART = 0x01,      /* read partition */
    SF_RP_QUERY = 0x02,       /*  query */
    SF_RP_QLIST = 0x03,       /*  query list */
    SF_RPQ_LIST = 0x00,       /*   QCODE list */
    SF_RPQ_EQUIV = 0x40,      /*   equivalent+ QCODE list */
    SF_RPQ_ALL = 0x80,        /*   all */
    SF_ERASE_RESET = 0x03,    /* erase/reset */
    SF_ER_DEFAULT = 0x00,     /*  default */
    SF_ER_ALT = 0x80,         /*  alternate */
    SF_SET_REPLY_MODE = 0x09, /* set reply mode */
    SF_SRM_FIELD = 0x00,      /*  field */
    SF_SRM_XFIELD = 0x01,     /*  extended field */
    SF_SRM_CHAR = 0x02,       /*  character */
    SF_CREATE_PART = 0x0c,    /* create partition */
    CPFLAG_PROT = 0x40,       /*  protected flag */
    CPFLAG_COPY_PS = 0x20,    /*  local copy to presentation space */
    CPFLAG_BASE = 0x07,       /*  base character set index */
    SF_OUTBOUND_DS = 0x40,    /* outbound 3270 DS */
    SF_TRANSFER_DATA = 0xd0   /* file transfer open request */
};
/* Query replies */
enum QUERY_3270 {

    QR_SUMMARY = 0x80,      /* summary */
    QR_USABLE_AREA = 0x81,  /* usable area */
    QR_ALPHA_PART = 0x84,   /* alphanumeric partitions */
    QR_CHARSETS = 0x85,     /* character sets */
    QR_COLOR = 0x86,        /* color */
    QR_HIGHLIGHTING = 0x87, /* highlighting */
    QR_REPLY_MODES = 0x88,  /* reply modes */
    QR_DBCS_ASIA = 0x91,    /* DBCS-Asia */
    QR_PC3270 = 0x93,       /* PC3270 */
    QR_DDM = 0x95,          /* distributed data management */
    QR_RPQNAMES = 0xa1,     /* RPQ names */
    QR_IMP_PART = 0xa6,     /* implicit partition */
    QR_NULL = 0xff          /* null */
};
/* Field attributes. */
enum FIELD_ATTRIBUTES {
    FA_PRINTABLE = 0xc0,     /* these make the character "printable" */
    FA_PROTECT = 0x20,       /* unprotected (0) / protected (1) */
    FA_NUMERIC = 0x10,       /* alphanumeric (0) /numeric (1) */
    FA_INTENSITY = 0x0c,     /* display/selector pen detectable: */
    FA_INT_NORM_NSEL = 0x00, /*  00 normal, non-detect */
    FA_INT_NORM_SEL = 0x04,  /*  01 normal, detectable */
    FA_INT_HIGH_SEL = 0x08,  /*  10 intensified, detectable */
    FA_INT_ZERO_NSEL = 0x0c, /*  11 nondisplay, non-detect */
    FA_RESERVED = 0x02,      /* must be 0 */
    FA_MODIFY = 0x01         /* modified (1) */
};
/* Bits in the field attribute that are stored. */
#define FA_MASK (FA_PROTECT | FA_NUMERIC | FA_INTENSITY | FA_MODIFY)

/* Tests for various attribute properties. */
#define FA_IS_MODIFIED(c) ((c)&FA_MODIFY)
#define FA_IS_NUMERIC(c) ((c)&FA_NUMERIC)
#define FA_IS_PROTECTED(c) ((c)&FA_PROTECT)
#define FA_IS_SKIP(c) (((c)&FA_PROTECT) && ((c)&FA_NUMERIC))

#define FA_IS_ZERO(c) (((c)&FA_INTENSITY) == FA_INT_ZERO_NSEL)
#define FA_IS_HIGH(c) (((c)&FA_INTENSITY) == FA_INT_HIGH_SEL)
#define FA_IS_NORMAL(c) (((c)&FA_INTENSITY) == FA_INT_NORM_NSEL || ((c)&FA_INTENSITY) == FA_INT_NORM_SEL)
#define FA_IS_SELECTABLE(c) (((c)&FA_INTENSITY) == FA_INT_NORM_SEL || ((c)&FA_INTENSITY) == FA_INT_HIGH_SEL)
#define FA_IS_INTENSE(c) ((c & FA_INT_HIGH_SEL) == FA_INT_HIGH_SEL)

/* Extended attributes */
enum EXTENDED_ATTRIBUTES {
    XA_ALL = 0x00,
    XA_3270 = 0xc0,
    XA_VALIDATION = 0xc1,
    XAV_FILL = 0x04,
    XAV_ENTRY = 0x02,
    XAV_TRIGGER = 0x01,
    XA_OUTLINING = 0xc2,
    XAO_UNDERLINE = 0x01,
    XAO_RIGHT = 0x02,
    XAO_OVERLINE = 0x04,
    XAO_LEFT = 0x08,
    XA_HIGHLIGHTING = 0x41,
    XAH_DEFAULT = 0x00,
    XAH_NORMAL = 0xf0,
    XAH_BLINK = 0xf1,
    XAH_REVERSE = 0xf2,
    XAH_UNDERSCORE = 0xf4,
    XAH_INTENSIFY = 0xf8,
    XA_FOREGROUND = 0x42,
    XAC_DEFAULT = 0x00,
    XA_CHARSET = 0x43,
    XA_BACKGROUND = 0x45,
    XA_TRANSPARENCY = 0x46,
    XAT_DEFAULT = 0x00,
    XAT_OR = 0xf0,
    XAT_XOR = 0xf1,
    XAT_OPAQUE = 0xff,
    XA_INPUT_CONTROL = 0xfe,
    XAI_DISABLED = 0x00,
    XAI_ENABLED = 0x01
};
/* WCC definitions */
#define WCC_RESET(c) ((c)&0x40)
#define WCC_START_PRINTER(c) ((c)&0x08)
#define WCC_SOUND_ALARM(c) ((c)&0x04)
#define WCC_KEYBOARD_RESTORE(c) ((c)&0x02)
#define WCC_RESET_MDT(c) ((c)&0x01)

/* AIDs */

/*enum AID_3270 {
    AID_NO = 0x60, 
    AID_QREPLY = 0x61,
    AID_ENTER = 0x7d,
    AID_PF1 = 0xf1,
    AID_PF2 = 0xf2,
    AID_PF3 = 0xf3,
    AID_PF4 = 0xf4,
    AID_PF5 = 0xf5,
    AID_PF6 = 0xf6,
    AID_PF7 = 0xf7,
    AID_PF8 = 0xf8,
    AID_PF9 = 0xf9,
    AID_PF10 = 0x7a,
    AID_PF11 = 0x7b,
    AID_PF12 = 0x7c,
    AID_PF13 = 0xc1,
    AID_PF14 = 0xc2,
    AID_PF15 = 0xc3,
    AID_PF16 = 0xc4,
    AID_PF17 = 0xc5,
    AID_PF18 = 0xc6,
    AID_PF19 = 0xc7,
    AID_PF20 = 0xc8,
    AID_PF21 = 0xc9,
    AID_PF22 = 0x4a,
    AID_PF23 = 0x4b,
    AID_PF24 = 0x4c,
    AID_OICR = 0xe6,
    AID_MSR_MHS = 0xe7,
    AID_SELECT = 0x7e,
    AID_PA1 = 0x6c,
    AID_PA2 = 0x6e,
    AID_PA3 = 0x6b,
    AID_CLEAR = 0x6d,
    AID_SYSREQ = 0xf0,
    AID_SF = 0x88,
    SFID_QREPLY = 0x81
};*/
/* Colors */
enum COLOR_3270 {
    COLOR_NEUTRAL_BLACK = 0,
    COLOR_BLUE = 1,
    COLOR_RED = 2,
    COLOR_PINK = 3,
    COLOR_GREEN = 4,
    COLOR_TURQUOISE = 5,
    COLOR_YELLOW = 6,
    COLOR_NEUTRAL_WHITE = 7,
    COLOR_BLACK = 8,
    COLOR_DEEP_BLUE = 9,
    COLOR_ORANGE = 10,
    COLOR_PURPLE = 11,
    COLOR_PALE_GREEN = 12,
    COLOR_PALE_TURQUOISE = 13,
    COLOR_GREY = 14,
    COLOR_WHITE = 15,
};
/* Data stream manipulation macros. */
#define MASK32 0xff000000U
#define MASK24 0x00ff0000U
#define MASK16 0x0000ff00U
#define MASK08 0x000000ffU
#define MINUS1 0xffffffffU

/* Field attributes. */
#define FA_PRINTABLE 0xc0     /* these make the character "printable" */
#define FA_PROTECT 0x20       /* unprotected (0) / protected (1) */
#define FA_NUMERIC 0x10       /* alphanumeric (0) /numeric (1) */
#define FA_INTENSITY 0x0c     /* display/selector pen detectable: */
#define FA_INT_NORM_NSEL 0x00 /*  00 normal, non-detect */
#define FA_INT_NORM_SEL 0x04  /*  01 normal, detectable */
#define FA_INT_HIGH_SEL 0x08  /*  10 intensified, detectable */
#define FA_INT_ZERO_NSEL 0x0c /*  11 nondisplay, non-detect */
#define FA_RESERVED 0x02      /* must be 0 */
#define FA_MODIFY 0x01        /* modified (1) */

/* Bits in the field attribute that are stored. */
#define FA_MASK (FA_PROTECT | FA_NUMERIC | FA_INTENSITY | FA_MODIFY)

/* Tests for various attribute properties. */
#define FA_IS_MODIFIED(c) ((c)&FA_MODIFY)
#define FA_IS_NUMERIC(c) ((c)&FA_NUMERIC)
#define FA_IS_PROTECTED(c) ((c)&FA_PROTECT)
#define FA_IS_SKIP(c) (((c)&FA_PROTECT) && ((c)&FA_NUMERIC))

#define FA_IS_ZERO(c) (((c)&FA_INTENSITY) == FA_INT_ZERO_NSEL)
#define FA_IS_HIGH(c) (((c)&FA_INTENSITY) == FA_INT_HIGH_SEL)
#define FA_IS_NORMAL(c) (((c)&FA_INTENSITY) == FA_INT_NORM_NSEL || ((c)&FA_INTENSITY) == FA_INT_NORM_SEL)
#define FA_IS_SELECTABLE(c) (((c)&FA_INTENSITY) == FA_INT_NORM_SEL || ((c)&FA_INTENSITY) == FA_INT_HIGH_SEL)
#define FA_IS_INTENSE(c) ((c & FA_INT_HIGH_SEL) == FA_INT_HIGH_SEL)

/* Extended attributes */
#define XA_ALL 0x00
#define XA_3270 0xc0
#define XA_VALIDATION 0xc1
#define XAV_FILL 0x04
#define XAV_ENTRY 0x02
#define XAV_TRIGGER 0x01
#define XA_OUTLINING 0xc2
#define XAO_UNDERLINE 0x01
#define XAO_RIGHT 0x02
#define XAO_OVERLINE 0x04
#define XAO_LEFT 0x08
#define XA_HIGHLIGHTING 0x41
#define XAH_DEFAULT 0x00
#define XAH_NORMAL 0xf0
#define XAH_BLINK 0xf1
#define XAH_REVERSE 0xf2
#define XAH_UNDERSCORE 0xf4
#define XAH_INTENSIFY 0xf8
#define XA_FOREGROUND 0x42
#define XAC_DEFAULT 0x00
#define XA_CHARSET 0x43
#define XA_BACKGROUND 0x45
#define XA_TRANSPARENCY 0x46
#define XAT_DEFAULT 0x00
#define XAT_OR 0xf0
#define XAT_XOR 0xf1
#define XAT_OPAQUE 0xff
#define XA_INPUT_CONTROL 0xfe
#define XAI_DISABLED 0x00
#define XAI_ENABLED 0x01

/* WCC definitions */
#define WCC_RESET(c) ((c)&0x40)
#define WCC_START_PRINTER(c) ((c)&0x08)
#define WCC_SOUND_ALARM(c) ((c)&0x04)
#define WCC_KEYBOARD_RESTORE(c) ((c)&0x02)
#define WCC_RESET_MDT(c) ((c)&0x01)

/* AIDs */
/*#define AID_NO 0x60 
#define AID_QREPLY 0x61
#define AID_ENTER 0x7d
#define AID_PF1 0xf1
#define AID_PF2 0xf2
#define AID_PF3 0xf3
#define AID_PF4 0xf4
#define AID_PF5 0xf5
#define AID_PF6 0xf6
#define AID_PF7 0xf7
#define AID_PF8 0xf8
#define AID_PF9 0xf9
#define AID_PF10 0x7a
#define AID_PF11 0x7b
#define AID_PF12 0x7c
#define AID_PF13 0xc1
#define AID_PF14 0xc2
#define AID_PF15 0xc3
#define AID_PF16 0xc4
#define AID_PF17 0xc5
#define AID_PF18 0xc6
#define AID_PF19 0xc7
#define AID_PF20 0xc8
#define AID_PF21 0xc9
#define AID_PF22 0x4a
#define AID_PF23 0x4b
#define AID_PF24 0x4c
#define AID_OICR 0xe6
#define AID_MSR_MHS 0xe7
#define AID_SELECT 0x7e
#define AID_PA1 0x6c
#define AID_PA2 0x6e
#define AID_PA3 0x6b
#define AID_CLEAR 0x6d
#define AID_SYSREQ 0xf0

#define AID_SF 0x88
#define SFID_QREPLY 0x81*/

/* Colors */
#define COLOR_NEUTRAL_BLACK 0
#define COLOR_BLUE 1
#define COLOR_RED 2
#define COLOR_PINK 3
#define COLOR_GREEN 4
#define COLOR_TURQUOISE 5
#define COLOR_YELLOW 6
#define COLOR_NEUTRAL_WHITE 7
#define COLOR_BLACK 8
#define COLOR_DEEP_BLUE 9
#define COLOR_ORANGE 10
#define COLOR_PURPLE 11
#define COLOR_PALE_GREEN 12
#define COLOR_PALE_TURQUOISE 13
#define COLOR_GREY 14
#define COLOR_WHITE 15

/* Data stream manipulation macros. */
#define MASK32 0xff000000U
#define MASK24 0x00ff0000U
#define MASK16 0x0000ff00U
#define MASK08 0x000000ffU
#define MINUS1 0xffffffffU
// bitwise and on unsigned char will first convert the char to an int
// used to get buffer position
#define MASK12 0X0000003fU
#define MASK12_1 0X00000fc0U
//#define MASK12_CHK_1 0X00004000 // INDICATE 12 BIT
#define MASK12_CHK_2 0X00000040 // INDICATE 12 BIT
#define GET12BITADR(val, ptr)                                   \
    {                                                           \
	(val) = ((*(ptr)&MASK12) << 6) | (*(ptr + 1) & MASK12); \
    }
#define SET12BITADR(ptr, val)                            \
    {                                                    \
	*(ptr) = (((val)&MASK12_1)) >> 6 | MASK12_CHK_2; \
	ptr++;                                           \
	*(ptr) = (((val)&MASK12)) | MASK12_CHK_2;        \
    }
// below could be buggy code - please check
#define GET14BITADR(val, ptr)                          \
    {                                                  \
	(val) = ((*(ptr)&MASK12) << 8) | (*(ptr + 1)); \
    }

#define SET14BITADR(ptr, val)         \
    {                                 \
	*(ptr) = (val >> 8) & MASK12; \
	ptr++;                        \
	*(ptr) = val & MASK08;        \
    }
#define SET16(ptr, val)             \
    {                               \
	*ptr = ((val)&MASK16) >> 8; \
	ptr++;                      \
	*ptr = ((val)&MASK08);      \
	ptr++;                      \
    }
#define GET16(val, ptr)       \
    {                         \
	(val) = *((ptr)+1);   \
	(val) += *(ptr) << 8; \
    }
#define SET32(ptr, val)                    \
    {                                      \
	*((ptr)++) = ((val)&MASK32) >> 24; \
	*((ptr)++) = ((val)&MASK24) >> 16; \
	*((ptr)++) = ((val)&MASK16) >> 8;  \
	*((ptr)++) = ((val)&MASK08);       \
    }
#define HIGH8(s) (((s) >> 8) & 0xff)
#define LOW8(s) ((s)&0xff)

/* Other EBCDIC control codes. */
#define EBC_null 0x00
#define EBC_ff 0x0c
#define EBC_cr 0x0d
#define EBC_so 0x0e
#define EBC_si 0x0f
#define EBC_nl 0x15
#define EBC_em 0x19
#define EBC_dup 0x1c
#define EBC_fm 0x1e
#define EBC_space 0x40
#define EBC_nobreakspace 0x41
#define EBC_period 0x4b
#define EBC_ampersand 0x50
#define EBC_greater 0x6e
#define EBC_question 0x6f
#define EBC_Yacute 0xad
#define EBC_diaeresis 0xbd
#define EBC_minus 0xca
#define EBC_0 0xf0
#define EBC_9 0xf9
#define EBC_eo 0xff

/* BIND definitions. */
#define BIND_RU 0x31
#define BIND_OFF_PLU_NAME_LEN 26
#define BIND_OFF_PLU_NAME 27
#define BIND_PLU_NAME_MAX 8
#endif

/*
 *	3270ds.h
 *
 *		Header file for the 3270 Data Stream Protocol.
 */

///* 3270 commands */
///*
//#define CMD_W		0x01	//* write
//#define CMD_RB		0x02	//* read buffer
//#define CMD_NOP		0x03	//* no-op
//#define CMD_EW		0x05	//* erase/write
//#define CMD_RM		0x06	//* read modified
//#define CMD_EWA		0x0d	//* erase/write alternate
//#define CMD_RMA		0x0e	//* read modified all
//#define CMD_EAU		0x0f	//* erase all unprotected
//#define CMD_WSF		0x11	//* write structured field
//
//*/
//* 3270 orders
//#define ORDER_PT	0x05	/* program tab */
//#define ORDER_GE	0x08	/* graphic escape */
//#define ORDER_SBA	0x11	/* set buffer address */
//#define ORDER_EUA	0x12	/* erase unprotected to address */
//#define ORDER_IC	0x13	/* insert cursor */
//#define ORDER_SF	0x1d	/* start field */
//#define ORDER_SA	0x28	/* set attribute */
//#define ORDER_SFE	0x29	/* start field extended */
//#define ORDER_YALE	0x2b	/* Yale sub command */
//#define ORDER_MF	0x2c	/* modify field */
//#define ORDER_RA	0x3c	/* repeat to address */

//#define FCORDER_NULL	0x00	/* format control: null */
//#define FCORDER_FF	0x0c	/*		   form feed */
//#define FCORDER_CR	0x0d	/*		   carriage return */
//#define FCORDER_SO	0x0e	/*                 shift out (DBCS subfield) */
//#define FCORDER_SI	0x0f	/*                 shift in (DBCS end) */
//#define FCORDER_NL	0x15	/*		   new line */
//#define FCORDER_EM	0x19	/*		   end of medium */
//#define FCORDER_DUP	0x1c	/*		   duplicate */
//#define FCORDER_FM	0x1e	/*		   field mark */
//#define FCORDER_SUB	0x3f	/*		   substitute */
//#define FCORDER_EO	0xff	/*		   eight ones */

///* SCS control code, some overlap orders */
//#define SCS_BS      	0x16	/* Back Space  */
//#define SCS_BEL		0x2f	/* Bell Function */
//#define SCS_CR      	0x0d	/* Carriage Return */
//#define SCS_ENP		0x14	/* Enable Presentation */
//#define SCS_FF		0x0c	/* Forms Feed */
//#define SCS_GE		0x08	/* Graphic Escape */
//#define SCS_HT		0x05	/* Horizontal Tab */
//#define SCS_INP		0x24	/* Inhibit Presentation */
//#define SCS_IRS		0x1e	/* Interchange-Record Separator */
//#define SCS_LF		0x25	/* Line Feed */
//#define SCS_NL		0x15	/* New Line */
//#define SCS_SA		0x28	/* Set Attribute: */
//#define  SCS_SA_RESET	0x00	/*  Reset all */
//#define  SCS_SA_HIGHLIGHT 0x41	/*  Highlighting */
//#define  SCS_SA_CS	0x42	/*  Character set */
//#define  SCS_SA_GRID	0xc2	/*  Grid */
//#define SCS_SET		0x2b	/* Set: */
//#define  SCS_SHF	0xc1	/*  Horizontal format */
//#define  SCS_SLD	0xc6	/*  Line Density */
//#define  SCS_SVF	0xc2	/*  Vertical Format */
//#define SCS_SO		0x0e	/* Shift out (DBCS subfield start) */
//#define SCS_SI		0x0f	/* Shift in (DBCS subfield end) */
//#define SCS_TRN		0x35	/* Transparent */
//#define SCS_VCS		0x04	/* Vertical Channel Select */
//#define SCS_VT		0x0b	/* Vertical Tab */
//
///* Structured fields */
//#define SF_READ_PART	0x01	/* read partition */
//#define  SF_RP_QUERY	0x02	/*  query */
//#define  SF_RP_QLIST	0x03	/*  query list */
//#define   SF_RPQ_LIST	0x00	/*   QCODE list */
//#define   SF_RPQ_EQUIV	0x40	/*   equivalent+ QCODE list */
//#define   SF_RPQ_ALL	0x80	/*   all */
//#define SF_ERASE_RESET	0x03	/* erase/reset */
//#define  SF_ER_DEFAULT	0x00	/*  default */
//#define  SF_ER_ALT	0x80	/*  alternate */
//#define SF_SET_REPLY_MODE 0x09	/* set reply mode */
//#define  SF_SRM_FIELD	0x00	/*  field */
//#define  SF_SRM_XFIELD	0x01	/*  extended field */
//#define  SF_SRM_CHAR	0x02	/*  character */
//#define SF_CREATE_PART	0x0c	/* create partition */
//#define  CPFLAG_PROT	0x40	/*  protected flag */
//#define  CPFLAG_COPY_PS	0x20	/*  local copy to presentation space */
//#define  CPFLAG_BASE	0x07	/*  base character set index */
//#define SF_OUTBOUND_DS	0x40	/* outbound 3270 DS */
//#define SF_TRANSFER_DATA 0xd0   /* file transfer open request */
//
///* Query replies */
//#define QR_SUMMARY	0x80	/* summary */
//#define QR_USABLE_AREA	0x81	/* usable area */
//#define QR_ALPHA_PART	0x84	/* alphanumeric partitions */
//#define QR_CHARSETS	0x85	/* character sets */
//#define QR_COLOR	0x86	/* color */
//#define QR_HIGHLIGHTING	0x87	/* highlighting */
//#define QR_REPLY_MODES	0x88	/* reply modes */
//#define QR_DBCS_ASIA	0x91	/* DBCS-Asia */
//#define QR_PC3270	0x93    /* PC3270 */
//#define QR_DDM    	0x95    /* distributed data management */
//#define QR_RPQNAMES	0xa1	/* RPQ names */
//#define QR_IMP_PART	0xa6	/* implicit partition */
//#define QR_NULL		0xff	/* null */

//#define BA_TO_ROW(ba)		((ba) / COLS)
//#define BA_TO_COL(ba)		((ba) % COLS)
//#define ROWCOL_TO_BA(r,c)	(((r) * COLS) + c)
//#define INC_BA(ba)		{ (ba) = ((ba) + 1) % (COLS * ROWS); }
//#define DEC_BA(ba)		{ (ba) = (ba) ? (ba - 1) : ((COLS*ROWS) - 1); }
