#ifndef CONNECTIONSETUPDIALOG_H
#define CONNECTIONSETUPDIALOG_H
#include "wxcrafter.h"
#include <wx/msgdlg.h>

#include <Utility.h>
class ConnectionSetupDialog : public ConnectionSetupDialogBase
{
public:
    ConnectionSetupDialog(wxWindow* parent);
    ConnectionSetupDialog(wxWindow* parent,wxString t_host,wxString t_port,wxString t_term_type,bool t_tn3270);
    virtual ~ConnectionSetupDialog();
	wxString Host;
	wxString Port;
	wxString Terminal_Type;
    bool tn3270eflag;
	int max_rows,max_columns;
	bool verified= false;
protected:
    virtual void Button_Key_Down(wxKeyEvent& event);
    virtual void On_Click_3270_Connect(wxCommandEvent& event);
    
private:
  
//check http://www-01.ibm.com/support/knowledgecenter/SSLTBW_2.1.0/com.ibm.zos.v2r1.halz001/telnetdevicestatement.htm
/*

      -model name
           The model of 3270 display to be emulated.  The model name is in
           three parts, any of which may be omitted:

           The first part is the base model, which is either 3278 or 3279.
           3278 specifies a monochrome 3270 display; 3279 specifies a color
           3270 display.  When 3278 emulation is specified for a color X
           display, fields are displayed using pseudo-colors; see PSEUDO-
           COLOR below.

           The second part is the model number, which specifies the number
           of rows and columns.  Model 4 is the default.

                          Model Number   Columns   Rows
                          _____________________________
                               2          80      24
                             3          80      30
                           4          80      43
                         5          132      27

           Note: Technically, there is no such 3270 display as a 3279-4 or
           3279-5, but most hosts seem to work with them anyway.

           The third part specifies the Extended 3270 Data Stream, and is
           given as -E.  It signals the host that the 3270 display is
           capable of displaying extended field attributes, and supports
           structured fields and query replies.  A 3279 always uses the
           Extended Data Stream (whether or not -E is specified); for a 3278



                                    - 2 -      Formatted:  February 11, 2002






 x3270(1)                                                           x3270(1)
                              31 December 2001



           it is optional.

           The default model for a color X display is 3279-4-E.  For a
           monochrome X display, it is 3278-4-E.  (The behavior of previous
           versions of x3270 on color X displays can be specified as
           3278-4).

*/
};
#endif // CONNECTIONSETUPDIALOG_H
