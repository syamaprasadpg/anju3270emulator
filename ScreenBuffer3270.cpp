#include "ScreenBuffer3270.h"
#include "TelnetCommands.h"
/*#define INC_ROUND(i, max) \
    {                     \
        if(i++ >= max) {  \
            i = 0;        \
        }                 \
    }*/

ScreenBuffer3270::ScreenBuffer3270(int r, int c)
{
    wxASSERT ( r>= 0 && c >=0 );
    max_rows = r;
    max_columns = c;
    screenlocked = true;
    // Set up sizes. (HEIGHT x WIDTH)
    screenbuf.resize(max_rows);
    address14bit = false;
    for(int i = 0; i < max_rows; ++i) {
	screenbuf[i].resize(max_columns);
    }

    // Put some values in
    // array2D[1][2] = 6.0;
    // array2D[3][1] = 5.5;

    // re
    formatted = false;
    cursorpos = 0;
    Set_Screen_Buffer_Address(0);
}

ScreenBuffer3270::~ScreenBuffer3270()
{
}

void ScreenBuffer3270::Set_Screen_Buffer_Address(int bufadr)
{
    //*----------------send message to stream ----
    std::stringstream buffer;
    buffer << "ScreenBuffer3270::Set_Screen_Buffer_Address " << bufadr << std::endl;
    Utility::Log_Message(buffer.str());
    //*-------------------------------------------
    currow = (int)(bufadr / max_columns);
    curcolumn = int(bufadr % max_columns);
    wxASSERT(bufadr <= (max_rows * max_columns - 1));
}

int ScreenBuffer3270::Send_Screen_Buffer(unsigned char* tempbuf, unsigned char AID)
{
    // unsigned char* tbuf;

    // tbuf=tempbuf;
    tempbuf[0] = AID; //_NO;
    // memcpy(tempbuf,())AID_NO,1);
    int cursorpos, size;
    // int cols = Cur_Session->Screen_Width;//->GetNumberCols();
    // int rows = Cur_Session->Screen_Height;//m_grid_main->GetNumberRows();
    cursorpos = this->Get_Cursor_Pos(); // Get_Cur_Buffer_Pos();
    // cursorpos = 0;
    tempbuf++;
    if(Is_14Bit_Mode()) {
	SET14BITADR(tempbuf, cursorpos); // this increases tempbuf by 1
    } else {
	SET12BITADR(tempbuf, cursorpos); // this increases tempbuf by 1
    }
    tempbuf++;
    size = Get_Screen_Buffer_Data(tempbuf);
    // size=0;
    tempbuf += size;
    *(tempbuf++) = IAC;
    *(tempbuf) = EORMARK;
    // buggy code
    // Cur_Session->Sock_Send(tbuf,size+5);
    // free(tbuf);
    //---
    return size + 5;
}
/*
int ScreenBuffer3270::Get_Screen_Buffer_Data(unsigned char* getbuf)
{
        //int cols = Cur_Session->Screen_Width;//m_grid_main->GetNumberCols();
        //int rows = Cur_Session->Screen_Height;//m_grid_main->GetNumberRows();
        return Get_Screen_Buffer_Data(0,max_columns*max_rows-1,getbuf);

}*/
int ScreenBuffer3270::Get_Cur_Buffer_Pos()
{
    return currow * max_columns + curcolumn;
}
int ScreenBuffer3270::Get_Screen_Buffer_Data(unsigned char* getbuf, bool ebcdic) //, bool changed)
{

    int i = 0, startbufadr = 0, endbufadr = max_columns * max_rows - 1, length = 0;
    unsigned char* tempptr = getbuf;
    if(!formatted) {
	for(i = startbufadr; i <= endbufadr; i++) {
	    // GETBUFATADR (bufadr)
	    *tempptr++ = screenbuf[GetRow(i)][GetCol(i)].GetData();
	}
	if(ebcdic) {
	    Utility::ASCII_to_EBCDIC(length, getbuf, getbuf);
	}
	length = endbufadr - startbufadr + 1;
    } else {
	ScreenChar3270* scrchr = NULL, * scrchrf = NULL, * scrchrn = NULL;
	int strtfld;
	while(i <= endbufadr) {
	    scrchrf = &screenbuf[GetRow(i)][GetCol(i)];
	    if(scrchrf->IsField()) {
		strtfld = i;
		break;
	    }
	    i++;
	}
	scrchr = scrchrn = scrchrf;
	bool fieldchange = false;
	// int ebclen=0;
	unsigned char* ebcbufs = tempptr;
	if(scrchr)
	    do {
		if(scrchr->IsField()) {

		    if(scrchr->IsChanged()) {

			*tempptr++ = ORDER_SBA;
			// if()
			if(Is_14Bit_Mode()) {
			    SET14BITADR(tempptr, i); // this increases tempbuf by 1
			} else {
			    SET12BITADR(tempptr, i); // this increases tempbuf by 1
			}
			tempptr++;
			ebcbufs = tempptr;
			fieldchange = true;
			length += 3;
		    } else {
			fieldchange = false;
		    }
		} else {
		    if(fieldchange) {
			if(scrchr->GetData() != 0x00) {
			    *tempptr = scrchr->GetData();
			    if(ebcdic) { // last feld ended. it was changed. so convert
				Utility::ASCII_to_EBCDIC(1, tempptr, tempptr);
				tempptr++;
			    }
			    // length++;
			} else {
			    //		tempptr++;
			}
		    }
		}
		// INC_ROUND(i, endbufadr);
		if(i++ >= endbufadr) {
		    i = 0;
		}
		scrchr = &screenbuf[GetRow(i)][GetCol(i)];
		//	if(scrchr->IsField()) {
		//	    scrchrn = scrchr;
		//	}

	    } while(i != strtfld);
	length = tempptr - getbuf;
    }

    return length;
}

int ScreenBuffer3270::GetRow(int bufadr)
{
    return bufadr / max_columns;
}
int ScreenBuffer3270::GetCol(int bufadr)
{
    // column numbering start from zero
    return (bufadr % max_columns);
}
bool ScreenBuffer3270::Update_Screen_Buffer(unsigned char* data, int length, bool changeflag)
{
    int curpos;
    int i, row, col;
    if(changeflag) // key input?
    {

	curpos = Get_Cursor_Pos();
	row = GetRow(curpos);
	col = GetCol(curpos);
	if(screenbuf[row][col].IsField()) {
        LockScreen();
	    return false;
	}
	if(formatted == true) {
 if(!IsFieldProtected(curpos)) {
	 MarkFieldChanged(curpos);
	}
	 else {   
         LockScreen();
return false;
	} 
	}//else {
	  //  return false;
	//}
	// MarkFieldChanged(curpos);
    } else {
	curpos = Get_Cur_Buffer_Pos();
    }
    // if(changeflag) {

    //    }
    // if(changeflag) {
    // MarkFieldChanged(curpos);
    // }

    if((curpos + length) <= GetMaxRows() * GetMaxColumns()) {
	for(i = 0; i < length; i++) {
	    //	Screen_Buffer[curpos+i]=(char)*(data++);
	    row = GetRow(curpos);
	    col = GetCol(curpos);
	    screenbuf[row][col].SetData((char)*(data++));

	    //	if(row==2 && col==0)
	    //	{
	    //		wxASSERT(screenbuf[row][col].GetData()=='M');
	    //	}
	    // if(i==(length-1))
	    curpos++;
	}
	if((i == length) && curpos == (max_columns * max_rows)) // curpos just exceeded buffer end by one. last field
	{
	    curpos--;
	} // go back by one
	if(!changeflag) {
	    currow = GetRow(curpos);
	    curcolumn = GetCol(curpos);
	} else {
	    this->Set_Cursor_Pos(curpos);
	}
	wxASSERT(curpos <= (max_columns * max_rows - 1));
	return true;
    } else {
	return false;
    }
    // Refresh_Screen();
}
int ScreenBuffer3270::GetMaxColumns() const
{
    return max_columns;
}
int ScreenBuffer3270::GetMaxRows() const
{
    return max_rows;
}

void ScreenBuffer3270::Clear_Screen()
{
    LOGPRINTF("Clearing Screen");
    for(int i = 0; i < max_rows * max_columns; i++) {
	//	Screen_Buffer[curpos+i]=(char)*(data++);
	screenbuf[GetRow(i)][GetCol(i)].Init(); // SetData((unsigned char)0x00);
	//	curpos++;
    }
    cursorpos = 0;
    formatted = false;
    UnLockScreen();
    Set_Screen_Buffer_Address(0);
}

void ScreenBuffer3270::Start_Field(unsigned char fielddat)
{
    std::stringstream bufferstr;
    bufferstr << "ScreenBuffer3270::Start_Field" << std::endl;
    Utility::Log_Message(bufferstr.str());
    formatted = true;
    //*------startfied at the last column must not increment
    if(Get_Cur_Buffer_Pos() < (max_rows * max_columns - 2)) {

	screenbuf[GetRow()][GetCol()].SetField((unsigned char)(fielddat));
	Set_Screen_Buffer_Address(Get_Cur_Buffer_Pos() + 1);
    }
}

ScreenChar3270* ScreenBuffer3270::GetScreenDataAt(int row, int col)
{
    return &(this->screenbuf[row][col]); //.GetData();
}

void ScreenBuffer3270::Move_Cursor_Bottom()
{
    Move_Cursor_Right(max_columns);
}

void ScreenBuffer3270::Move_Cursor_Left(int x)
{
    int curpos = Get_Cursor_Pos() - x;
    if(curpos < 0) {
	curpos = max_rows * max_columns + curpos;
    }
    Set_Cursor_Pos(curpos);
}

void ScreenBuffer3270::Move_Cursor_Right(int x)
{
    int curpos = Get_Cursor_Pos();
    curpos = (curpos + x) % (max_rows * max_columns);
    Set_Cursor_Pos(curpos);
}

void ScreenBuffer3270::Move_Cursor_Top()
{
    Move_Cursor_Left(max_columns);
}

int ScreenBuffer3270::GetCol()
{
    return GetCol(Get_Cur_Buffer_Pos());
}

int ScreenBuffer3270::GetRow()
{
    return GetRow(Get_Cur_Buffer_Pos());
}

void ScreenBuffer3270::MarkFieldChanged(int bufferpos)
{
    int bufpos = bufferpos;
    ScreenChar3270* schar;
    while(bufpos >= 0) {
	schar = &(screenbuf[GetRow(bufpos)][GetCol(bufpos)]);
	if(schar->IsField()) {
	    schar->MarkFieldChanged();
	    break;
	}
	bufpos--;
    }
}
int ScreenBuffer3270::Get_Next_Field_Pos()
{
   int bufpos = Get_Cursor_Pos();
   int endbufpos = max_columns * max_rows;
   int i=0;
   ScreenChar3270* schar;
   for(i=0;i<endbufpos;i++)
   {
       bufpos = (bufpos+1)%(endbufpos);
       schar = &(screenbuf[GetRow(bufpos)][GetCol(bufpos)]);
	if(schar->IsField() && !schar->IsProtected()) 
    {
       break; 
    }
   }
return bufpos+1;
}
void ScreenBuffer3270::Start_Field_Extended(unsigned char* buffer, int length)
{
    Utility::Log_Message("ScreenBuffer3270::Start_Field_Extended");
    formatted = true;
    //*------startfied at the last column must not increment
    if(Get_Cur_Buffer_Pos() < (max_rows * max_columns - 2)) {
	screenbuf[GetRow()][GetCol()].SetFieldExtended(buffer, length);
	Set_Screen_Buffer_Address(Get_Cur_Buffer_Pos() + 1);
    }
}

int ScreenBuffer3270::Get_Cursor_Pos()
{
    return this->cursorpos;
}

void ScreenBuffer3270::Set_Cursor_Pos(int position)
{
    // Utility::Log_Message(__func__);
    std::stringstream bufferstr;
    bufferstr << "ScreenBuffer3270::Set_Cursor_Pos:" << position << std::endl;
    Utility::Log_Message(bufferstr.str());
    this->cursorpos = position;
}

bool ScreenBuffer3270::IsFieldProtected(int position)
{
    int bufpos = position;
    ScreenChar3270* schar;
    while(bufpos >= 0) {
	schar = &(screenbuf[GetRow(bufpos)][GetCol(bufpos)]);
	if(schar->IsField()) {
	    break;
	}
	bufpos--;
    }
    return schar->IsProtected();
}

void ScreenBuffer3270::LockScreen()
{
    screenlocked = true;
}

void ScreenBuffer3270::UnLockScreen()
{
    screenlocked = false;
}

bool ScreenBuffer3270::IsLocked()
{
    return screenlocked;
}

bool ScreenBuffer3270::Is_14Bit_Mode()
{
    return address14bit;
}

void ScreenBuffer3270::Set_14Bit_Mode(bool mode)
{
    address14bit = mode;
}
