//////////////////////////////////////////////////////////////////////
// This file was auto-generated by codelite's wxCrafter Plugin
// wxCrafter project file: wxcrafter.wxcp
// Do not modify this file by hand!
//////////////////////////////////////////////////////////////////////

#include "wxcrafter.h"


// Declare the bitmap loading function
extern void wxC9ED9InitBitmapResources();

static bool bBitmapLoaded = false;


Anju3270FrameBase::Anju3270FrameBase(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style)
    : wxFrame(parent, id, title, pos, size, style)
{
    if ( !bBitmapLoaded ) {
        // We need to initialise the default bitmap handler
        wxXmlResource::Get()->AddHandler(new wxBitmapXmlHandler);
        wxC9ED9InitBitmapResources();
        bBitmapLoaded = true;
    }
    
    m_menuBar_main = new wxMenuBar(0);
    this->SetMenuBar(m_menuBar_main);
    
    m_menu_host = new wxMenu();
    m_menuBar_main->Append(m_menu_host, _("Host"));
    
    m_menuItem_Connect = new wxMenuItem(m_menu_host, wxID_ANY, _("New Session"), wxT(""), wxITEM_NORMAL);
    m_menu_host->Append(m_menuItem_Connect);
    
    m_menuItem_Save = new wxMenuItem(m_menu_host, wxID_ANY, _("Save Session"), wxT(""), wxITEM_NORMAL);
    m_menu_host->Append(m_menuItem_Save);
    
    m_menuItem_Save_As = new wxMenuItem(m_menu_host, wxID_ANY, _("Save Session As"), wxT(""), wxITEM_NORMAL);
    m_menu_host->Append(m_menuItem_Save_As);
    
    m_menuItem_Close = new wxMenuItem(m_menu_host, wxID_ANY, _("Close"), wxT(""), wxITEM_NORMAL);
    m_menu_host->Append(m_menuItem_Close);
    
    m_menuItem_Open = new wxMenuItem(m_menu_host, wxID_ANY, _("Open Session"), wxT(""), wxITEM_NORMAL);
    m_menu_host->Append(m_menuItem_Open);
    
    m_menu_edit = new wxMenu();
    m_menuBar_main->Append(m_menu_edit, _("Edit"));
    
    m_menuItem_copy = new wxMenuItem(m_menu_edit, wxID_ANY, _("Copy"), wxT(""), wxITEM_NORMAL);
    m_menu_edit->Append(m_menuItem_copy);
    
    m_menuItem_cut = new wxMenuItem(m_menu_edit, wxID_ANY, _("Cut"), wxT(""), wxITEM_NORMAL);
    m_menu_edit->Append(m_menuItem_cut);
    
    m_menuItem_paste = new wxMenuItem(m_menu_edit, wxID_ANY, _("Paste"), wxT(""), wxITEM_NORMAL);
    m_menu_edit->Append(m_menuItem_paste);
    
    m_menuItem_seesion_edit = new wxMenuItem(m_menu_edit, wxID_ANY, _("Edit Session Settings"), wxT(""), wxITEM_NORMAL);
    m_menu_edit->Append(m_menuItem_seesion_edit);
    
    m_menu_macros = new wxMenu();
    m_menuBar_main->Append(m_menu_macros, _("Macros"));
    
    m_menu_start_record = new wxMenu();
    m_menu_macros->AppendSubMenu(m_menu_start_record, _("Start Recording"));
    
    m_menu_stop_record = new wxMenu();
    m_menu_macros->AppendSubMenu(m_menu_stop_record, _("Stop Recording"));
    
    m_menuItem_replay = new wxMenuItem(m_menu_macros, wxID_ANY, _("Replay"), wxT(""), wxITEM_NORMAL);
    m_menu_macros->Append(m_menuItem_replay);
    
    m_menuItem_macro_edit = new wxMenuItem(m_menu_macros, wxID_ANY, _("Edit"), wxT(""), wxITEM_NORMAL);
    m_menu_macros->Append(m_menuItem_macro_edit);
    
    m_menuItem_macro_delete = new wxMenuItem(m_menu_macros, wxID_ANY, _("Delete"), wxT(""), wxITEM_NORMAL);
    m_menu_macros->Append(m_menuItem_macro_delete);
    
    m_mainsizer = new wxBoxSizer(wxVERTICAL);
    this->SetSizer(m_mainsizer);
    
    SetBackgroundColour(wxColour(wxT("rgb(0,0,0)")));
    SetName(wxT("Anju3270FrameBase"));
    SetSizeHints(500,600);
    if ( GetSizer() ) {
         GetSizer()->Fit(this);
    }
    CentreOnParent(wxBOTH);
#if wxVERSION_NUMBER >= 2900
    if(!wxPersistenceManager::Get().Find(this)) {
        wxPersistenceManager::Get().RegisterAndRestore(this);
    } else {
        wxPersistenceManager::Get().Restore(this);
    }
#endif
    // Connect events
    m_menu_host->Connect(wxEVT_MENU_OPEN, wxMenuEventHandler(Anju3270FrameBase::On_Menu_Save_Session), NULL, this);
    this->Connect(m_menuItem_Connect->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(Anju3270FrameBase::OnConnect), NULL, this);
    this->Connect(m_menuItem_Save_As->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(Anju3270FrameBase::On_Menu_Save_Session_As), NULL, this);
    this->Connect(m_menuItem_Close->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(Anju3270FrameBase::Connection_Close), NULL, this);
    this->Connect(m_menuItem_Open->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(Anju3270FrameBase::On_Menu_Open_Session), NULL, this);
    m_menu_edit->Connect(wxEVT_MENU_OPEN, wxMenuEventHandler(Anju3270FrameBase::Edit_Session_Settings), NULL, this);
    this->Connect(m_menuItem_seesion_edit->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(Anju3270FrameBase::On_Edit_Session), NULL, this);
    
}

Anju3270FrameBase::~Anju3270FrameBase()
{
    m_menu_host->Disconnect(wxEVT_MENU_OPEN, wxMenuEventHandler(Anju3270FrameBase::On_Menu_Save_Session), NULL, this);
    this->Disconnect(m_menuItem_Connect->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(Anju3270FrameBase::OnConnect), NULL, this);
    this->Disconnect(m_menuItem_Save_As->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(Anju3270FrameBase::On_Menu_Save_Session_As), NULL, this);
    this->Disconnect(m_menuItem_Close->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(Anju3270FrameBase::Connection_Close), NULL, this);
    this->Disconnect(m_menuItem_Open->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(Anju3270FrameBase::On_Menu_Open_Session), NULL, this);
    m_menu_edit->Disconnect(wxEVT_MENU_OPEN, wxMenuEventHandler(Anju3270FrameBase::Edit_Session_Settings), NULL, this);
    this->Disconnect(m_menuItem_seesion_edit->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(Anju3270FrameBase::On_Edit_Session), NULL, this);
    
}

ConnectionSetupDialogBase::ConnectionSetupDialogBase(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style)
    : wxDialog(parent, id, title, pos, size, style)
{
    if ( !bBitmapLoaded ) {
        // We need to initialise the default bitmap handler
        wxXmlResource::Get()->AddHandler(new wxBitmapXmlHandler);
        wxC9ED9InitBitmapResources();
        bBitmapLoaded = true;
    }
    
    flexGridSizermain = new wxFlexGridSizer(6, 2, 0, 0);
    flexGridSizermain->SetFlexibleDirection( wxBOTH );
    flexGridSizermain->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
    flexGridSizermain->AddGrowableCol(0);
    flexGridSizermain->AddGrowableCol(1);
    this->SetSizer(flexGridSizermain);
    
    m_staticText133 = new wxStaticText(this, wxID_ANY, _("Host Name"), wxDefaultPosition, wxSize(-1,-1), 0);
    
    flexGridSizermain->Add(m_staticText133, 0, wxALL, 5);
    
    m_text_host = new wxTextCtrl(this, wxID_ANY, wxT(""), wxDefaultPosition, wxSize(-1,-1), 0);
    #if wxVERSION_NUMBER >= 3000
    m_text_host->SetHint(wxT(""));
    #endif
    
    flexGridSizermain->Add(m_text_host, 0, wxALL, 5);
    
    m_staticText139 = new wxStaticText(this, wxID_ANY, _("Port"), wxDefaultPosition, wxSize(-1,-1), 0);
    
    flexGridSizermain->Add(m_staticText139, 0, wxALL, 5);
    
    m_text_port = new wxTextCtrl(this, wxID_ANY, wxT(""), wxDefaultPosition, wxSize(-1,-1), 0);
    #if wxVERSION_NUMBER >= 3000
    m_text_port->SetHint(wxT(""));
    #endif
    
    flexGridSizermain->Add(m_text_port, 0, wxALL, 5);
    
    m_staticText143 = new wxStaticText(this, wxID_ANY, _("Terminal Type"), wxDefaultPosition, wxSize(-1,-1), 0);
    
    flexGridSizermain->Add(m_staticText143, 0, wxALL, 5);
    
    wxArrayString m_combo_terminal_typeArr;
    m_combo_terminal_type = new wxComboBox(this, wxID_ANY, wxT(""), wxDefaultPosition, wxSize(-1,-1), m_combo_terminal_typeArr, 0);
    #if wxVERSION_NUMBER >= 3000
    m_combo_terminal_type->SetHint(wxT(""));
    #endif
    
    flexGridSizermain->Add(m_combo_terminal_type, 0, wxALL, 5);
    
    DeviceName_t = new wxStaticText(this, wxID_ANY, _("Device Name"), wxDefaultPosition, wxSize(-1,-1), 0);
    
    flexGridSizermain->Add(DeviceName_t, 0, wxALL, 5);
    
    m_device_name = new wxTextCtrl(this, wxID_ANY, wxT(""), wxDefaultPosition, wxSize(-1,-1), 0);
    #if wxVERSION_NUMBER >= 3000
    m_device_name->SetHint(wxT(""));
    #endif
    
    flexGridSizermain->Add(m_device_name, 0, wxALL, 5);
    
    m_staticText185 = new wxStaticText(this, wxID_ANY, _("TN3270 Extended"), wxDefaultPosition, wxSize(-1,-1), 0);
    
    flexGridSizermain->Add(m_staticText185, 0, wxALL, 5);
    
    m_check_tn3270e = new wxCheckBox(this, wxID_ANY, wxT(""), wxDefaultPosition, wxSize(-1,-1), wxALIGN_RIGHT);
    m_check_tn3270e->SetValue(true);
    
    flexGridSizermain->Add(m_check_tn3270e, 0, wxALL, 5);
    
    m_button_connect = new wxButton(this, wxID_ANY, _("Connect"), wxDefaultPosition, wxSize(-1,-1), 0);
    
    flexGridSizermain->Add(m_button_connect, 0, wxALL, 5);
    
    SetName(wxT("ConnectionSetupDialogBase"));
    SetSizeHints(-1,-1);
    if ( GetSizer() ) {
         GetSizer()->Fit(this);
    }
    CentreOnParent(wxBOTH);
#if wxVERSION_NUMBER >= 2900
    if(!wxPersistenceManager::Get().Find(this)) {
        wxPersistenceManager::Get().RegisterAndRestore(this);
    } else {
        wxPersistenceManager::Get().Restore(this);
    }
#endif
    // Connect events
    m_button_connect->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(ConnectionSetupDialogBase::On_Click_3270_Connect), NULL, this);
    
}

ConnectionSetupDialogBase::~ConnectionSetupDialogBase()
{
    m_button_connect->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(ConnectionSetupDialogBase::On_Click_3270_Connect), NULL, this);
    
}
