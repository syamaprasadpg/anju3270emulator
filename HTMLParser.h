#ifndef HTML_PARSER_H
#define HTML_PARSER_H
//#include <string.h>
#include <sstream>
//#include "Session3270.h"
#include "ScreenBuffer3270.h"

#define APPENDHTML(ptr,val) { \
		memcpy (ptr, val, strlen( (char*) val ) );\
		ptr += strlen( (char*) val );\
    }
class HTMLParser {
public:
	/*static int Convert_3270_Screen_to_HTML(unsigned char* buf3270, unsigned char* bufhtml,
	int rows,int cols,
	int htmlbufsize);*/
static std::string Convert_3270_Screen_to_HTML( ScreenBuffer3270* scrnbuf);
};
#endif