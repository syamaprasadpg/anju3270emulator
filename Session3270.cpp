#include "Session3270.h"

// bit bucket upload-test

Session3270::Session3270(wxFrame* parent, std::string hst, std::string prt, std::string ttype, bool tn3270eflag)

{
    //Utility::Log_Message(__func__);
    LOGPRINTF("Session intialising");
    this->support_TN3270E = tn3270eflag;

    this->Host = hst;

    this->Port = prt;

    this->SetTerminalType(ttype);

    Scr_Buffer = new ScreenBuffer3270(GetScreenHeight(), GetScreenWidth());

    Screen_Background_Colour = wxTheColourDatabase->Find(
        "BLACK");

    isTN3270E = false;

    // support_TN3270E= false;

    // raw_data_mode = false;

    // Screen_Width = 80;

    // Screen_Height = 32;

    //Err_Flag = false;

    //Err_Message = "";

    exitflg = false;

    m_parent = parent;

    Command_Length = 0;

    //	obuf.resize ( OUT_BUF_MAX );

    //	obufsize=0;

    device_name.resize(DEVICE_NAME_LENGTH);

    wxInitialize();
}

Session3270::~Session3270()

{
    //Utility::Log_Message(__func__);
    LOGPRINTF("Session Ended");
    sockConn->Close();

    delete sockConn;

    wxUninitialize();
}

void Session3270::Connect_To_Host()

{
   // Utility::Log_Message(__func__);
    LOGPRINTF("Connecting to host");
    sockConn = new wxSocketClient(wxSOCKET_WAITALL);

    wxIPV4address addr;

    addr.Hostname(Host);

    addr.Service(wxAtoi(Port));
    LOGPRINTF(
        "Socket Connecting to HOST: " << Host << " Port: " << Port);

    sockConn->Connect(addr, true);

    if(!(sockConn->IsConnected()))

        {
            LOGPRINTF("Connect failed.....");

            if(sockConn->Error())

                {
                    LOGPRINTF("Session3270::Socket Error is: " << sockConn->LastError());
                }

            Raise_GUI_Event(CONNECT_3270_ERROR);
            return;
        }

    Raise_GUI_Event(CONNECT_3270_SUCCESS);

    return;
}

void* Session3270::Entry()

{
    LOGPRINTF("Thread Starting");

    sockConn->SetFlags(wxSOCKET_NONE);

    int pret = 0;

    try

        {
            while(1)

                {
                    Command_Length = Sock_Read(&Command_Buffer, COMMAND_BUF_MAX);  // try to read 100 bytes

                    // Command_Length = sockConn->LastCount();

                    if(Command_Length == 0)  // Not possible but if that happens

                        {
                            if(sockConn->Error())

                                {
                                    LOGPRINTF(
                                        "Session3270::Socket Error is: " << sockConn->LastError());

                                    Raise_GUI_Event(TN3270_SOCKET_READ_ERROR);
                                }

                            break;
                        }

                    if(Command_Buffer[0] == IAC)  // telnet command

                        {
                            pret = Parse_Telnet_Command();
                        }

                    else
                        {
                            pret = Parse_3270_DataStream();
                        }

                    if(pret != 0)

                        {
                            Raise_GUI_Event(pret);

                            break;
                        }

                    if(sockConn->Error())

                        {
                            break;
                        }
                }
        }

    catch(std::exception& excptn)

        {
            Raise_GUI_Event(TN3270_LOGIC_ERROR);
        }

    sockConn->Close();

    Exit(0);

    return NULL;
}

int Session3270::Parse_Telnet_Command()

{
    LOGPRINTF("Telnet command parsing started");
    int i = 0;

    while(i < Command_Length)

        {
            // ARE YOU A TERMINAL?

            if(memcmp(Command_Buffer + i, IAC_DO_TERMINAL, sizeof IAC_DO_TERMINAL) == 0)

                {
                    LOGPRINTF(
                        "Host=>DO TERMINAL");
                    LOGPRINTF(
                        "Client=>WILL TERMINAL");
                    Sock_Send(RESP_WILL_TERMINAL, 3);
                    i = i + sizeof IAC_DO_TERMINAL;
                }

            else if(memcmp(Command_Buffer + i, IAC_WILL_BINARY, sizeof IAC_WILL_BINARY) == 0)

                {
                    LOGPRINTF(
                        "Host=>WILL BINARY");
                    LOGPRINTF(
                        "Client=>DO BINARY");
                    Sock_Send(IAC_DO_BINARY, 3);

                    i = i + sizeof IAC_WILL_BINARY;
                }

            else if(memcmp(Command_Buffer + i, IAC_DO_BINARY, sizeof IAC_DO_BINARY) == 0)

                {
                    LOGPRINTF(
                        "Host=>DO BINARY");
                    LOGPRINTF(
                        "Client=>WILL BINARY");
                    Sock_Send(IAC_WILL_BINARY, 3);

                    i = i + sizeof IAC_DO_BINARY;
                }

            else if(memcmp(Command_Buffer + i, IAC_WILL_EOR, sizeof IAC_WILL_EOR) == 0)

                {
                    LOGPRINTF("Host=>WILL EOR");
                    LOGPRINTF("Client=>DO EOR");
                    Sock_Send(IAC_DO_EOR, 3);

                    i = i + sizeof IAC_WILL_EOR;
                }

            else if(memcmp(Command_Buffer + i, IAC_DO_EOR, sizeof IAC_DO_EOR) == 0)

                {
                    LOGPRINTF("Host=>DO EOR");
                    LOGPRINTF("Client=>WILL EOR");
                    Sock_Send(IAC_WILL_EOR, 3);

                    i = i + sizeof IAC_DO_EOR;
                }

            else if(memcmp(Command_Buffer + i, IAC_DO_TN3270E, sizeof IAC_WILL_TN3270E) == 0)

                {
                    LOGPRINTF("Host=>DO TN3270E");
                    
                    if(support_TN3270E)

                        {
                            LOGPRINTF("Client=>WILL TN3270E");
                            Sock_Send(IAC_WILL_TN3270E, 3);

                            isTN3270E = true;
                        }

                    else

                        {
                            LOGPRINTF("Client=>WONT TN3270E");
                            Sock_Send(IAC_WONT_TN3270E, 3);

                            isTN3270E = false;
                        }

                    i = i + sizeof IAC_DO_TN3270E;
                }

            else if(memcmp(Command_Buffer + i, IAC_SB_TN3270E_SEND_DEVICE, sizeof IAC_SB_TN3270E_SEND_DEVICE) == 0)

                {
                    LOGPRINTF("Host=>SB TN3270E SEND DEVICE");
                    
                    // Sock_Send ( IAC_SB_TN3270E_SEND_DEVICE, 5 );

                    unsigned char send_resp[20];

                    unsigned char* resptr;

                    int resptln = 0;

                    resptr = send_resp;

                    memcpy(resptr, SB_TN3270E_DEVICE_TYPE_REQUEST, sizeof SB_TN3270E_DEVICE_TYPE_REQUEST);

                    resptr += sizeof SB_TN3270E_DEVICE_TYPE_REQUEST;

                    memcpy(resptr, Terminal_Type.data(), Terminal_Type.length());

                    resptr += Terminal_Type.length();

                    if(isTN3270E)

                        {
                            *resptr = '-';
                            resptr++;
                            *resptr = 'E';
                            resptr++;
                            LOGPRINTF("Client=>SB TN3270E DEVICE TYPE REQUEST "<< Terminal_Type<<"-E");
                        }
                        else
                        {
                            LOGPRINTF("Client=>SB TN3270E DEVICE TYPE REQUEST "<< Terminal_Type);
                        }

                    unsigned char IACSE[2] = {IAC, SE};

                    memcpy(resptr, IACSE, sizeof IACSE);

                    resptr += sizeof IACSE;

                    resptln = (int)(resptr - send_resp);

                    Sock_Send(send_resp, resptln);

                    i = i + sizeof IAC_SB_TN3270E_SEND_DEVICE;
                }

            // now parse fffa280802fff0 is this IAC SB TN3270E SEND DEVICE-TYPE IAC SE

            // parse fffa28020449424d2d333237392d32015443504130303034fff0   too.

            // IAC SB TN3270E SEND TN3270_DEVICE IS

            /*

         *      Server:  IAC DO TN3270E

                Client:  IAC WILL TN3270E

                Server:  IAC SB TN3270E SEND DEVICE-TYPE IAC SE

                Client:  IAC SB TN3270E DEVICE-TYPE REQUEST IBM-3278-5-E

                                CONNECT myterm IAC SE

                Server:  IAC SB TN3270E DEVICE-TYPE IS IBM-3278-5-E CONNECT

                                myterm IAC SE

                Client:  IAC SB TN3270E FUNCTIONS REQUEST RESPONSES

                                BIND-IMAGE IAC SE

                Server:  IAC SB TN3270E FUNCTIONS IS RESPONSES BIND-IMAGE

                                IAC SE

                                                         * next example

                                                         *         Server:  IAC DO TN3270E

                Client:  IAC WILL TN3270E

                Server:  IAC SB TN3270E SEND DEVICE-TYPE IAC SE

                Client:  IAC SB TN3270E DEVICE-TYPE REQUEST IBM-3278-2 IAC SE

                Server:  IAC SB TN3270E DEVICE-TYPE IS IBM-3278-2 CONNECT

                                anyterm IAC SE

                Client:  IAC SB TN3270E FUNCTIONS REQUEST RESPONSES IAC SE

                Server:  IAC SB TN3270E FUNCTIONS IS RESPONSES IAC SE

                   (3270 data stream is exchanged)



           //ACCEPT fffa28030402fff0 TOO

         * */
            else if(memcmp(Command_Buffer + i, IAC_SB_TN3270E_REJECT_DEVICE, sizeof IAC_SB_TN3270E_REJECT_DEVICE) == 0)
            {
                int reason = (int)(unsigned char)*(Command_Buffer+i+sizeof IAC_SB_TN3270E_REJECT_DEVICE);
                LOGPRINTF("Host=>SB TN3270E REJECT DEVICE REASON :" << TN3270E_REASON_CODES[reason] );
                return TN3270_INVALID_COMMAND;
            }
            else if(memcmp(Command_Buffer + i, SB_TN3270E_DEVICE_IS, sizeof SB_TN3270E_DEVICE_IS) == 0)

                {
                    int obtptr = 0;  // device_name.size();

                    bool deviceflg = false;

                    i += sizeof SB_TN3270E_DEVICE_IS;

                    while(Command_Buffer[i] != IAC || Command_Buffer[i + 1] != SE)

                        {
                            if(deviceflg)

                                {
                                    device_name[obtptr++] = Command_Buffer[i];
                                }

                            if(Command_Buffer[i] == TN3270E_CONNECT)

                                {
                                    deviceflg = true;
                                }

                            i++;
                        }

                    device_name[obtptr] = 0x00;

                    i = i + 2;  // for IAC SE

                    //*----------------send message to stream ----

                    std::stringstream strbuffer;

                    strbuffer << "Session3270::TN3270E device name is " << (char*)&device_name[0] << std::endl;

                    Utility::Log_Message(strbuffer.str());

                    //*-------------------------------------------

                    Sock_Send(TN3270E_FUNCTIONS_REQUEST, sizeof TN3270E_FUNCTIONS_REQUEST);
                }

            else if(memcmp(Command_Buffer + i, TN3270E_FUNCTIONS_RESPONSE, sizeof TN3270E_FUNCTIONS_RESPONSE) == 0)

                {
                    i = i + sizeof TN3270E_FUNCTIONS_RESPONSE;
                }

            else if(memcmp(Command_Buffer + i, SB_SEND_TERMINAL, sizeof SB_SEND_TERMINAL) == 0)

                {
                    // send terminal type now.

                    unsigned char send_resp[20];

                    unsigned char* resptr;

                    int resptln = 0;

                    resptr = send_resp;

                    memcpy(resptr, SB_IS_TERMINAL, sizeof SB_IS_TERMINAL);

                    resptr += sizeof SB_IS_TERMINAL;

                    memcpy(resptr, Terminal_Type.data(), Terminal_Type.length());

                    resptr += Terminal_Type.length();

                    unsigned char IACSE[2] = {IAC, SE};

                    memcpy(resptr, IACSE, sizeof IACSE);

                    resptr += sizeof IACSE;

                    resptln = (int)(resptr - send_resp);

                    Sock_Send(send_resp, resptln);

                    // wxLogMessage ( "Sending terminal info" );

                    i = i + sizeof SB_SEND_TERMINAL;
                }

            else

                {
                    return TN3270_INVALID_COMMAND;  // invalid command
                }
        }

    return 0;
}

void Session3270::Raise_GUI_Event(int event_num, int event_id)

{
    //Utility::Log_Message(__func__);
    LOGPRINTF("GUI event raised:"<<event_num);
    wxCommandEvent event(wxEVT_COMMAND_MENU_SELECTED, ID_3270_CALLBACK);

    event.SetInt(event_num);

    event.SetExtraLong(event_id);

    m_parent->GetEventHandler()->AddPendingEvent(event);

    }

wxString Session3270::Get_Command_Buffer()

{
   
    return Utility::Get_Hex_String(Command_Buffer, Command_Length);
}

int Session3270::Parse_3270_DataStream()

{
   // Utility::Log_Message(__func__);
LOGPRINTF("3270 Datastream parsing started");
    int i = 0;

    // read chapter 8 of RFC 2355 https://tools.ietf.org/html/rfc2355#page-20

    // unsigned char telent_cmd;

    Remove_Double_Telnet_Escape();

    if(isTN3270E)

        {
            //	SeqNum[0] = Command_Buffer[3] ;

            //	SeqNum[1] = Command_Buffer[4] ;

            // https://tools.ietf.org/html/rfc2355 refer to 10.4.1 Response Message:

            memcpy(SeqNum, Command_Buffer + 3, 2);

            if(Command_Buffer[DATA_TYPE] == DATA_3270 && Command_Buffer[RESPONSE_FLAG] == ALWAYS_RESPONSE)

                {
                    Send_Response_Command();
                }

            i = 5;
        }

    while(i < Command_Length)

        {
            // wxLogDebug ( "inside Parse_3270_DataStream i=%d", i );

            // Utility::Log_Message(" CMD_W ");

            std::stringstream bufferstr;

            bufferstr << "Parsing command word at position " << i << " " << (int)Command_Buffer[i] << std::endl;

            Utility::Log_Message(bufferstr.str());

            switch(Command_Buffer[i])

                {
                case CMD_W:  // x01

                case SNA_CMD_W:

                    Utility::Log_Message(
                        " CMD_W ");

                    i += CMD_Write(Command_Buffer + i, Command_Length - i);  //	X'01'	//Write to a character buffer.

                    break;

                case CMD_RB:  // = 0x02,	/* read buffer */

                case SNA_CMD_RB:

                    Utility::Log_Message(
                        " CMD_RB ");

                    i += Read_Buffer();  //	X'02'	//Read the entire character buffer.

                    break;

                case CMD_NOP:  //= 0x03,	/* no-op */

                    //			case SNA_CMD_NOP:

                    Utility::Log_Message(
                        " CMD_NOP ");

                    i += No_Op();

                    break;

                case CMD_EW:  //= 0x05,	/* erase/write */

                case SNA_CMD_EW:

                    Utility::Log_Message(
                        " CMD_EW ");

                    i += Erase_write(Command_Buffer + i,

                                     Command_Length - i);  // X'05'	//Erase and then write to a character buffer.

                    break;

                case CMD_RM:  //= 0x06,	/* read modified */

                case SNA_CMD_RM:

                    Utility::Log_Message(
                        " CMD_RM ");

                    i += Read_Modified();  //	X'06'	//Read only the modified data from the character buffer (some

                    // exceptions).

                    break;

                case CMD_EWA:  //= 0x0d,	/* erase/write alternate */

                case SNA_CMD_EWA:

                    LOGPRINTF(" CMD_EWA ");

                    i += Erase_Write_Alternate(Command_Buffer + i,Command_Length - i);  // X'0d'	//Erase and then write to the alternate size character buffer.

                    break;

                case CMD_RMA:  //= 0x0e,	/* read modified all */

                case SNA_CMD_RMA:

                    LOGPRINTF(" CMD_RMA ");

                    i += Read_Modified_All();  // X'0e'	//Read all the modified data from the character buffer (no exceptions).

                    break;

                case CMD_EAU:  //= 0x0f,	/* erase all unprotected */

                case SNA_CMD_EAU:

                    Utility::Log_Message(
                        " CMD_EAU ");

                    i = i + Erase_All_Unprotected();  // X'0f'	//Erase the unprotected data from the character buffer.

                    break;

                case SNA_CMD_WSF:

                case CMD_WSF:  // = 0x11	/* write structured field */

                    Utility::Log_Message(
                        " CMD_WSF ");

                    i += Write_Structured_Field(Command_Buffer + i, Command_Length - i);  //	X'11'	//Write a structured field.

                    break;

                default:

                    Utility::Log_Message(
                        " Invalid Datastream Error");

                    Raise_GUI_Event(TN3270_INVALID_DATASTREAM);

                    return -1;

                    break;

                    /*	 */

                    // Copy_Buffer()	   //X'f7'	 	 	//Copy the contents of character buffer A to character

                    // buffer

                    // B.

                    // break;
                }
        }

    return 0;
}

int Session3270::Sock_Read(void* buffer, int size)

{
    int length = 0, partlen = 0;

    do

        {
            if(exitflg)  // thread exiting

                {
                    length = 0;

                    break;
                }

            // Utility::Log_Message ( "Sock_Read " );// possible loop and memory leak

            sockConn->Read(((char*)buffer) + length, size);

            partlen = sockConn->LastCount();

            if(partlen <= 0)

                {
                    // break;

                    Sleep(1000);  // no activity so sleep and comeback
                }

            length = length + partlen;
        }

    while(*((unsigned char*)buffer) != IAC && *((unsigned char*)(buffer)+length - 1) != EORMARK);

    Utility::Log_Message(
        "Sock_Read ");

    Utility::Log_Hex_String((unsigned char*)buffer, length);

    return length;
}

void Session3270::Sock_Send(void* msg, int size)

{
    Utility::Log_Message(__func__);

    Utility::Log_Hex_String((unsigned char*)msg, size);

    sockConn->Write(msg, size);
}

int Session3270::Erase_All_Unprotected()

{
    Utility::Log_Message(__func__);

    return 1;
}

int Session3270::Erase_Write_Alternate(unsigned char* cmdbuf, int len)

{
    //Utility::Log_Message(__func__);

    //return 2 + Process_Data_Stream(Command_Buffer + 2, Command_Length - 2);
     LOGPRINTF("Command of length "<<len<<" received");
    Scr_Buffer->Clear_Screen();

    Raise_GUI_Event(TN3270_REFRESH_SCREEN);

    return 2 + Process_Data_Stream(cmdbuf + 2, len - 2);

}

int Session3270::Order_Erase_Unprotected_to_Address(unsigned char* buffer)

{
   // Utility::Log_Message(__func__);
LOGPRINTF("Unimplemented function");
    return 1;
}

int Session3270::Order_Graphic_Escape(unsigned char* buffer)

{
    //Utility::Log_Message(__func__);
    LOGPRINTF("Unimplemented function");

    return 1;
}

int Session3270::Order_Insert_Cursor(unsigned char* buffer)

{
    //Utility::Log_Message(__func__);
   LOGPRINTF("Cursor position set");
    //   Raise_GUI_Event(TN3270_INSERT_CURSOR);

    Scr_Buffer->Set_Cursor_Pos(Scr_Buffer->Get_Cur_Buffer_Pos());

    return 1;
}

int Session3270::Order_Modify_Field(unsigned char* buffer)

{
   // Utility::Log_Message(__func__);
   LOGPRINTF("Unimplemented function");
    return 1;
}

int Session3270::Order_Program_Tab(unsigned char* buffer)

{
   // Utility::Log_Message(__func__);
LOGPRINTF("Program tab processing");
    // this->Scr_Buffer.

    Scr_Buffer->Set_Screen_Buffer_Address(Scr_Buffer->Get_Next_Field_Pos());

    return 1;
}

int Session3270::Order_Repeat_Address(unsigned char* buffer)

{
   // Utility::Log_Message(__func__);
 LOGPRINTF("Unimplemented function");
    return 1;
}

int Session3270::Order_Set_Attribute(unsigned char* buffer)

{
    //Utility::Log_Message(__func__);
LOGPRINTF("Unimplemented function");
    return 1;
}

int Session3270::Order_Set_Buffer_Address(unsigned char* buffer)

{
    //Utility::Log_Message(__func__);
     LOGPRINTF("Setting buffer address");
    unsigned int bufferadr;

    buffer++;  // skip the order byte

    if(*buffer & 0xC0)  //<=is 12 bit

        {
            GET12BITADR(bufferadr, buffer);
        }

    else

        {
            GET14BITADR(bufferadr, buffer);

            // Raise_GUI_Event(TN3270_14BIT_ADR);

            Scr_Buffer->Set_14Bit_Mode();
        }

    wxASSERT(bufferadr >= 0);

    //*----------------send message to stream ----

   // std::stringstream strbuffer;

    LOGPRINTF("Session3270::Order_Set_Buffer_Address:" << bufferadr);

   // Utility::Log_Message(strbuffer.str());

    //*-------------------------------------------

    // unsigend char*

    // Raise_GUI_Event(TN3270_SET_BUFFER_ADR, bufferadr);

    Scr_Buffer->Set_Screen_Buffer_Address(bufferadr);

    return 3;  // return the length
}

int Session3270::Order_Start_Field(unsigned char* buffer)

{
    //Utility::Log_Message(__func__);
    LOGPRINTF("Starting a new field");
    //*----------------send message to stream ----

    // std::stringstream strbuffer;

    // strbuffer << "Session3270::Order_Start_Field" << std::endl;

    // Utility::Log_Message ( strbuffer.str() );

    //*---------------------------------------

    //  Raise_GUI_Event(TN3270_START_FIELD, (int)*(buffer + 1));

    Scr_Buffer->Start_Field(*(buffer + 1));

    return 2;  // need to work on it further

    // return 1+Order_Set_Buffer_Address(buffer);
}

int Session3270::Order_Start_Field_Extended(unsigned char* buffer)

{
    //*----------------send message to stream ----

    // std::stringstream strbuffer;

    // strbuffer << "Session3270::Order_Start_Field_Extended" << std::endl;

    // Utility::Log_Message ( strbuffer.str() );

   // Utility::Log_Message(__func__);
LOGPRINTF("Starting an extended field");
    //*---------------------------------------

    buffer++;  // skip order

    int numofpairs = (int)(*buffer);

    int i = 0, atrsize = numofpairs * 2;

    unsigned char* attributes, *tmpatr;

    buffer++;

    // memcpy(((unsigned char*)&numofpairs)+1,buffer,1);

    attributes = (unsigned char*)malloc(atrsize);

    // tmpatr = attributes + 4;

    // memcpy(attributes, &atrsize, sizeof(unsigned int));

    memcpy(attributes, buffer, atrsize);

    // Raise_GUI_Event(SET_EXTENDED_FIELD_ATTRIBUTE, (int)attributes);

    //---*

    // bufferstr << "Anju3270Frame::On3270Callback SET_EXTENDED_FIELD_ATTRIBUTE"  <<std::endl;

    //Utility::Log_Message(bufferstr.str());

    //unsigned char* buffer;

    //buffer = ((unsigned char*) event.GetExtraLong())+4;

    //int length;

    //length= *((int*)((unsigned char*) event.GetExtraLong()));

    Scr_Buffer->Start_Field_Extended(attributes, atrsize);

    free(attributes);

    //---*

    // atrsize

    //

    //

    //	for(i=0;i<numofpairs;i++)

    //{

    // Raise_GUI_Event (SET_EXTENDED_FIELD_ATTRIBUTE, ( int ) * ( buffer + 1 ) );

    //	buffer+=2;

    //}

    return 2 + numofpairs * 2;

    // int i=1;

    // Order_Program_Tab(buffer+i);
}

int Session3270::Order_Yale(unsigned char* buffer)

{
   // Utility::Log_Message(__func__);
   LOGPRINTF("Unimplemented function");
    return 1;
}

int Session3270::Process_Data_Stream(unsigned char* buffer, int size)

{
   // Utility::Log_Message(__func__);
 LOGPRINTF("Processing a data stream of size:"<<size);
    int i = 0;

    // i+=Order_Program_Tab(buffer+i);

    unsigned char data;

    while(i < size)

        {
            data = *(buffer + i);

            switch(data)

                {
                case ORDER_PT:  //= 0x05,	/* program tab */

                    i += Order_Program_Tab(buffer + i);

                    break;

                case ORDER_GE:  //= 0x08,	/* graphic escape */

                    i += Order_Graphic_Escape(buffer + i);

                    break;

                case ORDER_SBA:  //= 0x11,	/* set buffer address */

                    i += Order_Set_Buffer_Address(buffer + i);

                    break;

                case ORDER_EUA:  //= 0x12,	/* erase unprotected to address */

                    i += Order_Erase_Unprotected_to_Address(buffer + i);

                    break;

                case ORDER_IC:  //= 0x13,	/* insert cursor */

                    i += Order_Insert_Cursor(buffer + i);

                    break;

                case ORDER_SF:  //= 0x1d,	/* start field */

                    i += Order_Start_Field(buffer + i);

                    break;

                case ORDER_SA:  //= 0x28,	/* set attribute */

                    i += Order_Set_Attribute(buffer + i);

                    break;

                case ORDER_SFE:  //= 0x29,	/* start field extended */

                    i += Order_Start_Field_Extended(buffer + i);

                    break;

                case ORDER_YALE:  //= 0x2b,	/* Yale sub command */

                    i += Order_Yale(buffer + i);

                    break;

                case ORDER_MF:  //= 0x2c,	/* modify field */

                    i += Order_Modify_Field(buffer + i);

                    break;

                case ORDER_RA:  // = 0x3c	/* repeat to address */

                    i += Order_Repeat_Address(buffer + i);

                    break;

                // The end of record is received. this is a telnet option

                case IAC:

                    i += EOR_Received(buffer + i);  // FFEF 255 and 239

                    Raise_GUI_Event(TN3270_REFRESH_SCREEN);

                    return i;

                    break;

                default:

                    i += Process_Raw_Data(buffer + i);

                    break;
                }

            wxLogDebug(
                "inside Process_Data_Stream i=%d", i);
        }

    return i;
}

int Session3270::Erase_write(unsigned char* cmdbuf, int len)

{
    //Utility::Log_Message(__func__);
    LOGPRINTF("Command of length "<<len<<" received");
    Scr_Buffer->Clear_Screen();

    Raise_GUI_Event(TN3270_REFRESH_SCREEN);

    return 2 + Process_Data_Stream(cmdbuf + 2, len - 2);
}

int Session3270::No_Op()

{
    //Utility::Log_Message(__func__);
    LOGPRINTF("No Op received");
    return 1;
}

int Session3270::Read_Buffer()

{
    //Utility::Log_Message(__func__);
    LOGPRINTF("Starting a buffer read");
    //Raise_GUI_Event(TN3270_GET_SCREEN_BUFFER);

    Send_Screen_Buffer(GET_AID("AID_NO"));

    return 1 + Process_Data_Stream(Command_Buffer + 1, Command_Length - 1);  // no wcc
}

int Session3270::Read_Modified()

{
 //   Utility::Log_Message(__func__);
LOGPRINTF("Unimplemented function");
    return 1;
}

int Session3270::Read_Modified_All()

{
  //  Utility::Log_Message(__func__);
LOGPRINTF("Unimplemented function");
    return 1;
}

int Session3270::CMD_Write(unsigned char* cmdbuf, int len)

{
   // Utility::Log_Message(__func__);
   LOGPRINTF("New command of length "<< len << " received");
    return 2 + Process_Data_Stream(cmdbuf + 2, len - 2);

    // return 1;
}

/*

int Session3270::Write_Structured_Field(unsigned char* buffer)

{

                //----------------send message to stream ----

        std::stringstream strbuffer;

    strbuffer << "Session3270::Order_Write_Structured_Field"<<std::endl;

        Utility::Log_Message(strbuffer.str());

//-------------------------

/-----

 *

 * http://publibz.boulder.ibm.com/cgi-bin/bookmgr_OS390/BOOKS/CN7P4000/5.1?SHELF=&DT=19920626112004&CASE=

 * http://publibz.boulder.ibm.com/cgi-bin/bookmgr_OS390/BOOKS/CN7P4000/5.1?SHELF=&DT=19920626112004&CASE=

 * page 140 of datastream pdf

 *

        return 1;

}*/

int Session3270::EOR_Received(unsigned char* buffer)

{
   // Utility::Log_Message(__func__);
LOGPRINTF("End of transmisiion");
    return 2;  // ffef length 2
}

int Session3270::Process_Raw_Data(unsigned char* buffer)

{
   // Utility::Log_Message(__func__);
LOGPRINTF("Processing raw data");
    unsigned int i = 0;

    unsigned char* bufmax = Command_Buffer + Command_Length;

    unsigned char* bufout;  //, *tmpbuf;

    bufout = (unsigned char*)malloc(COMMAND_BUF_MAX);

    // tmpbuf = bufout + 4; // for the length

    while(!Is3270Order(*(buffer + i)) && *(buffer + i) != IAC && buffer + i < bufmax)

        {
            *(bufout + i) = *(buffer + i);

            i++;
        }

    // memcpy(bufout, &i, sizeof(unsigned int));

    // Raise_GUI_Event(TN3270_RAW_DATA, (int)bufout);

    //*

    Utility::EBCDIC_to_ASCII(i, bufout, bufout);

    Scr_Buffer->Update_Screen_Buffer(bufout, i, false);

    //*

    free(bufout);

    return i;
}

bool Session3270::Is3270Order(unsigned char data)

{
    // Utility::Log_Message(__func__);

    if(data != ORDER_PT &&  // = 0x05,	/* program tab */

       data != ORDER_GE &&  //= 0x08,	/* graphic escape */

       data != ORDER_SBA &&  //= 0x11,	/* set buffer address */

       data != ORDER_EUA &&  //= 0x12,	/* erase unprotected to address */

       data != ORDER_IC &&  //= 0x13,	/* insert cursor */

       data != ORDER_SF &&  //= 0x1d,	/* start field */

       data != ORDER_SA &&  //= 0x28,	/* set attribute */

       data != ORDER_SFE &&  //= 0x29,	/* start field extended */

       data != ORDER_YALE &&  //= 0x2b,	/* Yale sub command */

       data != ORDER_MF &&  //= 0x2c,	/* modify field */

       data != ORDER_RA)  //= 0x3c	/* repeat to address */

        {
            return false;
        }

    else

        {
            return true;
        }
}


 unsigned char Session3270::supported_replies[] =

    {

     QR_SUMMARY, /* 0x80 */

     QR_USABLE_AREA, /* 0x81 */

     // QR_ALPHA_PART,		/* 0x84 */

     QR_CHARSETS, /* 0x85 */

     QR_COLOR, /* 0x86 */

     QR_HIGHLIGHTING, /* 0x87 */

     QR_REPLY_MODES, /* 0x88 */

     QR_IMP_PART, /* 0xa6 */

     //	QR_DDM,			/* 0x95 */

};

#define NSR int(sizeof(supported_replies) / sizeof(unsigned char))

// https://stuff.mit.edu/afs/sipb/project/source-sipb/source/third/x3270/sf.c

/*

 * Process a 3270 Write Structured Field command

 */

int Session3270::Write_Structured_Field(unsigned char* buf, int len)

{
    //Utility::Log_Message(__func__);
LOGPRINTF("Writing structured field of length:"<<len);
    int buflen = len - 2;  // remove iac and eor

    // int buflen = Command_Length - 3; // IAC + WSF + ....+IAC +EOR 1 byte at the beginning and 2 at the end

    int retlen = buflen + 2;

    unsigned short fieldlen;

    unsigned char* cp = buf;

    // bool first = true;

    /* Skip the WSF command itself. */

    cp++;

    buflen--;

    /* Interpret fields. */

    while(buflen > 0)

        {
            /*	if (first)

                        trace_ds(" ");

                else

                        trace_ds("< WriteStructuredField ");

                first = False;*/

            /* Pick out the field length. */

            if(buflen < 2)

                {
                    // trace_ds("error: single byte at end of message\n");

                    Raise_GUI_Event(TN3270_INVALID_WSF);

                    return retlen;
                }

            fieldlen = (cp[0] << 8) + int(cp[1]);

            LOGPRINTF(
                "WSF buflen is " << buflen << " fieldlen is " << fieldlen << " field value is " << (unsigned int)cp[0] << " " << (unsigned int)cp[1] << "\n");

            if(fieldlen == 0)

                fieldlen = buflen;

            if(fieldlen < 3)

                {
                    Raise_GUI_Event(TN3270_INVALID_WSF);

                    return retlen;
                }

            if((int)fieldlen > buflen)

                {
                    Raise_GUI_Event(TN3270_INVALID_WSF);

                    return retlen;
                }

            /* Dispatch on the ID. */

            LOGPRINTF(
                "WSF Command is " << (int)(cp[2]));

            switch(cp[2])

                {
                case SF_READ_PART:

                    // trace_ds("ReadPartition");

                    if(!SF_Read_Part(cp, (int)fieldlen))

                        {
                            return retlen;
                        }

                    break;

                case SF_ERASE_RESET:

                    // trace_ds("EraseReset");

                    if(!SF_Erase_Reset(cp, (int)fieldlen))

                        {
                            return retlen;
                        }

                    break;

                case SF_SET_REPLY_MODE:

                    // trace_ds("SetReplyMode");

                    if(!SF_Set_Reply_Mode(cp, (int)fieldlen))

                        {
                            return retlen;
                        }

                    break;

                case SF_OUTBOUND_DS:

                    // trace_ds("OutboundDS");

                    if(!SF_Outbound_DS(cp, (int)fieldlen))

                        {
                            return retlen;
                        }

                    break;

                case SF_TRANSFER_DATA: /* File transfer data         */

                    // trace_ds("FileTransferData");

                    //	FT_DFT_data(cp, (int)fieldlen);

                    break;

                default:

                    // trace_ds("unsupported ID 0x%02x\n", cp[2]);

                    break;
                }

            /* Skip to the next field. */

            cp += fieldlen;

            buflen -= fieldlen;
        }

    /*if (first)

            trace_ds(" (null)\n");*/

    return retlen;
}

bool Session3270::SF_Read_Part(unsigned char* buf, int buflen)

{
  //  Utility::Log_Message(__func__);
LOGPRINTF("Reading primary partition");
    // unsigned char buf[];

    // int buflen;

    unsigned char* querybuf;

    int offset = 0;

    querybuf = (unsigned char*)malloc(1000);

    unsigned char partition;

    int i;

    int any = 0;

    char comma = ' ';

    if(buflen < 5)

        {
            //	trace_ds(" error: field length %d too small\n", buflen);

            Raise_GUI_Event(TN3270_INVALID_WSF);

            return false;
        }

    LOGPRINTF(
        "Read Partition " << (int)buf[3] << " "

                          << " Command " << (int)buf[4])

    partition = buf[3];

    // trace_ds("(0x%02x)", partition);

    // bug here. since x'ff' is a telnet character , if partion is x'ff' it will be doubled.

    // refer https://www.wireshark.org/lists/wireshark-bugs/201204/msg00198.html

    switch(buf[4])

        {
        case SF_RP_QUERY:

            if(!process_SF_RP_QUERY(partition))

                {
                    return false;
                }

            break;

        case SF_RP_QLIST:

            if(buflen < 6)

                {
                    // trace_ds("error: missing request type\n");

                    Raise_GUI_Event(TN3270_INVALID_WSF);

                    return false;

                    // return;		/* error */
                }

            if(!process_SF_RP_QLIST(partition, buf, buflen))

                {
                    return false;
                }

            break;

        // LOGPRINTF("SF_RP_QLIST");

        // trace_ds(" QueryList ");

        // Query_Reply_Start();

        //  obuf.push_back ( AID_SF );

        //  switch(buf[5]) {

        //  }

        case SNA_CMD_RMA:

            //	trace_ds(" ReadModifiedAll");

            if(partition != 0x00)

                {
                    // trace_ds(" error: illegal partition\n");

                    // return;

                    Raise_GUI_Event(TN3270_INVALID_WSF);

                    return false;
                }

            // trace_ds("\n");

            //			ctlr_read_modified ( AID_QREPLY, true );

            //Raise_GUI_Event(TN3270_GET_SCREEN_BUFFER, (int)AID_QREPLY);

            Send_Screen_Buffer(GET_AID(
                "AID_QREPLY"));

            break;

        case SNA_CMD_RB:

            // trace_ds(" ReadBuffer");

            if(partition != 0x00)

                {
                    // trace_ds(" error: illegal partition\n");

                    Raise_GUI_Event(TN3270_INVALID_WSF);

                    return false;

                    //	return;
                }

            //	trace_ds("\n");

            //	ctlr_read_buffer ( AID_QREPLY );

            // Raise_GUI_Event(TN3270_GET_SCREEN_BUFFER, (int)AID_QREPLY);

            Send_Screen_Buffer(GET_AID(
                "AID_QREPLY"));

            break;

        case SNA_CMD_RM:

            // trace_ds(" ReadModified");

            if(partition != 0x00)

                {
                    // trace_ds(" error: illegal partition\n");

                    // return;

                    Raise_GUI_Event(TN3270_INVALID_WSF);

                    return false;
                }

            //	trace_ds("\n");

            // ctlr_read_modified ( AID_QREPLY, false );

            // Raise_GUI_Event(TN3270_GET_SCREEN_BUFFER, (int)AID_QREPLY);

            Send_Screen_Buffer(GET_AID(
                "AID_QREPLY"));

            break;

        default:

            // trace_ds(" unknown type 0x%02x\n", buf[4]);

            // return;

            Raise_GUI_Event(TN3270_INVALID_WSF);

            return false;
        }

    free(querybuf);

    return true;
}

// void

// unsigned char buf[];

// int buflen;

bool Session3270::SF_Erase_Reset(unsigned char* buf, int buflen)

{
    Utility::Log_Message(__func__);

    if(buflen != 4)

        {
            // trace_ds(" error: wrong field length %d\n", buflen);

            Raise_GUI_Event(TN3270_INVALID_WSF);

            return false;
        }

    switch(buf[3])

        {
        case SF_ER_DEFAULT:

            // trace_ds(" Default\n");

            // ctlr_erase ( false );

            Raise_GUI_Event(TN3270_CTL_ERASE, 0);

            break;

        case SF_ER_ALT:

            // trace_ds(" Alternate\n");

            // ctlr_erase ( true );

            Raise_GUI_Event(TN3270_CTL_ERASE, 1);

            break;

        default:

            // trace_ds(" unknown type 0x%02x\n", buf[3]);

            return false;
        }

    return true;
}

// void

// unsigned char buf[];

// int buflen;

bool Session3270::SF_Set_Reply_Mode(unsigned char* buf, int buflen)

{
    Utility::Log_Message(__func__);

    unsigned char partition;

    unsigned char reply_mode;

    unsigned char crm_attr[16];

    int crm_nattr;

    int i;

    //	char comma = '(';

    if(buflen < 5)

        {
            // trace_ds(" error: wrong field length %d\n", buflen);

            Raise_GUI_Event(TN3270_INVALID_WSF);

            return false;
        }

    partition = buf[3];

    // trace_ds("(0x%02x)", partition);

    if(partition != 0x00)

        {
            Raise_GUI_Event(TN3270_INVALID_WSF);

            // trace_ds(" error: illegal partition\n");

            return false;
        }

    switch(buf[4])

        {
        case SF_SRM_FIELD:

            // trace_ds(" Field\n");

            break;

        case SF_SRM_XFIELD:

            // trace_ds(" ExtendedField\n");

            break;

        case SF_SRM_CHAR:

            //	trace_ds(" Character");

            break;

        default:

            // trace_ds(" unknown mode 0x%02x\n", buf[4]);

            Raise_GUI_Event(TN3270_INVALID_WSF);

            return false;
        }

    reply_mode = buf[4];

    if(buf[4] == SF_SRM_CHAR)

        {
            crm_nattr = buflen - 5;

            for(i = 5; i < buflen; i++)

                {
                    crm_attr[i - 5] = buf[i];

                    // trace_ds("%s%s", comma, see_efa_only(buf[i]));

                    //			comma = ',';
                }

            //	trace_ds("%s\n", crm_nattr ? ")" : "");
        }

    return true;
}

// void

// unsigned char buf[];

// int buflen;

bool Session3270::SF_Outbound_DS(unsigned char* buf, int buflen)

{
    Utility::Log_Message(__func__);

    if(buflen < 5)

        {
            // trace_ds(" error: field length %d too short\n", buflen);

            // return;

            Raise_GUI_Event(TN3270_INVALID_WSF);

            return false;
        }

    // trace_ds("(0x%02x)", buf[3]);

    if(buf[3] != 0x00)

        {
            // trace_ds(" error: illegal partition 0x%0x\n", buf[3]);

            Raise_GUI_Event(TN3270_INVALID_WSF);

            return false;

            // return false;
        }

    switch(buf[4])

        {
        case SNA_CMD_W:

            // trace_ds(" Write");

            if(buflen > 5)

                // ctlr_write ( &buf[4], buflen - 4, false );

                Raise_GUI_Event(TN3270_CTL_WRITE);

            else

                //	trace_ds("\n");

                break;

        case SNA_CMD_EW:

            // trace_ds(" EraseWrite");

            //	ctlr_erase ( screen_alt );

            Raise_GUI_Event(TN3270_CTL_ERASE, 1);

            if(buflen > 5)

                //	ctlr_write ( &buf[4], buflen - 4, true );

                Raise_GUI_Event(TN3270_CTL_WRITE);

            else

                //	trace_ds("\n");

                break;

        case SNA_CMD_EWA:

            // trace_ds(" EraseWriteAlternate");

            // ctlr_erase ( screen_alt );

            Raise_GUI_Event(TN3270_CTL_ERASE, 1);

            if(buflen > 5)

                //	ctlr_write ( &buf[4], buflen - 4, true );

                Raise_GUI_Event(TN3270_CTL_WRITE);

            else

                // trace_ds("\n");

                break;

        case SNA_CMD_EAU:

            // trace_ds(" EraseAllUnprotected\n");

            // ctlr_erase_all_unprotected();

            Raise_GUI_Event(TN3270_CTL_ERASE_ALL_UNPROT);

            break;

        default:

            Raise_GUI_Event(TN3270_INVALID_WSF);

            return false;

            // trace_ds(" unknown type 0x%02x\n", buf[4]);

            return false; /* error */
        }

    return true;
}

/*

void Session3270::Query_Reply_Start()

{

        //Utility::Log_Message(__func__);

//	obptr = obuf;

//	space3270out(1);

        // obuf.

        //obuf.push_back ( AID_SF );

        //qr_in_progress = True;

}

*/

int Session3270::Do_Query_Reply(unsigned char* qbuf, unsigned char code)

// unsigned char code;

{
    Utility::Log_Message(__func__);

    // int len;

    int i;

    unsigned char* obuf = qbuf;

    unsigned char* obuf0 = obuf;

    //	char *comma = "";

    //	int obptr0 = obptr - obuf;

    //   int obptr = 0;//obuf.size()-1;

    // int obptr0=obptr;

    //	unsigned char *obptr_len;

    unsigned char displaybytes[] = {0x00, 0x02, 0x00, 0x89, 0x00, 0x02, 0x00, 0x85, 0x09, 0x10};

   // unsigned short num, denom;

    //extern int default_screen;

#define CHAR_WIDTH 10

#define CHAR_HEIGHT 20

    // if ( qr_in_progress )

    //{

    //	trace_ds ( "> StructuredField\n" );

    //	qr_in_progress = False;

    //}

    // space3270out(4);

    // obptr+=2;

    obuf += 2;

    // obuf[obptr += 2;	/* skip length for now */

    *(obuf++) = GET_AID(
        "SFID_QREPLY");

    *(obuf++) = code;

    switch(code)

        {
        case QR_CHARSETS:

            //	trace_ds ( "> QueryReply(CharacterSets)\n" );

            // space3270out(23);

            *(obuf++) = 0x82; /* flags: GE, CGCSGID present */

            *(obuf++) = 0x00; /* more flags */

            *(obuf++) = (unsigned char)(CHAR_WIDTH); /* SDW */

            *(obuf++) = (unsigned char)(CHAR_HEIGHT); /* SDH */

            *(obuf++) = 0x00; /* Load PS format types */

            *(obuf++) = 0x00;

            *(obuf++) = 0x00;

            *(obuf++) = 0x00;

            *(obuf++) = 0x07; /* DL */

            *(obuf++) = 0x00; /* SET 0: */

            *(obuf++) = 0x10; /*  FLAGS: non-loadable, single-plane,

                       single-byte, no compare */

            *(obuf++) = 0x00; /*  LCID */

            *(obuf++) = 0x00; /*  CGCSGID: international */

            *(obuf++) = 0x67;

            *(obuf++) = 0x00;

            *(obuf++) = 0x26;

            *(obuf++) = 0x01; /* SET 1: */

            *(obuf++) = 0x00; /*  FLAGS: non-loadable, single-plane,

                       single-byte, no compare */

            *(obuf++) = 0xf1; /*  LCID */

            *(obuf++) = 0x03; /*  CGCSGID: 3179-style APL2 */

            *(obuf++) = 0xc3;

            *(obuf++) = 0x01;

            *(obuf++) = 0x36;

            break;

        case QR_IMP_PART:

            //	trace_ds ( "> QueryReply(ImplicitPartition)\n" );

            // space3270out(13);

            *(obuf++) = 0x0; /* reserved */

            *(obuf++) = 0x0;

            *(obuf++) = 0x0b; /* length of display size */

            *(obuf++) = 0x01; /* "implicit partition size" */

            *(obuf++) = 0x00; /* reserved */

            SET16(obuf, 80); /* implicit partition width */

            SET16(obuf, 24); /* implicit partition height */

            SET16(obuf, Screen_Width); /* alternate height */

            SET16(obuf, Screen_Height); /* alternate width */

            break;

        case QR_NULL:

            //	trace_ds ( "> QueryReply(Null)\n" );

            break;

        case QR_SUMMARY:

            // trace_ds ( "> QueryReply(Summary(" );

            // space3270out(NSR);

            for(i = 0; i < NSR; i++)

                {
                    // trace_ds ( "%s%s", comma, see_qcode ( supported_replies[i] ) );

                    // comma = ",";

                    *(obuf++) = supported_replies[i];
                }

            // trace_ds ( "))\n" );

            break;

        case QR_USABLE_AREA:

            // trace_ds ( "> QueryReply(UsableArea)\n" );

            // space3270out(19);

            *(obuf++) = 0x01; /* 12/14-bit addressing */

            *(obuf++) = 0x00; /* no special character features */

            SET16(obuf, Screen_Width); /* usable width */

            SET16(obuf, Screen_Height); /* usable height */

            *(obuf++) = 0x00; /* units (mm) */

            /*check this part*/

            // num = XDisplayWidthMM ( display, default_screen );

            // denom = XDisplayWidth ( display, default_screen );

            // 0x00020089 0x00020085 0x09 0x10

            // first 4 bytes distance between x points as fraction

            // second four bytes distance in y as fraction

            // 1 bytes number of x units in cell

            // 1 bytes number of y units in cells

            memcpy(obuf, displaybytes, 10);

            obuf += 10;

            //	while ( ! ( num % 2 ) && ! ( denom % 2 ) )

            //{

            // num /= 2;

            // denom /= 2;

            //}

            // SET16 ( obuf, ( int ) num );	/* Xr numerator */

            // SET16 ( obuf, ( int ) denom ); /* Xr denominator */

            //*check this part

            // num = XDisplayHeightMM ( display, default_screen );

            // denom = XDisplayHeight ( display, default_screen );

            // while ( ! ( num % 2 ) && ! ( denom % 2 ) )

            //{

            // num /= 2;

            // denom /= 2;

            //}

            // SET16 ( obuf, ( int ) num );	/* Yr numerator */

            // SET16 ( obuf, ( int ) denom ); /* Yr denominator */

            *(obuf++) = (unsigned char)CHAR_WIDTH; /* AW */

            *(obuf++) = (unsigned char)CHAR_HEIGHT; /* AH */

            SET16(obuf, 0); /* buffer */

            break;

        case QR_COLOR:

            //	trace_ds ( "> QueryReply(Color)\n" );

            // space3270out(4 + 2*15);

            *(obuf++) = 0x00; /* no options */

            *(obuf++) = 16; /* report on 16 colors */

            *(obuf++) = 0x00; /* default color: */

            *(obuf++) = 0xf0 + COLOR_GREEN; /*  green */

            for(i = 0xf1; i <= 0xf7; i++)

                {
                    *(obuf++) = i;

                    // if (model_3279 )

                    *(obuf++) = i;

                    //	else

                    //	*(obuf++) = 0x00;
                }

            break;

        case QR_HIGHLIGHTING:

            //	trace_ds ( "> QueryReply(Highlighting)\n" );

            // space3270out(11);

            *(obuf++) = 5; /* report on 5 pairs */

            *(obuf++) = XAH_DEFAULT; /* default: */

            *(obuf++) = XAH_NORMAL; /*  normal */

            *(obuf++) = XAH_BLINK; /* blink: */

            *(obuf++) = XAH_BLINK; /*  blink */

            *(obuf++) = XAH_REVERSE; /* reverse: */

            *(obuf++) = XAH_REVERSE; /*  reverse */

            *(obuf++) = XAH_UNDERSCORE; /* underscore: */

            *(obuf++) = XAH_UNDERSCORE; /*  underscore */

            *(obuf++) = XAH_INTENSIFY; /* intensify: */

            *(obuf++) = XAH_INTENSIFY; /*  intensify */

            break;

        case QR_REPLY_MODES:

            //	trace_ds ( "> QueryReply(ReplyModes)\n" );

            // space3270out(3);

            *(obuf++) = SF_SRM_FIELD;

            *(obuf++) = SF_SRM_XFIELD;

            *(obuf++) = SF_SRM_CHAR;

            break;

        case QR_ALPHA_PART:

            // trace_ds ( "> QueryReply(AlphanumericPartitions)\n" );

            // space3270out(4);

            *(obuf++) = 0; /* 1 partition */

            SET16(obuf, Screen_Height * Screen_Width); /* buffer space */

            *(obuf++) = 0; /* no special features */

            break;

        case QR_DDM:

            // trace_ds ( "> QueryReply(DistributedDataManagement)\n" );

            // space3270out(8);

            SET16(obuf, 0); /* set reserved field to 0 */

            SET16(obuf, 2048); /* set inbound length limit */

            SET16(obuf, 2048); /* set outbound length limit */

            SET16(obuf, 0x0101); /* NSS=01, DDMSS=01 */

            break;

        default:

            return 0; /* internal error */
        }

    // obptr_len = obuf + obptr0;

    // len = ( obptr - obuf ) - obptr0;

    //	SET16 ( obuf_len, len );

    // obufsize=obufsize+(obptr-obufsize);

    int size = (int)(obuf - obuf0);

    SET16(obuf0, size);

    return size;  // skip length bytes
}

void Session3270::Query_Reply_End(unsigned char* qbuf, int bufsz)

{
    Utility::Log_Message(__func__);

    //	Send_Output_Buffer();

    //	kybd_inhibit ( True );

    // Raise_GUI_Event(TN3270_KEYBOARD_INHIBIT,1);

    //	kybd_inhibit ( True );

    // Send_Output_Buffer();

    qbuf[bufsz++] = IAC;

    qbuf[bufsz++] = EORMARK;

    Send_Buffer(qbuf, bufsz);

    Raise_GUI_Event(TN3270_KEYBOARD_INHIBIT, 1);
}

/*

void Session3270::Set16(unsigned char* bufptr, int x)

{

        Utility::Log_Message(__func__);

        //int obptr = pos;

        //unsigned char*bufptr;

   // unsigned char buf16[2];

        //bufptr = buf16;

        SET16(bufptr,x);

        obuf[pos++]= (unsigned char) buf16[0];

        obuf[pos]= (unsigned char) buf16[1];

}*/

/*

void Session3270::Send_Output_Buffer()

{

        Utility::Log_Message(__func__);

        //Sock_Send(&obuf[0],obuf.size());

    Send_Buffer( (unsigned char*)&obuf[0],obuf.size());

}*/

void Session3270::Send_Buffer(unsigned char* buf, int size)

{
    unsigned char* localbuf;

    localbuf = (unsigned char*)malloc(size + 5);

    memcpy(localbuf + 5, buf, size);

    if(Is_TN3270E())

        {
            memset(localbuf, 0, 5);  // tn3270-e set up first 5 bytes zero header

            memcpy(localbuf + 3, SeqNum, 2);

            Sock_Send(localbuf, size + 5);

            // calloc
        }

    else

        {
            Sock_Send(localbuf + 5, size);
        }

    free(localbuf);
}

bool Session3270::Is_TN3270E()

{
    return isTN3270E;
}

void Session3270::Send_Response_Command()

{
    unsigned char respmsg[8];

    respmsg[DATA_TYPE] = RESPONSE;

    respmsg[REQUEST_FLAG] = ERR_COND_CLEARED;

    respmsg[RESPONSE_FLAG] = POSITIVE_RESPONSE;

    memcpy(respmsg + SEQ_NUMBER, SeqNum, 2);

    respmsg[SEQ_NUMBER + 2] = 0x00;

    respmsg[SEQ_NUMBER + 3] = IAC;

    respmsg[SEQ_NUMBER + 4] = EORMARK;

    Sock_Send(respmsg, sizeof respmsg);
}

void Session3270::Close_Connection()

{
    exitflg = true;
}

SessionParms Session3270::Get_Session_Parameters()

{
    // this->Host

    SessionParms parmlist;

    std::string sessionstr =
                    "",
                tn3270flg =
                    "";

    parmlist[
        "Host"] = this->Host;

    parmlist[
        "Port"] = this->Port;

    parmlist[
        "TerminalType"] = this->Terminal_Type;

    if(support_TN3270E == true)

        {
            tn3270flg =
                "True";
        }

    else

        {
            tn3270flg =
                "False";
        }

    parmlist[
        "TN3270E"] = tn3270flg;

    return parmlist;
}

void Session3270::Set_Session_Parameters(SessionParms parmlist)

{
    // this->Host

    this->Host = parmlist[
        "Host"];

    this->Port = parmlist[
        "Port"];

    this->SetTerminalType(parmlist[
        "TerminalType"]);

    if(parmlist[
           "TN3270E"] ==
       "True")

        {
            support_TN3270E = true;
        }

    else

        {
            support_TN3270E = false;
        }
}

void Session3270::Remove_Double_Telnet_Escape()

{
    Utility::Log_Message(__func__);

    int k = 0;

    for(int j = 0; j < Command_Length; j++)

        {
            // cancel double escapes x'ff' and x'ff' will become single x'ff'

            if(j != 0 && Command_Buffer[j] == 0xff && Command_Buffer[j - 1] == 0xff)

                {
                    // ignore

                    LOGPRINTF(
                        "DOUBLE ESCAPE SKIPPED");
                }

            else

                {
                    Command_Buffer[k] = Command_Buffer[j];

                    k++;
                }
        }

    Command_Length = k;
}

bool Session3270::process_SF_RP_QUERY(unsigned char partition)

{
    Utility::Log_Message(__func__);

    unsigned char* querybuf;

    int offset = 0;

    if(partition != 0xff)

        {
            // trace_ds(" error: illegal partition\n");

            Raise_GUI_Event(TN3270_INVALID_WSF);

            return false;
        }

    // trace_ds("\n");

    // Query_Reply_Start();

    querybuf = (unsigned char*)malloc(1000);

    offset = 0;

    querybuf[offset++] = GET_AID(
        "AID_SF");

    for(int i = 0; i < NSR; i++)

        {
            offset = offset + Do_Query_Reply(querybuf + offset, supported_replies[i]);
        }

    Query_Reply_End(querybuf, offset);

    //   LOGPRINTF("BEFORE BREAK");

    free(querybuf);

    return true;
}

bool Session3270::process_SF_RP_QLIST(unsigned char partition, unsigned char* buf, int buflen)

{
    Utility::Log_Message(__func__);

    unsigned char* querybuf;

    int offset = 0;

    if(partition != 0xff)

        {
            Raise_GUI_Event(TN3270_INVALID_WSF);

            return false;
        }

    // offset = 0;

    querybuf = (unsigned char*)malloc(1000);

    querybuf[offset++] = GET_AID(
        "AID_SF");

    switch(buf[5])

        {
        case SF_RPQ_LIST:

            // LOGPRINTF("SF_RPQ_LIST");

            // trace_ds("List(");

            if(buflen < 7)

                {
                    // trace_ds(")\n");

                    Do_Query_Reply(querybuf, QR_NULL);
                }

            else

                {
                    // trace_ds(")\n");

                    int any = 0;

                    for(int i = 0; i < NSR; i++)

                        {
                            if(memchr((char*)&buf[6], (char)supported_replies[i], buflen - 6))

                                {
                                    offset = offset + Do_Query_Reply(querybuf + offset, supported_replies[i]);

                                    any++;
                                }
                        }

                    if(!any)

                        {
                            Do_Query_Reply(querybuf, QR_NULL);
                        }
                }

            break;

        case SF_RPQ_EQUIV:

            //    LOGPRINTF("SF_RPQ_EQUIV");

            // trace_ds("Equivlent+List(");

            //     for(i = 6; i < buflen; i++) {

            //	trace_ds("%s%s", comma, see_qcode(buf[i]));

            //     comma = ',';

            //   }

            //	trace_ds(")\n");

            for(int i = 0; i < NSR; i++)

                {
                    offset = offset + Do_Query_Reply(querybuf + offset, supported_replies[i]);
                }

            break;

        case SF_RPQ_ALL:

            //    LOGPRINTF("SF_RPQ_ALL");

            //	trace_ds("All\n");

            for(int i = 0; i < NSR; i++)

                {
                    offset = offset + Do_Query_Reply(querybuf + offset, supported_replies[i]);
                }

            break;

        default:

            //	trace_ds("unknown request type 0x%02x\n", buf[5]);

            Raise_GUI_Event(TN3270_INVALID_WSF);

            return false;

            // return;		/* error */
        }

    Query_Reply_End(querybuf, offset);

    //   break;

    free(querybuf);

    return true;
}

std::string Session3270::Get_Screen_HTML()

{
    //Utility::Log_Message(__func__);
    LOGPRINTF("Extracing HTML");
    return HTMLParser::Convert_3270_Screen_to_HTML(Scr_Buffer);
}

void Session3270::Send_Screen_Buffer(unsigned char AID)

{
    LOGPRINTF("Processing AID:"<<AID);
    unsigned char* tempbuf;  ///, *tbuf;

    tempbuf = (unsigned char*)malloc(SCREEN_BUF_SIZE);

    // Send_Buffer(tempbuf,Scr_Buffer->Send_Screen_Buffer(tempbuf,GET_AID("AID_QREPLY")));

    Send_Buffer(tempbuf, Scr_Buffer->Send_Screen_Buffer(tempbuf, AID));

    free(tempbuf);
}

void Session3270::Process_Key(int key)

{
    //if(ALL_AIDS.)

    
    if(IS_WX_PFKEY(key))
    {
    key = CONVERT_WX_KEY(key);
    if(IS_AID(key))

        {
            if(key == GET_AID("AID_PA1"))

                {
                    Scr_Buffer->UnLockScreen();
                    Raise_GUI_Event(TN3270_UNLOCK_SCREEN);
                }

            else
                {
                    Send_Screen_Buffer(key);
                }
        }
    }
    else

        {
            if(!Scr_Buffer->IsLocked())

                {
                    Scr_Buffer->Update_Screen_Buffer((unsigned char*)&key, 1, true);

                    if(Scr_Buffer->IsLocked())

                        {
                            Raise_GUI_Event(TN3270_LOCK_SCREEN);
                        }

                    else

                        {
                            Raise_GUI_Event(TN3270_REFRESH_SCREEN);
                        }

                    //Refresh_Screen();
                }
        }
}

void Session3270::Move_Cursor(int direction)

{
    switch(direction)

        {
        case CURSOR_LEFT:

            Scr_Buffer->Move_Cursor_Left();

            break;

        case CURSOR_RIGHT:

            Scr_Buffer->Move_Cursor_Right();

            break;

        case CURSOR_UP:

            Scr_Buffer->Move_Cursor_Top();

            break;

        case CURSOR_DOWN:

            Scr_Buffer->Move_Cursor_Bottom();

            break;

        case CURSOR_TAB:

            Scr_Buffer->Set_Cursor_Pos(Scr_Buffer->Get_Next_Field_Pos());

            break;

        default:

            break;
        }

    Raise_GUI_Event(TN3270_REFRESH_SCREEN);
}

int Session3270::GetColPos()

{
    int curpos = Scr_Buffer->Get_Cursor_Pos();

    return Scr_Buffer->GetCol(curpos);
}

int Session3270::GetRowPos()

{
    int curpos = Scr_Buffer->Get_Cursor_Pos();

    return Scr_Buffer->GetRow(curpos);
}

Session3270::Session3270(wxFrame* parent, SessionParms& parms)

{
    Set_Session_Parameters(parms);

    Scr_Buffer = new ScreenBuffer3270(GetScreenHeight(), GetScreenWidth());

    Screen_Background_Colour = wxTheColourDatabase->Find(
        "BLACK");

    isTN3270E = false;

    exitflg = false;

    m_parent = parent;

    Command_Length = 0;

    device_name.resize(DEVICE_NAME_LENGTH);

    wxInitialize();
}

void Session3270::SetTerminalType(std::string ttype)

{
    this->Terminal_Type = ttype;

    for(const auto& s : Utility::Terminal_Types)

        {
            if(this->Terminal_Type == std::get<0>(s))

                {
                    this->Screen_Height = std::get<2>(s);

                    this->Screen_Width = std::get<3>(s);

                    break;
                }
        }
}

// bool Session3270::Is_14Bit_Mode()

//{

// return address14bit;

//}

/* Macros. */

/*-------------wsf end-------*/

unsigned char Session3270::IAC_DO_TERMINAL[] = {IAC, DO, TERMTYP};

unsigned char Session3270::RESP_WILL_TERMINAL[] = {IAC, WILL, TERMTYP};

unsigned char Session3270::SB_SEND_TERMINAL[] = {IAC, SB, TERMTYP, SEND, IAC, SE};

unsigned char Session3270::SB_IS_TERMINAL[] = {IAC, SB, TERMTYP, IS};

unsigned char Session3270::IAC_WILL_BINARY[] = {IAC, WILL, BINARY};

unsigned char Session3270::IAC_DO_BINARY[] = {IAC, DO, BINARY};

unsigned char Session3270::IAC_WILL_EOR[] = {IAC, WILL, EOR};

unsigned char Session3270::IAC_DO_EOR[] = {IAC, DO, EOR};

unsigned char Session3270::IAC_WILL_TN3270E[] = {IAC, WILL, TN3270E};

unsigned char Session3270::IAC_DO_TN3270E[] = {IAC, DO, TN3270E};

unsigned char Session3270::IAC_WONT_TN3270E[] = {IAC, WONT, TN3270E};

unsigned char Session3270::IAC_SB_TN3270E_SEND_DEVICE[] =

    {

     IAC, SB, TN3270E, TN3270E_SEND, TN3270E_DEVICE_TYPE, IAC, SE

};

unsigned char Session3270::SB_TN3270E_DEVICE_TYPE_REQUEST[] = {IAC,

                                                               SB,

                                                               TN3270E,

                                                               TN3270E_DEVICE_TYPE,

                                                               TN3270E_REQUEST

};

unsigned char Session3270::SB_TN3270E_DEVICE_IS[] = {IAC, SB, TN3270E, TN3270E_DEVICE_TYPE, TN3270E_IS};
unsigned char Session3270::IAC_SB_TN3270E_REJECT_DEVICE[] = {IAC, SB, TN3270E, TN3270E_DEVICE_TYPE, TN3270E_REJECT,TN3270E_REASON};

unsigned char Session3270::TN3270E_FUNCTIONS_REQUEST[] = {IAC, SB, TN3270E,

                                                          TN3270E_FUNCTIONS,
                                                          TN3270E_REQUEST,
                                                          TN3270E_RESPONSES,

                                                          IAC,
                                                          SE

};

unsigned char Session3270::TN3270E_FUNCTIONS_RESPONSE[] = {IAC, SB, TN3270E, TN3270E_FUNCTIONS,

                                                           TN3270E_IS,
                                                           TN3270E_RESPONSES,
                                                           IAC,
                                                           SE

};

aidlist Session3270::ALL_AIDS = {{"AID_NO", 0x60}, /* no AID generated */

                                 {"AID_QREPLY", 0x61},

                                 {"AID_ENTER", 0x7d},

                                 {"AID_PF1", 0xf1},

                                 {"AID_PF2", 0xf2},

                                 {"AID_PF3", 0xf3},

                                 {"AID_PF4", 0xf4},

                                 {"AID_PF5", 0xf5},

                                 {"AID_PF6", 0xf6},

                                 {"AID_PF7", 0xf7},

                                 {"AID_PF8", 0xf8},

                                 {"AID_PF9", 0xf9},

                                 {"AID_PF10", 0x7a},

                                 {"AID_PF11", 0x7b},

                                 {"AID_PF12", 0x7c},

                                 {"AID_PF13", 0xc1},

                                 {"AID_PF14", 0xc2},

                                 {"AID_PF15", 0xc3},

                                 {"AID_PF16", 0xc4},

                                 {"AID_PF17", 0xc5},

                                 {"AID_PF18", 0xc6},

                                 {"AID_PF19", 0xc7},

                                 {"AID_PF20", 0xc8},

                                 {"AID_PF21", 0xc9},

                                 {"AID_PF22", 0x4a},

                                 {"AID_PF23", 0x4b},

                                 {"AID_PF24", 0x4c},

                                 {"AID_OICR", 0xe6},

                                 {"AID_MSR_MHS", 0xe7},

                                 {"AID_SELECT", 0x7e},

                                 {"AID_PA1", 0x6c},

                                 {"AID_PA2", 0x6e},

                                 {"AID_PA3", 0x6b},

                                 {"AID_CLEAR", 0x6d},

                                 {"AID_SYSREQ", 0xf0},

                                 {"AID_SF", 0x88},

                                 {"SFID_QREPLY", 0x81}

};

std::map<int, unsigned char> Session3270::WX_KEY_MAP =

    {

     {WXK_RETURN, GET_AID("AID_ENTER")},

     {WXK_F1, GET_AID("AID_PF1")},

     {WXK_F2, GET_AID("AID_PF2")},

     {WXK_F3, GET_AID("AID_PF3")},

     {WXK_F4, GET_AID("AID_PF4")},

     {WXK_F5, GET_AID("AID_PF5")},

     {WXK_F6, GET_AID("AID_PF6")},

     {WXK_F7, GET_AID("AID_PF7")},

     {WXK_F8, GET_AID("AID_PF8")},

     {WXK_F9, GET_AID("AID_PF9")},

     {WXK_F10, GET_AID("AID_PF10")},

     {WXK_F11, GET_AID("AID_PF11")},

     {WXK_F12, GET_AID("AID_PF12")},

     {WXK_ESCAPE, GET_AID("AID_PA1")}

};

/*Please note that all numeric literals in this document specify

   decimal values, unless they are preceded by "0x", in which case a

   hexadecimal value is represented. look at rfc 2355

    *

   http://webcache.googleusercontent.com/search?q=cache:U_YI0l0zT18J:https://tools.ietf.org/html/rfc2355+&cd=2&hl=en&ct=clnk&gl=nz



       TN3270E            40

         ASSOCIATE          00

         CONNECT            01

         DEVICE-TYPE        02

         FUNCTIONS          03

         IS                 04

         REASON             05

         REJECT             06

         REQUEST            07

         SEND               08



       Reason-codes

         CONN-PARTNER       00

         DEVICE-IN-USE      01

         INV-ASSOCIATE      02

         INV-NAME           03*/

// fandezhi.efglobe.com

// check http://www.tommysprinkle.com/mvs/P3270/sfe.htm

// sample send message 00000000007dd9d811d9d5a3a296ffef for TN3270e

// look at http://www.ingenuityworking.com/knowledge/w/knowledgebase/ibm-3270-emulation-data-flow.aspx

// for aid sending reference

// look at http://midrange-l.midrange.narkive.com/w02e5ybD/tn3270e-support-in-v5r2

// also look at http://midrange-l.midrange.narkive.com/w02e5ybD/tn3270e-support-in-v5r2
