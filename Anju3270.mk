##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=Anju3270
ConfigurationName      :=Debug
WorkspacePath          := "C:\bitbucket\anju3270emulator"
ProjectPath            := "C:\bitbucket\anju3270emulator"
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=TPF
Date                   :=14/09/2015
CodeLitePath           :="C:\Program Files (x86)\CodeLite"
LinkerName             :=C:/MinGW-4.8.1/bin/g++.exe
SharedObjectLinkerName :=C:/MinGW-4.8.1/bin/g++.exe -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="Anju3270.txt"
PCHCompileFlags        :=
MakeDirCommand         :=makedir
RcCmpOptions           := $(shell wx-config --rcflags)
RcCompilerName         :=C:/MinGW-4.8.1/bin/windres.exe
LinkOptions            :=  -mwindows $(shell wx-config --libs --debug) -lWS2_32
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)"C:/Program Files/Microsoft SDKs/Windows/v7.1/Lib" 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := C:/MinGW-4.8.1/bin/ar.exe rcu
CXX      := C:/MinGW-4.8.1/bin/g++.exe
CC       := C:/MinGW-4.8.1/bin/gcc.exe
CXXFLAGS := -std=c++11 -g -O0 -std=c++11 -Wall $(shell wx-config --cflags --debug) $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := C:/MinGW-4.8.1/bin/as.exe


##
## User defined environment variables
##
CodeLiteDir:=C:\Program Files (x86)\CodeLite
UNIT_TEST_PP_SRC_DIR:=C:\UnitTest++-1.3
WXWIN:=C:\wxWidgets
WXCFG:=gcc_dll\mswud
Objects0=$(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IntermediateDirectory)/wxcrafter.cpp$(ObjectSuffix) $(IntermediateDirectory)/wxcrafter_bitmaps.cpp$(ObjectSuffix) $(IntermediateDirectory)/Session3270.cpp$(ObjectSuffix) $(IntermediateDirectory)/Utility.cpp$(ObjectSuffix) $(IntermediateDirectory)/HTMLParser.cpp$(ObjectSuffix) $(IntermediateDirectory)/ScreenChar3270.cpp$(ObjectSuffix) $(IntermediateDirectory)/ScreenBuffer3270.cpp$(ObjectSuffix) $(IntermediateDirectory)/Anju3270Frame.cpp$(ObjectSuffix) $(IntermediateDirectory)/ConnectionSetupDialog.cpp$(ObjectSuffix) \
	$(IntermediateDirectory)/STLserialization.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@$(MakeDirCommand) "./Debug"


$(IntermediateDirectory)/.d:
	@$(MakeDirCommand) "./Debug"

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/main.cpp$(ObjectSuffix): main.cpp $(IntermediateDirectory)/main.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/bitbucket/anju3270emulator/main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/main.cpp$(DependSuffix): main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/main.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/main.cpp$(DependSuffix) -MM "main.cpp"

$(IntermediateDirectory)/main.cpp$(PreprocessSuffix): main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/main.cpp$(PreprocessSuffix) "main.cpp"

$(IntermediateDirectory)/wxcrafter.cpp$(ObjectSuffix): wxcrafter.cpp $(IntermediateDirectory)/wxcrafter.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/bitbucket/anju3270emulator/wxcrafter.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/wxcrafter.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/wxcrafter.cpp$(DependSuffix): wxcrafter.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/wxcrafter.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/wxcrafter.cpp$(DependSuffix) -MM "wxcrafter.cpp"

$(IntermediateDirectory)/wxcrafter.cpp$(PreprocessSuffix): wxcrafter.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/wxcrafter.cpp$(PreprocessSuffix) "wxcrafter.cpp"

$(IntermediateDirectory)/wxcrafter_bitmaps.cpp$(ObjectSuffix): wxcrafter_bitmaps.cpp $(IntermediateDirectory)/wxcrafter_bitmaps.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/bitbucket/anju3270emulator/wxcrafter_bitmaps.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/wxcrafter_bitmaps.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/wxcrafter_bitmaps.cpp$(DependSuffix): wxcrafter_bitmaps.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/wxcrafter_bitmaps.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/wxcrafter_bitmaps.cpp$(DependSuffix) -MM "wxcrafter_bitmaps.cpp"

$(IntermediateDirectory)/wxcrafter_bitmaps.cpp$(PreprocessSuffix): wxcrafter_bitmaps.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/wxcrafter_bitmaps.cpp$(PreprocessSuffix) "wxcrafter_bitmaps.cpp"

$(IntermediateDirectory)/Session3270.cpp$(ObjectSuffix): Session3270.cpp $(IntermediateDirectory)/Session3270.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/bitbucket/anju3270emulator/Session3270.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/Session3270.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Session3270.cpp$(DependSuffix): Session3270.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/Session3270.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/Session3270.cpp$(DependSuffix) -MM "Session3270.cpp"

$(IntermediateDirectory)/Session3270.cpp$(PreprocessSuffix): Session3270.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/Session3270.cpp$(PreprocessSuffix) "Session3270.cpp"

$(IntermediateDirectory)/Utility.cpp$(ObjectSuffix): Utility.cpp $(IntermediateDirectory)/Utility.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/bitbucket/anju3270emulator/Utility.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/Utility.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Utility.cpp$(DependSuffix): Utility.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/Utility.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/Utility.cpp$(DependSuffix) -MM "Utility.cpp"

$(IntermediateDirectory)/Utility.cpp$(PreprocessSuffix): Utility.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/Utility.cpp$(PreprocessSuffix) "Utility.cpp"

$(IntermediateDirectory)/HTMLParser.cpp$(ObjectSuffix): HTMLParser.cpp $(IntermediateDirectory)/HTMLParser.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/bitbucket/anju3270emulator/HTMLParser.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/HTMLParser.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/HTMLParser.cpp$(DependSuffix): HTMLParser.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/HTMLParser.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/HTMLParser.cpp$(DependSuffix) -MM "HTMLParser.cpp"

$(IntermediateDirectory)/HTMLParser.cpp$(PreprocessSuffix): HTMLParser.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/HTMLParser.cpp$(PreprocessSuffix) "HTMLParser.cpp"

$(IntermediateDirectory)/ScreenChar3270.cpp$(ObjectSuffix): ScreenChar3270.cpp $(IntermediateDirectory)/ScreenChar3270.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/bitbucket/anju3270emulator/ScreenChar3270.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/ScreenChar3270.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/ScreenChar3270.cpp$(DependSuffix): ScreenChar3270.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/ScreenChar3270.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/ScreenChar3270.cpp$(DependSuffix) -MM "ScreenChar3270.cpp"

$(IntermediateDirectory)/ScreenChar3270.cpp$(PreprocessSuffix): ScreenChar3270.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/ScreenChar3270.cpp$(PreprocessSuffix) "ScreenChar3270.cpp"

$(IntermediateDirectory)/ScreenBuffer3270.cpp$(ObjectSuffix): ScreenBuffer3270.cpp $(IntermediateDirectory)/ScreenBuffer3270.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/bitbucket/anju3270emulator/ScreenBuffer3270.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/ScreenBuffer3270.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/ScreenBuffer3270.cpp$(DependSuffix): ScreenBuffer3270.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/ScreenBuffer3270.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/ScreenBuffer3270.cpp$(DependSuffix) -MM "ScreenBuffer3270.cpp"

$(IntermediateDirectory)/ScreenBuffer3270.cpp$(PreprocessSuffix): ScreenBuffer3270.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/ScreenBuffer3270.cpp$(PreprocessSuffix) "ScreenBuffer3270.cpp"

$(IntermediateDirectory)/Anju3270Frame.cpp$(ObjectSuffix): Anju3270Frame.cpp $(IntermediateDirectory)/Anju3270Frame.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/bitbucket/anju3270emulator/Anju3270Frame.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/Anju3270Frame.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Anju3270Frame.cpp$(DependSuffix): Anju3270Frame.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/Anju3270Frame.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/Anju3270Frame.cpp$(DependSuffix) -MM "Anju3270Frame.cpp"

$(IntermediateDirectory)/Anju3270Frame.cpp$(PreprocessSuffix): Anju3270Frame.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/Anju3270Frame.cpp$(PreprocessSuffix) "Anju3270Frame.cpp"

$(IntermediateDirectory)/ConnectionSetupDialog.cpp$(ObjectSuffix): ConnectionSetupDialog.cpp $(IntermediateDirectory)/ConnectionSetupDialog.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/bitbucket/anju3270emulator/ConnectionSetupDialog.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/ConnectionSetupDialog.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/ConnectionSetupDialog.cpp$(DependSuffix): ConnectionSetupDialog.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/ConnectionSetupDialog.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/ConnectionSetupDialog.cpp$(DependSuffix) -MM "ConnectionSetupDialog.cpp"

$(IntermediateDirectory)/ConnectionSetupDialog.cpp$(PreprocessSuffix): ConnectionSetupDialog.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/ConnectionSetupDialog.cpp$(PreprocessSuffix) "ConnectionSetupDialog.cpp"

$(IntermediateDirectory)/STLserialization.cpp$(ObjectSuffix): STLserialization.cpp $(IntermediateDirectory)/STLserialization.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/bitbucket/anju3270emulator/STLserialization.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/STLserialization.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/STLserialization.cpp$(DependSuffix): STLserialization.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/STLserialization.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/STLserialization.cpp$(DependSuffix) -MM "STLserialization.cpp"

$(IntermediateDirectory)/STLserialization.cpp$(PreprocessSuffix): STLserialization.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/STLserialization.cpp$(PreprocessSuffix) "STLserialization.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Debug/


