#ifndef SCREENBUFFER3270_H
#define SCREENBUFFER3270_H
#define SCREEN_BUF_SIZE 5000
#include <vector>
#include "ScreenChar3270.h"
#include "DataStream3270.h"
//#include "Session3270.h"
#include "Utility.h"
//#include <assert>
#include <sstream>
using std::vector;
class ScreenBuffer3270
{
	public:
		ScreenBuffer3270 ( int r, int c );
		virtual ~ScreenBuffer3270();
		void Set_Screen_Buffer_Address ( int bufadr );
		void Move_Cursor_Left(int x=1);
		void Move_Cursor_Right(int x=1);
		void Move_Cursor_Top();
		void Move_Cursor_Bottom();
		int Send_Screen_Buffer ( unsigned char* bufadr , unsigned char AID);// = AID_NO);
		int Get_Cur_Buffer_Pos();
		void MarkFieldChanged(int bufferpos);
		void Clear_Screen();
		int GetMaxRows() const;
		int GetMaxColumns() const;
		bool Update_Screen_Buffer(unsigned char* data,int length,bool changeflag=false);
		int Get_Screen_Buffer_Data ( unsigned char* getbuf,bool ebcdic=true );//,bool changed=false);
        int Get_Next_Field_Pos();
		void Start_Field(unsigned char fielddat);
		void Set_Cursor_Pos(int position);
		int Get_Cursor_Pos();
		ScreenChar3270* GetScreenDataAt(int row,int col);
		int GetRow ( int bufadr );
		int GetCol ( int bufadr );
		int GetRow();
		int GetCol();
		void Start_Field_Extended(unsigned char *buffer,int length);
		void LockScreen();
		void UnLockScreen();
		bool IsLocked();
		bool Is_14Bit_Mode();
		void Set_14Bit_Mode(bool mode=true);
	private:
		int max_rows, max_columns;
		int cursorpos;
		
		bool address14bit;
		int currow, curcolumn;
		bool screenlocked;
//expand this to get all data till attribute changes, for html parser
		/*int Get_Screen_Buffer_Data ( unsigned char* getbuf );*/
         bool formatted;
         bool IsFieldProtected(int position);
//ScreenChar3270* scrbuffer;
		vector<vector<ScreenChar3270> > screenbuf;
};

#endif // SCREENBUFFER3270_H
