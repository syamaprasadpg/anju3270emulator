#include "ConnectionSetupDialog.h"
//termtype ConnectionSetupDialog::Terminal_Type =
ConnectionSetupDialog::ConnectionSetupDialog(wxWindow* parent)
    : ConnectionSetupDialogBase(parent)
{
	//int x;
	//wxString termstring;
    this->m_device_name->SetMaxLength(8);
	for(const auto& s:Utility::Terminal_Types)
	{ //termstring=;
	//wxMessageBox(termstring);
		m_combo_terminal_type->Append((wxString)(std::get<0>(s)+std::get<1>(s)));
		}
	//wxMessageBox( wxT("Invalid port number"), wxT("Connect Error"), wxICON_ERROR);
	//m_combo_terminal_type->Append("xyz");
	this->Fit();
}

ConnectionSetupDialog::~ConnectionSetupDialog()
{
}

void ConnectionSetupDialog::On_Click_3270_Connect(wxCommandEvent& event)
{
	Host = this->m_text_host->GetValue();//.Trim().Trim(true);
	Host = Host.Trim().Trim(false);
    tn3270eflag = this->m_check_tn3270e->IsChecked();
	Port =this->m_text_port->GetValue();
	Port = Port.Trim().Trim(false);
	Terminal_Type=this->m_combo_terminal_type->GetValue().BeforeFirst('(');
	for(const auto& s:Utility::Terminal_Types)
	{
		if(Terminal_Type==std::get<0>(s))
		{max_rows = std::get<2>(s);
		max_columns = std::get<3>(s);
		break;
		}	}

	long value;
    if(!Port.ToLong(&value)) {
		wxMessageBox( wxT("Invalid port number"), wxT("Connect Error"), wxICON_ERROR);
	}
	else
	{
		verified=true;
	this->Close();
	}
	
	
}
void ConnectionSetupDialog::Button_Key_Down(wxKeyEvent& event)
{
	wxMessageBox("KeyDown-Button");
}

ConnectionSetupDialog::ConnectionSetupDialog(wxWindow* parent, wxString t_host, wxString t_port, wxString t_term_type, bool t_tn3270):ConnectionSetupDialogBase(parent)
{
    this->m_text_host->SetValue(t_host);
    this->m_text_port->SetValue(t_port);
    
        this->m_check_tn3270e->SetValue(t_tn3270);

    
}
