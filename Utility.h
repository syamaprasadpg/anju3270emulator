#ifndef UTILITY_H
#define UTILITY_H
#include <fstream>
#include <wx/string.h>
#include <wx/log.h>
#include <wx/event.h>
#include <DataStream3270.h>
#include <vector>
#include <map>
#include <string>
#include <tuple>
#include <functional>
#include <stdexcept>
#include <algorithm>
using std::vector;
typedef std::tuple<std::string,std::string,int, int> termtuple; 
typedef std::array<termtuple, 10> termtype;
#define TERMINAL std::make_tuple 
class Utility 
{
public:
	static wxMutex *s_mutexlog;
    static void    ASCII_to_EBCDIC ( long, unsigned char *, unsigned char *);
    static void    EBCDIC_to_ASCII ( long, unsigned char *, unsigned char *);
	static wxString Get_Hex_String(unsigned char *,int );
	static void Log_Hex_String(unsigned char *,int);
	static void Log_Message(const std::string& vstring);
    static void string_split(const std::string& str, const std::string& delim, vector<std::string>& parts);
    static termtype Terminal_Types;/* */
    //*-----------
   static  std::map<int, unsigned char> Key_Map;
    //*-----------
    //
    //    ASCII to EBCDIC translation table
    //
    static std::string get_file_contents(const char *filename);
    static void Cleanup_String(std::string& str);
	private:
    static unsigned char ASCII_translate_EBCDIC [ 256 ];

    //
    //    EBCDIC to ASCII translation table
    //
    static unsigned char EBCDIC_translate_ASCII [ 256 ];

};
//}
#define LOGPRINTF(X) \
{             \
				std::stringstream strbuffer; \
				strbuffer <<__func__<<":"<< X << std::endl; \
				Utility::Log_Message ( strbuffer.str() );	\
				}
#endif